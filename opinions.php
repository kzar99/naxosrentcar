<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Give us <strong>your opinion!</strong></h1>
	       			
					<p><strong>Coming soon.</strong></p>
                    
                    
					
              	</div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
<script language="JavaScript">
function validateForm()
{	
	if (document.forms[0].elements[0].value == "")
	{
		$('#error').html("Please write your full name!");
		return false;
	}

	if (document.forms[0].elements[1].value == "" || isValidEmailAddress(document.forms[0].elements[1].value) != true)
	{
		$('#error').html("Invalid E-mail Address! Please re-enter");
		return false;
	}

	if (document.forms[0].elements[2].value == "")
	{
		$('#error').html("Please write your phone!");
		return false;
	}

}

$("#phone").keypress(function(e) {
	var a = [];
	var k = e.which;
	
	for (i = 48; i < 58; i++)
		a.push(i);
	
	if (!(a.indexOf(k)>=0))
		e.preventDefault();
	
});


</script>

</body>
</html>
				