<?php
include("includes/connection.php"); 
include("includes/func.php"); 


//echo "Posts: <pre>"; 
//print_r($_POST); 
//echo "</pre>";

/*
Array
(
    [flag] => insert
    [session_id] => ef48d3f5e55de994965e06ee571facc1
    [total_cost] => 186
    [pre_payment] => 37.2
    [extras_temp_cost] => 74.00
    [Submit2] => Proceed!
)
*/

if (!isset($_POST['session_id']))
{
	header("Location: index.htm");
}
$session_id = $_POST['session_id'];

$query1="SELECT * FROM rental WHERE session_id='$session_id' ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
$myrow1 = mysql_fetch_array($result1);

$temp_start_date_real = $myrow1['start_date'];
$temp_end_date_real = $myrow1['end_date'];
$pick_up_location_id = $myrow1['pickup_area'];
$drop_off_location_id = $myrow1['dropoff_area'];
$company_id = $myrow1['company'];
$pre_payment = $myrow1['pre_pay'];
$category = $myrow1['category'];
$discount_code = trim($myrow1['discount_code']);

$base_cost = $myrow1['total_cost'];
$total_cost = $myrow1['total_cost'];

$extras = $myrow1['extras'];
$original_cost = $myrow1['original_cost'];



$extras_cost = $total_cost - $base_cost;
$original_cost += $extras_cost;

//
// company data
//
$km_cost = 0;
$query100="SELECT * FROM main_company_list WHERE comp_id='$company_id' ";
$result100 = mysql_query($query100)  or die(mysql_error().'<p>'.$query100.'</p>');
while ($myrow100 = mysql_fetch_array($result100))
{
$temp_company_web=$myrow100['comp_name_db'];
$temp_company_name=$myrow100['comp_name_title'];
$temp_company_id=$myrow100['comp_id'];
} // close connection 100


//
// category data
//
$query2="SELECT * FROM company_category WHERE category='$category' ";
$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
while ($myrow2 = mysql_fetch_array($result2))
{
$cat_id = $myrow2['cat_id'];
$cat_name = $myrow2['category'];
$cat_description = $myrow2['description'];
$cat_cars = $myrow2['cat_cars'];
$cat_num_cars = $myrow2['num_cars'];
$cat_image = $myrow2['image'];
$car_type = $myrow2['car_type'];
}
	
//
// extras
//
$extras_array = explode(",", $extras);
$email_extras = "";
if (is_array($extras_array) && count($extras_array)>0)
{
	foreach ($extras_array as $extra_id)
	{
		$query10="SELECT * FROM company_extras WHERE ex_id='$extra_id' ";
		$result10 = mysql_query($query10)  or die(mysql_error().'<p>'.$query10.'</p>');
		while ($row10 = mysql_fetch_array($result10))
		{
			if ($row10['extras_name']!='')
			{
			$email_extras .= $row10['extras_name'].", ";
			}
		}
	}
	
	if ($email_extras!='') {  $email_extras = substr($email_extras, 0, -2); }
}

//
// area/locations calculations
//	


	$query1="SELECT * FROM main_special_location WHERE special_id='$pick_up_location_id' ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($myrow1 = mysql_fetch_array($result1))
	{
	$pick_up_location = $myrow1['special_name'];
	}

	$query11="SELECT * FROM main_special_location WHERE special_id='$drop_off_location_id' ";
	$result11 = mysql_query($query11)  or die(mysql_error().'<p>'.$query11.'</p>');
	while ($myrow11 = mysql_fetch_array($result11))
	{
	$drop_off_location = $myrow11['special_name'];
	}

//
// date/time calculations
//
	$a = explode("-",$temp_start_date_real);
	$mystart = mktime (0, 0, 0, $a[1], $a[0], $a[2]);
	$h1 = explode(":",$a[3]);
	$hour1 = $h1[0];
	
	$b = explode("-",$temp_end_date_real);
	$myend = mktime (0, 0, 0, $b[1], $b[0], $b[2]);
	$h2 = explode(":",$b[3]);
	$hour2 = $h2[0];
	
	$dur = $myend - $mystart;
	
	if ($hour2>$hour1)
	{
	$temp_duration = floor($dur/86400)+1;
	}
	else
	{
	$temp_duration = floor($dur/86400);
	}	


	$bb = get_start_end_period($mystart,$myend);
	$b1 = explode("@@",$bb);
	$temp_period_start = $b1[0];
	$temp_period_end = $b1[1];


	$query12="SELECT * FROM company_car_list_".$temp_period_start." WHERE cat_id='$cat_id' ";
	$result12 = mysql_query($query12)  or die(mysql_error().'<p>'.$query12.'</p>');
	while ($myrow12 = mysql_fetch_array($result12))
	{
	$pososto = $myrow12['pososto'];
	}
	$pososto_show = round($pososto*100,0);




$qT = "SELECT * FROM vehicles WHERE veh_code='$car_type' LIMIT 1 ";
$rT = mysql_query($qT)  or die(mysql_error().'<p>'.$qT.'</p>');
if (mysql_num_rows($rT)>0)
{
	$rowT = mysql_fetch_array($rT);
	
	$car_show = stripslashes($rowT['veh_name']);
}
else
{
	$car_show = 'Unknown';
}


//
// discount checks
//
$discount_show = 0;
$discount_form = "no";
$discount = 0;
$full_name_member = '';
$phone_member = '';
$mobile_member = '';
$email_member = '';


if ($discount_code!="")
{
	$query13="SELECT * FROM members WHERE discount_code='$discount_code' LIMIT 1 ";
	$result13 = mysql_query($query13)  or die(mysql_error().'<p>'.$query13.'</p>');
	if (mysql_num_rows($result13)>0)
	{
		while ($myrow13 = mysql_fetch_array($result13))
		{
		$full_name_member = stripslashes($myrow13['full_name']);
		$phone_member = stripslashes($myrow13['phone']);
		$mobile_member = stripslashes($myrow13['mobile']);
		$email_member = stripslashes($myrow13['email']);
		}		
	
		$query14="SELECT * FROM member_discount WHERE id=1 ";
		$result14 = mysql_query($query14)  or die(mysql_error().'<p>'.$query14.'</p>');
		while ($myrow14 = mysql_fetch_array($result14))
		{
		$discount_show = $myrow14['discount'];
		}
		
		$discount = (100-$discount_show)/100;
		
		$discount_form = "yes";
		
		// recalculate total cost		
		$base_cost_with_discount = $base_cost*$discount;
		$total_cost = $base_cost_with_discount + $extras_cost;

		$sql996a = "UPDATE  rental SET total_cost='$total_cost' WHERE session_id='$session_id' ";
		$result996a = mysql_query($sql996a)  or die(mysql_error().'<p>'.$sql996a.'</p>');	

	}
	else
	{
	$discount = 0;
	}
}







?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Your data - Motonaxos</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
<form action="car-extra.htm" method="post" style="display:none;" id="goBack">
<input name="session_id" type="hidden" value="<?php echo $session_id;?>" />
</form> 
 
    <!--content section-->
	<section class="contentin">
        <div class="container">
 		
        	
        	<div class="float-l mar-top25 color474"><h1 class="size50">STEP<strong class="rb-bold">03</strong></h1><p align="right">Fill form</p></div>
  
           	<div class="float-r mar-top25 color474 goback">
				<p>You can: <a href="javascript:history.back()" class="go-back">Go back</a> | <a href="index.htm">Make a new search</a></p>
			</div>
        
            <hr  style="clear:both; visibility:hidden;"/>
            
            <div class="row">
                <div class="rental-data col-md-12">
                    <h2>Rental Data</h2>
                    <div class="data-desc">
                        <p><strong>Pickup Data:</strong> From <?php echo $pick_up_location;?> on <?php echo $temp_start_date_real;?></p>
                        <p><strong>Dropof data:</strong> To <?php echo $drop_off_location;?> on <?php echo $temp_end_date_real;?></p>
                        <p><strong>Rental duration:</strong> <?php echo $temp_duration;?> days in total</p>
                        <p><strong>Vehicle Type:</strong> <?php echo $car_show;?></p>
                        <p><strong>Rental Rate:</strong> <?php echo $base_cost;?> € in <?php echo $cat_name;?> category</p>
                        <p><strong>Total Cost:</strong> <?php echo $total_cost;?> € <?php if ($email_extras!=''){echo '(including '.$email_extras.')';}?>
                        </p>
						
						<?php
						if ($original_cost!=$total_cost && $discount_show>0)
						{
						?>
						<p>Your membership provides a <strong><?php echo $discount_show;?>% discount</strong> to the original price of <?php echo $original_cost;?> &#8364; </p>
						<?php
						}
						?>						
						
                   </div>
                </div>
             </div>  
                
			<div class="carform mar-top25">

				<p class="head">To complete your booking please submit a <?php echo $pososto_show;?>% deposit (<strong><?php echo $pre_payment; ?> €</strong>).<br />
The remainder of your rental rate is paid upon pick up of your vehicle.</p>

				<div class="row">
                		<form action="payment.php" method="post" id="paymentForm" >
                        <ul>
                        <li class="col-md-6">
                            <div class="labels">Name:<span>as it appear on credit card</span></div>
                            <div class="inputs"><input id="full_name" name="full_name" type="text" class="field" value="<?php echo $full_name_member;?>" <?php if ($discount_form=="yes"){echo " disabled";}?>/></div>
                        </li>
                         <li class="col-md-6">
                            <div class="labels">Phone:</div>
                            <div class="inputs"><input id="phone" name="phone" type="text" class="field" value="<?php echo $phone_member;?>" <?php if ($discount_form=="yes"){echo " disabled";}?>/></div>
                        </li>
                        <li class="col-md-6">
                            <div class="labels">Mobile:</div>
                            <div class="inputs"><input id="mobile" name="mobile" type="text" class="field" value="<?php echo $mobile_member;?>" <?php if ($discount_form=="yes"){echo " disabled";}?>/></div>
                        </li>
                        <li class="col-md-6">
                            <div class="labels">Email:</div>
                            <div class="inputs"><input id="email" name="email" type="email" class="field" value="<?php echo $email_member;?>" <?php if ($discount_form=="yes"){echo " disabled";}?>/></div>
                        </li>
						
						<?php
						if ($pick_up_location=="1.Port (car/atv/buggy)")
						{						
						?>
						<li class="col-md-6">
							 <div class="labels">Ship Name:</div>
							 <div class="inputs"><input id="ship_name" name="ship_name" type="text" class="field"/></div>
						</li>						
						<?php
						}							
						
						if ($pick_up_location=="2.Airport")
						{
						?>
						<li class="col-md-6">
							 <div class="labels">Flight Number:</div>
							 <div class="inputs"><input id="flight" name="flight" type="text" class="field"/></div>
						</li>
						<?php
						}


						if ($pick_up_location=="6.Hotel up to 7km")
						{						
						?>
						<li class="col-md-6">
							 <div class="labels">Hotel Name:</div>
							 <div class="inputs"><input id="hotel_name" name="hotel_name" type="text" class="field"/></div>
						</li>						
						<?php
						}					
						?>						
												
						
                        <li class="col-md-6">
                            <div class="labels">Driver Age:</div>
                            <div class="inputs">
							<select id="age" class="driver_age" name="driver_age" tabindex="1">
							<option value="" selected="selected">Select Age</option>
							<?php
							for ($a = 19; $a<=79; $a++)
							{
							?>
							<option value="<?php echo $a;?>"><?php echo $a;?></option>
							<?php
							}
							?>
                              </select>                            
                            </div>
                            <hr  style="clear:both; visibility:hidden;"/>
                         </li>
                         <li class="col-md-6">
                            <div class="labels">Comments:<br />
                            <span class="col-md-9" style="padding:0px;">Please write down any comments you may have.<br /> If for example you would like to pick up your car from the airport, please write down your flight number and your time of arrival. Also, feel free to ask any further questions here.
In case you have asked for baby seats, please write the ages of the children, so as to include the proper sized baby seat with your car.</span>
                            </div>
                            <div class="inputs"><textarea name="sxolia" cols="" rows="" class="textarea"></textarea></div>
                        </li>
                        <li id="error" class="col-md-6">
                            
                            
                        </li>
                        <li class="col-md-6">
                        	<input id="submit-form" name="submit-form" type="submit" class="booknow" value="BOOK NOW" style="margin-right:15px;"/> 
							<?php 
							/*
							email owner 8 Jun 2019
							Θα παρακαλούσα πολύ όπως περιοριστούν οι ώρες πιθανής κράτησης από 8:00 το πρωί έως 23:00  το βράδυ 
							και να μην εμφανίζονται οι υπόλοιπες επιλογές.
							*/
							putenv ('TZ=Europe/Athens');
							$now = new DateTime();
							$begin = new DateTime('8:00');
							$end = new DateTime('23:00');

							if ($now >= $begin && $now <= $end)
							{
							?>
							<input id="submit-form2" name="submit-form2" type="submit" class="booknow" value="REQUEST NOW*"/>
							<?php 
							}
							?>
							
							<hr  style="clear:both; visibility:hidden; margin:0px;"/>
                        </li>
						<li class="col-md-6">
						<br style="clear:both;" />
						*Request will send an email to the rental office - no online payment will occur.
						</li>
                       </ul>
						 <input name="session_id" type="hidden" value="<?php echo $session_id;?>">
						 <input name="flag" type="hidden" value="insert">
                        </form>
               </div>


            </div>
           <hr  style="clear:both; visibility:hidden;"/>
        	
        </div><!--container-->
    </section>
    
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>



<script>
$(document).ready(function()
{
	$(".go-back").click(function(e)
	{
		e.preventDefault();
		$("#goBack").submit();
	});	

	$(".driver_age").dropkick();
	$("#phone,#mobile").keypress(function(e) {
		var a = [];
		var k = e.which;
		
		for (i = 48; i < 58; i++)
			a.push(i);
		
		if (!(a.indexOf(k)>=0))
			e.preventDefault();
		
	});
	
	
	$("#paymentForm").submit(function(){
	
		if ( $("#full_name").val()=="") {
			$('#error').html("Please write your full name!");
		return false;
		}
	
		if ( $("#phone").val() == "") {
			$('#error').html("Please write your phone!");
		return false;
		}	
	
		if ( $("#email").val()=="" ) {
			$('#error').html("Invalid E-mail Address! Please re-enter");
			return false;
		}	
	
		if ( $("#age").val()=="" ) {
			$('#error').html("Please select the driver's age!");
			return false;
		}	
	
		<?php if ($pick_up_location=="2.Airport") { ?>	
			if ( $("#flight").val()=="" ) {
				$('#error').html("Please write your flight number!");
				return false;
			}		
		<?php } ?>
	
		<?php if ($pick_up_location=="6.Hotel up to 7km") { ?>	
			if ( $("#hotel_name").val()=="" ) {
				$('#error').html("Please write your hotel name!");
				return false;
			}		
		<?php } ?>	
	
		<?php if ($pick_up_location=="1.Port (car/atv/buggy)") { ?>	
			if ( $("#ship_name").val()=="" ) {
				$('#error').html("Please write your ship name!");
				return false;
			}		
		<?php } ?>
	
	});
	
});
</script>






    </body>
</html>
				