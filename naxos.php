<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Naxos</h1>
					<h2>1. Routes By Bike</h2>
                    <p><em>In our office, you will find maps compatible to GPS for free.</em></p>
	                <p>Take a hat and some water with you and come to pick up the most suitable bike for your height. In our office, you will find specialized staff who will help you and will give you a free map with the most beautiful routes.</p>
<h4>A. For Beginners</h4>
<p>CHORA – AGIOS PROKOPIS - AGIA ANNA - MIKRI VIGLA AND BACK (25 KM)<br />
<i class="fa fa-angle-double-right"></i> <a href="/images/map8.swf" data-type="iframe" class="map1">click to view map</a></p>
<h4>B. For Advanced Cyclists</h4>
<p>CHORA – GALANADO - AGIOS MAMAS - KATO POTAMIA - ANO POTAMIA - KALAMITSIA – KOUROS – MILI – KOUROUNOCHORI - CHORA (25KM)<br />
<i class="fa fa-angle-double-right"></i> <a href="/images/map6.swf" data-type="iframe" class="map2">click to view map</a></p>
<h4>C. For Only A Few</h4>
<p>CHORA – IRIA - AGIOS ARSENIOS –TRIPODES -AGIOS MAMAS - KATO POTAMIA – ANO POTAMIA – KOUROS – MILI – KOUROUNOCHORI – AGIDIA – CHORA (30 KM)<br />
<i class="fa fa-angle-double-right"></i> <a href="/images/map7.swf" data-type="iframe" class="map3">click to view map</a></p>

<h2>2. Routes by ATV and Off Road Motorcycle</h2>
<p>In the following routes, we drive with great caution and low speed so as not to cause any damage to the vehicle. Don’t forget that insurance for destroyed car rubbers or damages on the lower part of the vehicle is not provided. So, pick up the 4 or 2 wheel vehicle that suits you, take a helmet and a map and set off for an unforgettable driving experience!</p>
<p>A. CHORA – AGIA ANNA - MIKRI VIGLA – KASTRAKI – ALIKO –PIRGAKI - AGIASSOS – DIMITRAS TEMPLE – SAGRI – GALANADO -CHORA<br />
<i class="fa fa-angle-double-right"></i> <a href="/images/map3.swf" data-type="iframe" class="map4">click to view map</a></p>
<p>B. CHORA - IPSILIS TOWER - AMITIS BAY – SKEPONI - MONI FANEROMENIS - ABRAMI BAY - CHORA<br />
<i class="fa fa-angle-double-right"></i> <a href="/images/map5.swf" data-type="iframe" class="map5">click to view map</a></p>
<p>C. CHORA – CHALKI – FILOTI – DANAKOS - MONI FOTODOTI - PSILI AMMOS – MOUTSOUNA – APERANTHOS – STAVROS – KINIDAROS - CHORA<br />
<i class="fa fa-angle-double-right"></i> <a href="/images/map4.swf" data-type="iframe" class="map6">click to view map</a></p>
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				