<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container terms">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                	
                  <h1>Termini e <strong>Condizioni</strong><p class="pull-right">Languages: <a href="terms.htm">EN</a> | <a href="terms-gr.htm">GR</a> | <a href="terms-de.htm">DE</a> | <a href="terms-fr.htm">FR</a> | <a href="terms-it.htm"><strong>IT</strong></a> | <a href="terms-ru.htm">RUS</a></p>
                  </h1>
	              
     	<h2>Regole e condizioni per la locazione di automobili e motociclette </h2>
<p>Benvenuti da Motonaxos.com, il sistema on line il più ampio per la locazione di automobili e motociclette di Naxos.Offriamo un’ampia gamma di automobili a noleggio da Naxos, modelli di motociclette e ATV nuovi e sicuri, la più alta qualità delle vetture a noleggio, servizio clienti VIP nonché ottime condizioni di noleggio di automobili o motociclette. </p>

<h3>Annullamento di polizza assicurativa: </h3>
<ul class="checklist2">
<li>ANNULLAMENTO SENZA SPESE SE AVVENUTO: </li>
<li>48 ore prima del primo giorno di noleggio per le categorie quanto segue:
Automobili, scooter, ATV </li>
<li>5 giorni prima del primo giorno di noleggio per le categorie quanto segue:
Automobili speciali, motociclette e buggies </li>
</ul>
<p>Per ogni annullamento eseguito dopo queste scadenze, il deposito cauzionale sarà trattenuto. 
La compagnia locatrice ha la facoltà di revocare la prenotazione in caso di imprecisioni quali per esempio dettagli sulla patente di guida, sulla carta di credito ecc., ed ha la facoltà di trattenere l’intero deposito cauzionale versato. </p>

<h3>POLITICA DEI PREZZI: </h3>
<p>Noi garantiamo i migiori prezzi e la massima copertura assicurativa di vetture di alta qualità con un ampio standart dei servizi. 
Attenzione: I numeri e le quotazioni indicati sono validi solo per prenotazioni fatte direttamente dalla nostra pagina.</p>

<h3>REGOLE E CONDIZIONI PER LA LOCAZIONE DI VETTURA </h3>
<strong>Equipaggiamento e servizi inclusi nel prezzo</strong><br />

<p>assistenza stradale guidatore aggiuntivo seggiolino infantile (da prenotare in anticipo con conferma dal punto di presa) numero illimitato di chilometri presa e rilascio della vettura da e all’aeroporto, il porto e gli alberghi nell’orario di aperture del nostro ufficio (8:30-21:30). Supplemento fuori orario. mappe stradali caschi</p>
<p><strong>TASSE:</strong> Tutte le tasse vengono incluse nel prezzo di base.
<strong>Pagamento:</strong> Si accettano le carte di credito quanto segue: MasterCard/VISA.
- Per i pagamenti effettuati solo attraverso la nostra wesbsite, sono accettate le seguenti carte: carte di credito, carte di debito, carte prepagate e di Visa, MasterCard, Maestro, American Express.
- Le tasse rimanenti di noleggio vengono pagate al momento della presa. Si richiede il pagamento di un deposito cauzionale ad ogni locazione. Si accettano carte di credito.</p>

<h3>Riconsegna della vettura: </h3>
<p>Il Cliente è tenuto a restituire la vettura alla data, ora e nel luogo convenuti nello stesso stato tecnico in cui l’a preso. La vettura dovrà essere restituita con la stessa quantità del carburante nel serbatoio che vi era presente al momento della consegna. 
Preghiamo di tener presente che non vengono rimborsate somme per carburante non utilizzato.</p>

<h3>NOLEGGIO DI AUTOMOBILE: </h3>
<strong>Requisiti per il Cliente:</strong>

<p>Età minima del Conducente- 23 anni per le categorie A, B, C, M, F, mini speciale, E, D e 27 anni per le categrorie U, K, L
Patente di guida: Il Conducente dovrà esssere in possesso di patente di guida valida da almeno un anno. 
Passaporto o carta d’identità ugualmente richiesti.</p>

<p><strong>ATTENZIONE!</strong> Per alcuni conducenti che non hanno la cittadinanza di un paese dell’UE, può essera richiesta la presenza di una patente di guida internazionale. In caso di dubbi, non esitate di spedirci una copia della patente per avere la nostra opinione. 
Assicurazioni: Le assicurazioni seguenti sono disponibili:</p>

<p>Responsabilità civile per ogni tipo di veicolo: Con la RC in caso di incidente la nostra compagnia di assicurazione copre i danni che potreste recare ad altra autovettura Responsabilità limitata in caso di urto (Collision Damage Waiver). Il Cliente è assicurato per danni recati all’autovettura da lui noleggiata, in caso di incidente per un massimale di: Gruppi А, В, C, M: 600  € (+IVA) Gruppi D, E, F: 800 € (+IVA) Gruppi U, L, speciali mini: 1100 € (+IVA) Esonero da responsabilità in caso di urto (SuperC.D.W. insurance) disponibile per la maggior parte degli automobili al livello locale (età minima del Conducente 27 anni) Assicurazione in caso di furto (TheftInsurance). L’assicurazione copre l’ammontare sopraindicato. Assicurazione furto e incendio (SuperTheftInsurance) senza tassa. Assicurazione infortunio individuale (PersonalAccidentInsurance) per il guidatore dell’autovettura.</p>
<p><strong>ATTENZIONE:</strong> Non è valida la copertura assicurativa nel caso in cui il guidatore fosse sotto l’effetto dell’alcool, di ogni tipo di sostanze stupefacenti oppure abbia commesso qualsiasi trasgressione sulla strada.Il Conducente si assume la responsabilità dello stazionamento o delle trasgressioni avvenute durante la durata del noleggio.E tassativamente proibito guidare i veicoli su sabbia, su sponde, in riva al mare, in laghi rinsecchiti oppure off-road (eccezione fatta per i jeep appropriati) I veicoli dovranno essere guidati solamente su strade lastricate o asfaltate. 
Preghiamo di tener presente che la copertura assicurativa non è valida per danni recati ai pneumatici, agli specchietti retrovisori, al telaio, chiavi smarrite, guasto dello scambio. </p>

<h3>Prestazioni automobilistiche: </h3>
<p><strong>Tetto rotaie:</strong> fornite dopo prenotazione cotro supplemento da versare, per alcuni modelli di vetture. 
Dispositivo GPS:fornito dopo prenotazione contro supplemento da pagare. 
Pulizia dell’automobile Allergy free- effettuata contro un supplemento. </p>

<h3>LOCAZIONE DI SCOOTER, MOTOCICELTTA, ATV E BUGGY </h3>
<p><strong>Requisiti per il Cliente: </strong><br />
Età minima del Conducente- 21 anni. 
Patente di guida: Il Conducente dovrà essere in possesso di una patente di guida. Potrete guidare uno scooter fino ai 150 metri cubi solo se ne avreste diritto nel proprio paese di origine. Per la motocicletta è necessario avere una patente di guida per la rispettiva categoria. Per gli ATV e i buggies dovete essere in possesso di una patente di guida per automobili. Passaporto o carta d’identità ugualmente richiesti. </p>

<p><strong>ATTENZIONE!</strong> Per alcuni conducenti che non hanno la cittadinanza di un paese dell’UE, può essera richiesta la presenza di una patente di guida internazionale. In caso di dubbi, non esitate di spedirci una copia della patente per avere la nostra opinione. 
Assicurazioni: Le assicurazioni seguenti sono disponibili:</p>

<p>Responsabilità civile per ogni tipo di veicolo: Con la RC in caso di incidente la nostra compagnia di assicurazione copre i danni che potreste recare ad altra autovettura Assicurazione in caso di furto (TheftInsurance). ). L’assicurazione copre l’ammontare sopraindicato. Assicurazione furto e incendio (SuperTheftInsurance) senza tassa. Responsabilità limitata in caso di urto (Collision Damage Waiver). Il Cliente è assicurato per danni recati all’autovettura da lui noleggiata, in caso di incidente dell’ammontare di (da definire sul posto) Assicurazione bucatura di gomma (FlatTiresInsurance) l’assicurazione per la bucatura di gomma copre la riparazione e il trasporto dell’autoveicolo senza tasse complementari</p>
<p><strong>ATTENZIONE:</strong> Non è valida la copertura assicurativa nel caso in cui il guidatore fosse sotto l’effetto dell’alcool, di ogni tipo di sostanze stupefacenti oppure abbia commesso qualsiasi trasgressione sulla strada.Il Conducente si assume la responsabilità dello stazionamento o delle trasgressioni avvenute durante la durata del noleggio.E tassativamente proibito guidare i veicoli su sabbia, su sponde, in riva al mare, in laghi rinsecchiti oppure off-road (eccezione fatta per i jeep appropriati) I veicoli dovranno essere guidati solamente su strade lastricate o asfaltate.</p>

<p>Preghiamo di tener presente che non è valida la copertura assicurativa per danni recati ai pneumatici, agli specchietti retrovisori, al telaio, chiavi smarrite, guasto dello scambio.</p>
		<center><img src="/images/horizontal.jpg" /></center>

                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				