<?php
include("includes/connection.php"); 
include("includes/func.php"); 

if ( !isset($_POST) ) 
{ 
	echo 'Unauthorized access.';
	exit();
}

$post_data_array = array();
if (isset($_POST['mid'])) 			{$post_data_array[0] = $_POST['mid'];}
if (isset($_POST['orderid'])) 		{$post_data_array[1] = $_POST['orderid'];}
if (isset($_POST['status'])) 		{$post_data_array[2] = $_POST['status'];}
if (isset($_POST['orderAmount'])) 	{$post_data_array[3] = $_POST['orderAmount'];}
if (isset($_POST['currency'])) 		{$post_data_array[4] = $_POST['currency'];}
if (isset($_POST['paymentTotal'])) 	{$post_data_array[5] = $_POST['paymentTotal'];}
if (isset($_POST['message'])) 		{$post_data_array[6] = $_POST['message'];}
if (isset($_POST['riskScore'])) 	{$post_data_array[7] = $_POST['riskScore'];}
if (isset($_POST['payMethod'])) 	{$post_data_array[8] = $_POST['payMethod'];}
if (isset($_POST['txId'])) 			{$post_data_array[9] = $_POST['txId'];}
if (isset($_POST['paymentRef'])) 	{$post_data_array[10] = $_POST['paymentRef'];}
									 $post_data_array[11] = "Cardlink"; // this from bank email

$post_DIGEST = $_POST['digest'];

$post_data = implode("", $post_data_array);
$digest = base64_encode(sha1($post_data,true));


$session_id = $post_data_array[1];
$splitter = "\n";


$sql999 = "UPDATE rental SET bank_status='Bank stopped',status='bank failed' WHERE session_id='$session_id' ";
$result999 = mysql_query($sql999)  or die(mysql_error().'<p>'.$sql999.'</p>');



//echo "Posts: <pre>"; 
//print_r($_POST); 
//echo "</pre>";

$query1="SELECT * FROM rental WHERE session_id='$session_id' ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
$myrow1 = mysql_fetch_array($result1);

$temp_start_date_real = $myrow1['start_date'];
$temp_end_date_real = $myrow1['end_date'];
$pick_up_location_id = $myrow1['pickup_area'];
$drop_off_location_id = $myrow1['dropoff_area'];
$company_id = $myrow1['company'];
$base_cost = $myrow1['base_cost'];
$extras = $myrow1['extras'];
$pre_payment = $myrow1['pre_pay'];
$category = $myrow1['category'];
$full_name = $myrow1['full_name'];
$phone = $myrow1['phone'];
$mobile = $myrow1['mobile'];
$email = $myrow1['email'];
$hotel_name = $myrow1['hotel_name'];
$flight_number = $myrow1['flight_number'];
$total_cost = $myrow1['total_cost'];
$assoc_code = $myrow1['associate'];
$sxolia = $myrow1['sxolia'];


$cat = '';

//
// company data
//
$query100="SELECT * FROM main_company_list WHERE comp_id='$company_id' ";
$result100 = mysql_query($query100)  or die(mysql_error().'<p>'.$query100.'</p>');
while ($myrow100 = mysql_fetch_array($result100))
{
$temp_company_web=$myrow100['comp_name_db'];
$temp_company_name=$myrow100['comp_name_title'];
$temp_company_id=$myrow100['comp_id'];
} // close connection 100


//
// category data
//
if ($cat!="")
{
	$query2="SELECT * FROM company_category WHERE cat_id='$cat' ";
	$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
	while ($myrow2 = mysql_fetch_array($result2))
	{
	$cat_id = $myrow2['cat_id'];
	$cat_name = $myrow2['category'];
	$cat_description = $myrow2['description'];
	$cat_cars = $myrow2['cat_cars'];
	$cat_num_cars = $myrow2['num_cars'];
	$cat_image = $myrow2['image'];
	$car_type = $myrow2['car_type'];
	}

	$sql999 = "UPDATE rental SET category='$cat_name',car_type='$car_type' WHERE session_id='$session_id' ";
	$result999 = mysql_query($sql999)  or die(mysql_error().'<p>'.$sql999.'</p>');
}
else
{
	$query2="SELECT * FROM company_category WHERE category='$category' ";
	$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
	while ($myrow2 = mysql_fetch_array($result2))
	{
	$cat_id = $myrow2['cat_id'];
	$cat_name = $myrow2['category'];
	$cat_description = $myrow2['description'];
	$cat_cars = $myrow2['cat_cars'];
	$cat_num_cars = $myrow2['num_cars'];
	$cat_image = $myrow2['image'];	
	$car_type = $myrow2['car_type'];
	}
}


//
// extras
//
$extras_array = explode(",", $extras);
$email_extras = "";
if (is_array($extras_array) && count($extras_array)>0)
{
	foreach ($extras_array as $extra_id)
	{
		$query10="SELECT * FROM company_extras WHERE ex_id='$extra_id' ";
		$result10 = mysql_query($query10)  or die(mysql_error().'<p>'.$query10.'</p>');
		while ($row10 = mysql_fetch_array($result10))
		{
			if ($row10['extras_name']!='')
			{
			$email_extras .= $row10['extras_name'].", ";
			}
		}
	}
	
	if ($email_extras!='') {  $email_extras = substr($email_extras, 0, -2); }
}


//
// area/locations calculations
//	


	$query1="SELECT * FROM main_special_location WHERE special_id='$pick_up_location_id' ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($myrow1 = mysql_fetch_array($result1))
	{
	$pick_up_location = $myrow1['special_name'];
	}

	$query11="SELECT * FROM main_special_location WHERE special_id='$drop_off_location_id' ";
	$result11 = mysql_query($query11)  or die(mysql_error().'<p>'.$query11.'</p>');
	while ($myrow11 = mysql_fetch_array($result11))
	{
	$drop_off_location = $myrow11['special_name'];
	}

//
// date/time calculations
//
	$a = explode("-",$temp_start_date_real);
	$mystart = mktime (0, 0, 0, $a[1], $a[0], $a[2]);
	$h1 = explode(":",$a[3]);
	$hour1 = $h1[0];
	
	$b = explode("-",$temp_end_date_real);
	$myend = mktime (0, 0, 0, $b[1], $b[0], $b[2]);
	$h2 = explode(":",$b[3]);
	$hour2 = $h2[0];
	
	$dur = $myend - $mystart;
	
	if ($hour2>$hour1)
	{
	$temp_duration = floor($dur/86400)+1;
	}
	else
	{
	$temp_duration = floor($dur/86400);
	}	


	$bb = get_start_end_period($mystart,$myend);
	$b1 = explode("@@",$bb);
	$temp_period_start = $b1[0];
	$temp_period_end = $b1[1];


	$query12="SELECT * FROM company_car_list_".$temp_period_start." WHERE cat_id='$cat_id' ";
	$result12 = mysql_query($query12)  or die(mysql_error().'<p>'.$query12.'</p>');
	while ($myrow12 = mysql_fetch_array($result12))
	{
	$pososto = $myrow12['pososto'];
	}
	$pososto_show = round($pososto*100,0);


$qT = "SELECT * FROM vehicles WHERE veh_code='$car_type' LIMIT 1 ";
$rT = mysql_query($qT)  or die(mysql_error().'<p>'.$qT.'</p>');
if (mysql_num_rows($rT)>0)
{
	$rowT = mysql_fetch_array($rT);
	
	$car_show = stripslashes($rowT['veh_name']);
}
else
{
	$car_show = 'Unknown';
}



//
// extras
//
$extras_array = explode(",", $extras);
$email_extras = "";
foreach ($extras_array as $extra_id)
{
	$query10="SELECT * FROM company_extras WHERE ex_id='$extra_id' ";
	$result10 = mysql_query($query10)  or die(mysql_error().'<p>'.$query10.'</p>');
	while ($row10 = mysql_fetch_array($result10))
	{
	$email_extras .= $row10['extras_name'].", ";
	}
}




//////////////////////////
//////////////////////////
// EMAIL TO info@naxosrentcar.com //
//////////////////////////
//////////////////////////
$test2 = "
<html>
<head>
<title>Canceled Car Hire from www.naxosrentcar.com</title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
</head>
<body>
A car hire transanction was canceled:<b>
<hr><br>
<b>Personal Data:</b><br>
User id (unique) : ".nl2br($session_id)." <br> 
Full Name : ".nl2br($full_name)."<br>
Phone : ".nl2br($phone)."<br>
Mobile : ".nl2br($mobile)."<br>
E-mail : ".nl2br($email)."<br>
Pick up location : ".nl2br($pick_up_location)."<br>
Drop off location : ".nl2br($drop_off_location)."<br>
Comments : ".nl2br($sxolia)."<br>
<hr><br>
<b>Car Hire Data:</b><br>
Category : ".nl2br($cat_name)."<br>
Start Date : ".nl2br($temp_start_date_real)."<br>
End Date : ".nl2br($temp_end_date_real)."<br>
Extras : ".nl2br($email_extras)."<br>
Total cost : ".nl2br($total_cost)." euro<br>
<br>
<br>
</body>
</html>
";



$to2 = "info@naxosrentcar.com";
$subject2 = "Canceled Car Hire from www.naxosrentcar.com";
$message2 = $test2;
$header1 = "MIME-Version: 1.0".$splitter;
$header1 .= "Content-Type: text/html; charset=utf-8".$splitter;
$header1 .= "Return-Path: ".$to2."".$splitter;
$header1 .= "Reply-To: ".$to2."".$splitter;
$header1 .= "From: website autoreport <".$to2.">".$splitter;
$header1 .= "X-Mailer: PHP/".phpversion().$splitter;

mail($to2, $subject2, $message2, $header1);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container">
 		
        	
        	<div class="float-l mar-top25 color474"><h1 class="size50">Error<strong class="rb-bold">Request</strong></h1><p align="right">was cancelled!</p></div>

           	<div class="float-r mar-top25 color474 goback">
				<p>You can: <a href="index.htm">Make a new search</a></p>
			</div>
           
            <hr  style="clear:both; visibility:hidden;"/>
            
            <div class="row">
                <div class="rental-data col-md-12">
                    <h2>Rental Data</h2>
                    <div class="data-desc">
                        <p><strong>Pickup Data:</strong> from <?php echo $pick_up_location;?> on <?php echo $temp_start_date_real;?></p>
                        <p><strong>Dropof data:</strong> to <?php echo $drop_off_location;?> on <?php echo $temp_end_date_real;?></p>
                        <p><strong>Rental duration:</strong> <?php echo $temp_duration;?> days in total</p>
                        <p><strong>Vehicle Type:</strong> <?php echo $car_show;?></p>
                        <p><strong>Rental Rate:</strong> <?php echo $base_cost;?> € in <?php echo $cat_name;?></p>
                        <p><strong>Total Cost:</strong> <?php echo $total_cost;?> € <?php if ($email_extras!=''){echo '(including '.$email_extras.')';}?>
						<p><strong>Pre-payment:</strong> <?php echo $pre_payment;?> € </p>
                   </div>
                </div>
             </div>  
                
			<div class="carform mar-top25">

				<p class="head">Your Request was <strong>Cancelled</strong>.<br />

				<?php
				if($post_DIGEST==$digest)
				{
				?>
					<span class="txt11">Your payment didn't reach a successful conclusion !!!<br>
					 There was an error while processing your payment.
					<br>
					<br>
					<strong class="txt11">Bank responce:</strong> <?php echo $_POST['message']; ?><br>
					</span>
				<?php
				}
				else
				{
				?>
					<span class="txt11">There was an error. Please contact us, providing us the following data:</span><br>
					
					Bank status: <?php echo $_POST['status'];?><br>
					Pay amount: <?php echo $_POST['orderAmount'];?><br>
					Bank message: <?php echo $_POST['message'];?><br>
					Pay method: <?php echo $_POST['payMethod'];?><br>
					Bank code: <?php echo $_POST['txId'];?><br>
					Payment reference number: <?php echo $_POST['paymentRef'];?><br>		
				<?php
				}
				?>
				
				
				<form action="payment.php" method="post" name="form2" class="form-cancel">
				<input name="session_id" type="hidden" value="<?php echo $session_id;?>">
				<input type="submit" name="Submit2"  class="btn_all col-sm-6" value="Click here to try again">
				</form>				
				
</p>

				
			<p>&nbsp;</p>
        	<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>

            </div>
           <hr  style="clear:both; visibility:hidden;"/>
        	
        </div><!--container-->
    </section>
    
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
    </body>
</html>
				