<?php 
$currentPage = $_SERVER['PHP_SELF'];
$index_seek = strpos($currentPage, 'index'); 
?>
<section class="grey-sec">
	<div class="container">
		<div class="row">
			
			<div class="address col-md-8 col-sm-8 col-xs-12" style="padding: 0px 20px;">
				<h4>Motonaxos</h4><p> Naxos island, 84300 Cyclades, Greece <br /><i class="fa fa-envelope"></i> <a href="mailto:info@motonaxos.com">info@motonaxos.com</a>
				<hr  style="clear:both; visibility:hidden; margin-top: 10px; margin-bottom: 10px;"/>
				<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
					<div class="col-md-4 col-sm-4 col-xs-12" style="padding: 0px">
					<strong>Office 1:<br /></strong> Court Square, Chora (Town),<br /> Tel.: +30 22850 23420,<br /> Fax: +30 22850 26393</div>
					<div class="col-md-4 col-sm-4 col-xs-12" style="padding: 0px"><strong>Office 2:</strong><br /> Agia Anna beach,<br /> Tel./fax: +30 22850 41404,<br /> Road service: +30 6944 622555</div>
					<div class="col-md-4 col-sm-4 col-xs-12" style="padding: 0px;"><strong>Office 3:</strong><br /> Main street Agios Prokopios,<br /> Tel./fax: +30 22850 41633,<br /> Road service: +30 6944 622555</div>
				</div>
		
		</div>	
		</div>	
	</div>	
</section>

<footer class="footer"><!--<a href="http://www.greeka.info/" target="_blank" rel="nofollow" class="greeka-logo"></a>-->

<a class="disclaimer-logo" target="_blank" href="http://www.codibee.com" 
    <?php if ($index_seek === false){  echo "rel='nofollow'"; }else{  echo "rel='designer'";} ?>
    ></a>
</footer>