<header>
	<nav class="navbar navbar-default navbargrey" role="navigation">
		<div class="container pos-rel">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div><!-- navbar-header -->
			<div class="collapse navbar-collapse" id="collapse" style="padding-right: 0px;padding-left:0px;">
				<ul class="menu nav navbar-nav navbar-left">
                	<li><a href="/index.htm">Home</a></li>
                    <li><a href="/offers.htm">Offers</a></li>
					<li><a href="/models.htm">Models</a></li>
					<li><a href="/terms.htm">Terms</a></li>
					<li><a href="/location.htm">Location</a></li>
					<li><a href="/about-us.htm">Our company</a></li>
					<li><a href="/news.htm">News</a></li>
					<li><a href="/contact.htm">Contact</a></li>
				</ul>
				<div class="social navbar-right"><a href="http://www.facebook.com/motonaxos" target="_blank" class="fb"><svg enable-background="new 0 0 167.657 167.657" version="1.1" viewBox="0 0 167.66 167.66" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="m83.829 0.349c-46.297 0-83.829 37.532-83.829 83.829 0 41.523 30.222 75.911 69.848 82.57v-65.081h-20.222v-23.42h20.222v-17.269c0-20.037 12.238-30.956 30.115-30.956 8.562 0 15.92 0.638 18.056 0.919v20.944l-12.399 6e-3c-9.72 0-11.594 4.618-11.594 11.397v14.947h23.193l-3.025 23.42h-20.168v65.653c41.476-5.048 73.631-40.312 73.631-83.154 0-46.273-37.532-83.805-83.828-83.805z" fill="#010002"/></svg></a></div>
			</div><!-- collapse navbar-collapse -->
		
		</div><!-- container -->
	</nav>
	
	<div class="headrow2">
		<div class="container pos-rel">
			<a href="/index.htm" title="MOTONAXOS"><img src="/images/logo.png"></a>
			<div class="europcar">Contact our rental specialist<br/>
				<span>+30 22850 23420</span></div>
		</div>
	</div>
</header>