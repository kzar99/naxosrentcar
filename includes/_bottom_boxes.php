    <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box ">
                    <a href="gallery.htm">
                    	<div class="pic" style="background-image: url(/images/thumb2.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;"><i class="fa fa-camera boxicon"></i>
                        <h4>Naxos <strong>Photogallery</strong></h4></div>
                    </a>
                </div><!--box-->
            </div>			

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box ">
                    <a href="guides.htm">
                    	<div class="pic" style="background-image: url(images/location1.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;">
                        <i class="fa fa-info-circle boxicon"></i>
                        <h4>Naxos <strong>Travel info</strong></h4></div>
                    </a>
                </div>
                <!--box-->
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box ">
                    <a href="guides.htm">
                    	<div class="pic" style="background-image: url(images/location2.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;">
                        <i class="fa fa-tags"></i>
                        <h4>Other <strong>Islands Deals</strong></h4></div>
                    </a>
                </div>
                <!--box-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box">
                    <a href="more.htm">
                    	<div class="pic" style="background-image: url(images/location3.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;">
                        <i class="fa fa-asterisk boxicon"></i>
                        <h4>Naxos <strong>plus</strong></h4></div>
                    </a>
                </div>
                <!--box-->
            </div>
    </div><!--row-->
