                <div class="col-md-4 col-lg-3 col-sm-12 col-xs-12 form-right" style="margin: 60px 0px 20px 0px;">
 
						<?php echo create_search_form();?>
                        <!--car-src-form-->
 
 
                            <div class="box-right white">
                                <h4 style="padding-top:15px;"><strong>ADVANTAGES</strong></h4>
                                <ul class="mar-top15">
                                    <li><i class="fa fa-check"></i> Direct  booking</li>
                                    <li><i class="fa fa-check"></i> No commission fees</li>
                                    <li><i class="fa fa-check"></i> All inclusive rates</li> 
                                    <li><i class="fa fa-check"></i> No hidden charges</li>
                                    <li><i class="fa fa-check"></i> Airport/Port service</li>
                                    <li><i class="fa fa-check"></i> Hotel delivery/ collection</li>
                                    <li><i class="fa fa-check"></i> Insurance Benefits</li>
                                    <li><i class="fa fa-check"></i> Unlimited kilometers</li>
                                    <li><i class="fa fa-check"></i> Free baby seats</li>
                                    <li><i class="fa fa-check"></i> Free additional driver</li>
                                    <li><i class="fa fa-check"></i> Road assistance</li>  
                                    <li><i class="fa fa-check"></i> New ,safe vehicles</li>
                                </ul>
                            </div>
                       		<!--box-->

                            <div class="box-right orange">
                                <div style="padding:0px;">
                                    <a href="/images/map-naxos.pdf" target="_blank" style="padding:0px;"><img src="/images/motonaxos-cover2015.jpg" class="image"/></a>
                                    <a href="/naxos.htm" style="padding:0px;"><h4 style="padding:30px;"><i class="fa fa-compass" style="margin-bottom:0px;"></i><br />explore<strong>naxos</strong></h4></a>
                                </div>
                            </div>
                        	<!--box-->

                            <div class="box-right orange">
                                <a href="/why-us.htm" style="padding:38px;">
                                <i class="fa fa-check-circle" style="font-size:60px;"></i>
                                <h4>Why choose<br /><strong>MotoNaxos?</strong></h4>
                                </a>
                            </div>
                        <!--box-->

                </div>
