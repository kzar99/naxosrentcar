<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,700&subset=greek-ext,latin-ext' rel='stylesheet' type='text/css'>
<link href="/css/custom.css" rel="stylesheet" type="text/css" media="all"/>
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.ui.timepicker.css">
<!--[if gte IE 9]>
<link rel="stylesheet" type="text/css" href="/css/ie9.css" />
<![endif]-->
<link rel="stylesheet" href="/css/supersized.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/css/supersized.shutter.css" type="text/css" media="screen" />
<script src="/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

<!--<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />-->
<link rel="stylesheet" href="/venobox/venobox.css" type="text/css" media="screen" />
