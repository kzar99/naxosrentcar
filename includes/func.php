<?php
function create_search_form()
{
	$hour = array("00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23");
	$min = array("00","30");
	
	$date_options = get_date_options();
	
	//
	// declare company id
	//
	$company=1;
	$km_cost = 0;
	$query100="SELECT * FROM main_company_list WHERE comp_id='$company' ";
	$result100 = mysql_query($query100)  or die(mysql_error().'<p>'.$query100.'</p>');
	while ($myrow100 = mysql_fetch_assoc($result100))
	{
	$temp_company_web=$myrow100['comp_name_db'];
	$temp_company_name=$myrow100['comp_name_title'];
	$temp_company_id=$myrow100['comp_id'];
	} // close connection 1
	
	
	$session_id="";
	srand((double)microtime()*1000000); // Seed the random number generator
	$session_id = md5(uniqid(rand())); // Construct the session ID
	
	
	
	//
	// dropdown options
	//
	$drop_down_options = '';
	
	$query1="SELECT * FROM main_special_location ORDER BY special_name ASC ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	$num_results1=mysql_num_rows($result1);
	while ($myrow1 = mysql_fetch_assoc($result1))
	{
		$drop_down_options .= '<option value="'.$myrow1['special_id'].'">'.$myrow1['special_name'].'</option>';
	}
	
	//
	// vehicle types
	//
	$vehicle_types_drop = '';
	
	$query1a="SELECT * FROM vehicles WHERE veh_code<>'' ORDER BY veh_order  ";
	$result1a = mysql_query($query1a)  or die(mysql_error().'<p>'.$query1a.'</p>');
	while ($myrow1a = mysql_fetch_assoc($result1a))
	{
		$vehicle_types_drop .= '<option value="'.$myrow1a['veh_code'].'">'.$myrow1a['veh_name'].'</option>';
	}	
	
	
	//
	// calculating starting dates (to show when page loads for the first time
	//
	$first_date_unix = mktime(0, 0, 0, date("m"), date("d")+$date_options['date_from_today'], date("Y"));
	$first_date = date("d-m-Y", $first_date_unix);
	
	$second_date_unix = mktime(0, 0, 0, date("m"), date("d")+$date_options['date_from_today']+$date_options['min_dur'], date("Y"));
	$second_date = date("d-m-Y", $second_date_unix);

	
	/*
	old discount code not in use anymore

		<div class="title">DISCOUNT CODE</div>
		<div class="src-row">
			<div class="label">Type your code <span class="tooltip tooltip-effect-1"><span class="tooltip-item"><i class="fa fa-question-circle"></i></span><span class="tooltip-content clearfix">Get <strong>10%</strong> discount!.<br>  --><a href="discounts.htm">Get your discount code now</a></span></span></span></div>
			<input type="text" name="discount_code" id="discount_code" class="input-curr">
		</div>
	
	*/
	

	$htmlData = '
	<div id="min_date_hidden" style="display:none;">'.$date_options['date_from_today'].'</div>
	<div id="min_dur_hidden" style="display:none;">'.$date_options['min_dur'].'</div>
	<form action="car-list.htm" method="post" name="form1" onSubmit="javascript:return vali2();">
	<input name="flag" type="hidden" value="submit">
	<input name="temp_company_web" type="hidden" value="'.$temp_company_web.'">
	<input name="temp_company_name" type="hidden" value="'.$temp_company_name.'">
	<input name="temp_company_id" type="hidden" value="'.$temp_company_id.'">
	<input name="session_id" type="hidden" value="'.$session_id.'">
	<input name="km_cost" type="hidden" value="'.$km_cost.'">
	
	<div class="car-src">
		<h2>Online booking</h2>
		<div class="title">
			<p>Pick Up</p>
			<p>Drop Off</p>
		</div>

		<div class="src-row">
			<input type="text" name="start_date" id="start_date" class="input-date col-l required" value="'.$first_date.'">
			<input type="text" name="end_date" id="end_date" class="input-date col-r required" value="'.$second_date.'">
		</div>

		<div class="src-row">
			<input placeholder="12:00" type="text" name="H1" id="H1" class="input-time col-l required hourField" value="09:00">
			<input placeholder="12:00" type="text" name="H2" id="H2" class="input-time col-r required hourField" value="09:00">
		</div>
		
		<div class="src-row">
			<div class="select"><select id="pickup-dd" class="pickup-dd" name="pickup"><option value="0" data-iconurl="images/location-icon.png" selected>Pick Up</option>'.$drop_down_options.'</select></div>
			<div class="select2"><select id="dropoff-dd" class="dropoff-dd" name="dropoff"><option value="0" data-iconurl="images/location-icon.png" selected>Drop Off</option>'.$drop_down_options.'</select></div>
		</div>

		
		<div class="src-row">
			<select id="car_type" name="car_type" class="select3 car_type">
				<option value="0" data-iconurl="images/vehicle-icon.png" selected="selected"> Vehicle Types </option>
				'.$vehicle_types_drop.'
			</select>
		</div>
		
		

		
		
		<div id="error"></div>
		<div class="src-row"><input type="SUBMIT" value="SEARCH VEHICLES" name="Submit" class="submit"></div>
		<hr  style="clear:both; visibility:hidden;"/>
	</div>
	</form>	
	';
	
return $htmlData;
}

function get_start_end_period($ms,$me)
{
// $ms = mystart
// $me = myend

$ms = trim($ms);
$me = trim($me);

$ms = (int)$ms;
$me = (int)$me;


/*
ms = 1594641601 
me = 1595224801
*/

$period_start = '';
$period_end = '';

	$query3="SELECT * FROM company_periods ORDER BY period_id";
	$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
	while ($row = mysql_fetch_assoc($result3))
	{
		$s1 = $row['start_date1'];
		$e1 = $row['end_date1'];

		$s2 = $row['start_date2'];
		$e2 = $row['end_date2'];
	



		if ( 
			( ($s1 <= $ms) && ($ms <= $e1) ) || 
			( ($s2 <= $ms) && ($ms <= $e2) ) 
		) { 
			$period_start = $row['period_name'];
		}
	

		if ( 
			( ($s1 <= $me) && ($me <= $e1) ) || 
			( ($s2 <= $me) && ($me <= $e2) ) 
		) { 
			$period_end = $row['period_name'];
		}

	}


	$a = $period_start . '@@' . $period_end;

return $a;
}


function get_cost_same_period($per_st,$temp_d,$cid)
{

// $per_st = temp_period_start
// $temp_d = temp_duration
// $cid = $cat_id
//
//
// calculate cost when duration belongs to same periods
	$query4="SELECT * FROM company_car_list_".$per_st." WHERE cat_id='$cid' ";
	$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
	while ($myrow4 = mysql_fetch_assoc($result4))
	{
		if ($temp_d<=7) // duration up to 7 days
		{
		$da = "day".$temp_d;
		$profit = $myrow4['profit'] +1; // profit in db = 0.10, then +1 for final 1.1 which means 110% or +10% extra cost
		$cost = round($myrow4[$da]*$profit,2);
		$pre_payment=round($cost*$myrow4['pososto'],2);
		}
		else // if duration >7 days
		{
		$profit = $myrow4['profit'] +1; // profit in db = 0.10, then +1 for final 1.1 which means 110% or +10% extra cost
		$cost = round($temp_d * $myrow4['day8']*$profit,2);
		$pre_payment=round($cost*$myrow4['pososto'],2);
		}
	}
$a = $cost."@@".$pre_payment;
return $a;
}

function get_cost_diff_period($mst,$temp_d,$cid)
{
// $mst = mystart
// $temp_d = temp_duration
// $cid = $cat_id
//
//
// set all available periods count to 0
// then with a loop add 1 day to proper period
// multiply this by appropriate cost and add all costs
$low=0;
$medium=0;
$high=0;
$peak=0;
$other=0;
$temp_start = $mst; // first daty of rental


$cost_low = 0; 
$cost_medium = 0; 
$cost_high = 0; 
$cost_peak = 0; 
$cost_other = 0;

$pre_payment_low = 0;
$pre_payment_medium = 0;
$pre_payment_high = 0;
$pre_payment_peak = 0;
$pre_payment_other = 0;

	for ($i=1;$i<=$temp_d;$i++) 
	{
		$query5="SELECT * FROM company_periods ORDER BY period_id";
		$result5 = mysql_query($query5)  or die(mysql_error().'<p>'.$query5.'</p>');
		while ($myrow5 = mysql_fetch_assoc($result5))
		{
			if ($temp_start>=$myrow5['start_date1'] && $temp_start<=$myrow5['end_date1'] || $temp_start>=$myrow5['start_date2'] && $temp_start<=$myrow5['end_date2'] ) 
			{
			$temp_period = $myrow5['period_name'];
			$$temp_period = $$temp_period +1;	
			}
		} // close connection 5	
	$temp_start = $temp_start + 86400 ; // 86400 sec are 1 day duration so we increase the current day by one
	} // close for (check i for duration)

// now we have all the number of days in each period (low, medium, high, peak, other)
// multiply each number by the appropriate cost and add all costs

if ($low!=0) // if low season has days assigned
{
	$query6="SELECT * FROM company_car_list_low WHERE cat_id='$cid' ";
	$result6 = mysql_query($query6)  or die(mysql_error().'<p>'.$query6.'</p>');
	while ($myrow6 = mysql_fetch_assoc($result6))
	{
	$profit_low = $myrow6['profit'] +1; // profit in db = 0.10, then +1 for final 1.1 which means 110% or +10% extra cost
	
		if ($low<=7) // duration up to 7 days
		{
		$da_low = "day".$low;
		$cost_low = $myrow6[$da_low]*$profit_low;
		$pre_payment_low = round($cost_low*$myrow6['pososto'],2);
		}
		else // if duration >7 days
		{
		$cost_low = $low * $myrow6['day8']*$profit_low;
		$pre_payment_low = round($cost_low*$myrow6['pososto'],2);
		}
	
	}
}

if ($medium!=0) // if medium season has days assigned
{
	$query7="SELECT * FROM company_car_list_medium WHERE cat_id='$cid' ";
	$result7 = mysql_query($query7)  or die(mysql_error().'<p>'.$query7.'</p>');
	while ($myrow7 = mysql_fetch_assoc($result7))
	{
	$profit_medium = $myrow7['profit'] +1; // profit in db = 0.10, then +1 for final 1.1 which means 110% or +10% extra cost
	
		if ($medium<=7) // duration up to 7 days
		{
		$da_medium = "day".$medium;
		$cost_medium = $myrow7[$da_medium]*$profit_medium;
		$pre_payment_medium = round($cost_medium*$myrow7['pososto'],2);
		}
		else // if duration >7 days
		{
		$cost_medium = $medium * $myrow7['day8']*$profit_medium;
		$pre_payment_medium = round($cost_medium*$myrow7['pososto'],2);
		}
	
	}
}


if ($high!=0) // if high season has days assigned
{
	$query8="SELECT * FROM company_car_list_high WHERE cat_id='$cid' ";
	$result8 = mysql_query($query8)  or die(mysql_error().'<p>'.$query8.'</p>');
	while ($myrow8 = mysql_fetch_assoc($result8))
	{
	$profit_high = $myrow8['profit'] +1; // profit in db = 0.10, then +1 for final 1.1 which means 110% or +10% extra cost
	
		if ($high<=7) // duration up to 7 days
		{
		$da_high = "day".$high;
		$cost_high = $myrow8[$da_high]*$profit_high;
		$pre_payment_high = round($cost_high*$myrow8['pososto'],2);
		}
		else // if duration >7 days
		{
		$cost_high = $high * $myrow8['day8']*$profit_high;
		$pre_payment_high = round($cost_high*$myrow8['pososto'],2);
		}
	
	}
}

if ($peak!=0) // if peak season has days assigned
{
	$query9="SELECT * FROM company_car_list_peak WHERE cat_id='$cid' ";
	$result9 = mysql_query($query9)  or die(mysql_error().'<p>'.$query9.'</p>');
	while ($myrow9 = mysql_fetch_assoc($result9))
	{
	$profit_peak = $myrow9['profit'] +1; // profit in db = 0.10, then +1 for final 1.1 which means 110% or +10% extra cost
		if ($peak<=7) // duration up to 7 days
		{
		$da_peak = "day".$peak;
		$cost_peak = $myrow9[$da_peak]*$profit_peak;
		$pre_payment_peak = round($cost_peak*$myrow9['pososto'],2);
		}
		else // if duration >7 days
		{
		$cost_peak = $peak * $myrow9['day8']*$profit_peak;
		$pre_payment_peak = round($cost_peak*$myrow9['pososto'],2);
		}
	}
}

if ($other!=0) // if other season has days assigned
{
	$query10="SELECT * FROM company_car_list_other WHERE cat_id='$cid' ";
	$result10 = mysql_query($query10)  or die(mysql_error().'<p>'.$query10.'</p>');
	while ($myrow10 = mysql_fetch_assoc($result10))
	{
	$profit_other = $myrow10['profit'] +1; // profit in db = 0.10, then +1 for final 1.1 which means 110% or +10% extra cost
		if ($other<=7) // duration up to 7 days
		{
		$da_other = "day".$other;
		$cost_other = $myrow10[$da_other]*$profit_other;
		$pre_payment_other = round($cost_other*$myrow10['pososto'],2);
		}
		else // if duration >7 days
		{
		$cost_other = $other * $myrow10['day8']*$profit_other;
		$pre_payment_other = round($cost_other*$myrow10['pososto'],2);
		}
	
	}
}
$total = round($cost_low + $cost_medium + $cost_high + $cost_peak + $cost_other,2);
$pre_payment = round($pre_payment_low + $pre_payment_medium + $pre_payment_high + $pre_payment_peak + $pre_payment_other,2);

$a = $total."@@".$pre_payment;
return $a;
}



function get_car_stop_sales($start_date,$end_date)
{
$car_id_array = "0";

	$query10="SELECT car_id FROM stop_sales WHERE 
		(date_from BETWEEN '$start_date' AND '$end_date') OR 
		(date_to BETWEEN '$start_date' AND '$end_date') OR
		(date_from <= '$start_date' AND date_to >= '$end_date')
		";
	$result10 = mysql_query($query10)  or die(mysql_error().'<p>'.$query10.'</p>');
	while ($myrow10 = mysql_fetch_assoc($result10))
	{
	$car_id_array .= ",".$myrow10['car_id'];
	}

return $car_id_array;
}

function get_date_options()
{
	$date_options = array();
	$today = time();
	$period_name = 'low'; // default name of the first period

	$query3 = "
	SELECT period_name 
	FROM company_periods 
	WHERE 
	(start_date1<='$today' AND end_date1>='$today') OR 
	(start_date2<='$today' AND end_date2>='$today') 
	ORDER BY period_id
	";
	$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
	$myrow3 = mysql_fetch_assoc($result3);
	
	if ( isset($myrow3['period_name']) && $myrow3['period_name']!='' )
	{
		$period_name = $myrow3['period_name'];
	}
	
	$query4="SELECT * FROM company_date_options WHERE period='$period_name' LIMIT 1 ";
	$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
	$myrow4 = mysql_fetch_assoc($result4);
	
	$date_options['date_from_today'] = $myrow4['date_from_today'];
	$date_options['min_dur'] = $myrow4['min_dur'];
	$date_options['min_book_dur'] = $myrow4['min_book_dur'];
		
	
return $date_options;
}
?>
