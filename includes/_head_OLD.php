<header>
	<nav class="navbar navbar-default navbargrey" role="navigation">
		<div class="container pos-rel">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div><!-- navbar-header -->
			<div class="collapse navbar-collapse" id="collapse">
				<ul class="menu nav navbar-nav navbar-right">
                	<li><a href="/index.htm">Home</a></li>
                    <li><a href="/offers.htm">Offers</a></li>
					<li><a href="/models.htm">Models</a></li>
					<li><a href="/terms.htm">Terms</a></li>
					<li><a href="/location.htm">Location</a></li>
					<li><a href="/about-us.htm">Our company</a></li>
					<li><a href="/news.htm">News</a></li>
					<li><a href="/contact.htm">Contact</a></li>
				</ul>
			</div><!-- collapse navbar-collapse -->
		
		</div><!-- container -->
	</nav>
	
	<div class="headrow2">
		<div class="container pos-rel">
			<a href="/index.htm" title="MOTONAXOS"><img src="/images/logo.png"></a>
			<div class="europcar"><img src="/images/europcar-logo.gif"></div>
		</div>
	</div>
</header>