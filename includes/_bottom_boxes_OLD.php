	<div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="/gallery.htm">
                        <i class="fa fa-camera boxicon"></i>
                        <h4>Naxos <strong>Photogallery</strong></h4>
                    </a>
                </div><!--box-->
            </div>			

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="/guides.htm">
                        <i class="fa fa-info-circle boxicon"></i>
                        <h4>Naxos <strong>Travel info</strong></h4>
                    </a>
                </div>
                <!--box-->
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <div class="wrap">
                        <i class="fa fa-tags"></i>
                        <h4>Other <strong>Islands Deals</strong></h4>
                    </div>
                </div>
                <!--box-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="/more.htm">
                        <i class="fa fa-asterisk boxicon"></i>
                        <h4>Naxos <strong>plus</strong></h4>
                    </a>
                </div>
                <!--box-->
            </div>
    </div><!--row-->
