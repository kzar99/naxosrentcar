<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container terms">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                	
                  <h1>Termes Et <strong>Conditions</strong><p class="pull-right">Languages: <a href="terms.htm">EN</a> | <a href="terms-gr.htm">GR</a> | <a href="terms-de.htm">DE</a> | <a href="terms-fr.htm"><strong>FR</strong></a> | <a href="terms-it.htm">IT</a> | <a href="terms-ru.htm">RUS</a></p>
                  </h1>
	              
<h2>Règles et conditions pour la location d’automobile et de motocyclette </h2>
<p>Soyez les bienvenus chez Motonaxos.com, le système en ligne le plus vaste pour la location d’ automobiles et motocyclettes de Naxos. Nous proposons une vaste gamme d’automobiles de location de Naxos, des modèles de motocyclettes et ATV nouveaux et sûrs, la plus haute qualité chez les véhicules de location, un service VIP, tout comme d’excellents marchés lors de la location d’une automobile ou motocyclette. </p>

<h3>Annulation d’une police d‘assurance:</h3> 
<ul class="checklist2">
<li>L’ANNULATION SANS FRAIS DANS LES LIMITES DE: </li>
<li>48 heures avant le premier jour de location pour les catégories: 
Automobiles, scooteurs, ATV </li>
<li>5 jours avant la première journée de location pour:
Automobiles spéciales, motocyclettes et buggies</li>
</ul>

<p>Pour toute annulation effectuée après ces délais, le dépôt de garantie sera retenu. <br />
La compagnie de location a le droit d’annuler la réservation en cas d’imprécisions, comme par exemple des détails concernant le permis de conduire, la carte de credit, etc. et a le droit d’exiger le paiement du dépôt de garantie versé par avance. </p>

<p>POLITIQUE DES PRIX:
Nous garantissons les meilleurs prix et la meilleure couverture possible pour des véhicules de transport de la plus haute qualité avec un niveau élevé des prestations.</p>

<p>Attention: Les prix indiqués et les offres sont valables uniquement pour les réservations faites à partir de notre page directement.</p>

<h3>RÈGLES ET CONDITIONS POUR LA LOCATION DE VÉHICULES</h3> 
<p><strong>Equipements et services inclus dans le prix</strong><br />

<p>assistance routière chauffeur additionnel siège d’enfants (celui-ci devra être réservé à l’avance et confirmé par le bureau de location) numéro illimité de kilomètres livraison et retour du véhicule à l’aéroport, le port et les hôtels pendant les heures d’ouverture de nos bureaux (8:30-21:30). En dehors de ceux-ci un paiement complémentaire sera redevable. cartes routières casques</p>

<p><strong>TAXES: </strong>Toutes les taxes sont incluses dans le prix de base.<br />

<p><strong>Paiement:</strong> Les cartes de crédits suivantes sont acceptéеs: MasterCard/VISA.
Les taxes restantes pour la location du véhicule sont payables au moment de la réception du véhicule. Un dépôt de garantie est requis lors de chaque location. Des cartes de crédit sont acceptéеs.</p>

<p><strong>Restitution du véhicule: </strong><br />
Le client est tenu de restituer le véhicule à la date, heure et endroit indiqués dans le contrat et dans le même état technique. Le véhicule devra être restitué avec la même quantité du contenu du réservoir qu’ au moment de la réception.
Veuillez prendre en compte le fait que le combustible non utilisé ne sera pas remboursé.</p>

<h3>LOCATION D’AUTOMOBILE: </h3>
<strong>Exigences envers le conducteur:</strong><br />

<p>Age minimum du conducteur-23 ans pour les catégories A, B, C, M, F, mini spécial, E, D, et 27 ans pour les catégories U, K, L<br />

Permis de conduire: Le conducteur doit avoir un permis de conduire valable depuis au moins un an. <br />
Un passeport ou carte d’identité sont également requis.<br />

<strong>ATTENTION!</strong> Pour certains conducteurs qui ne sont pas citoyens de pays de l’UE, un permis de conduire international pourra être exigé. Si vous avez des doutes, merci de nous envoyer une copie de votre permis de conduire pour avoir notre avis.</p>

<p><strong>Assurances: Les assurances suivantes sont disponibles:</strong><br />

<strong>Responsabilité civile</strong> pour tous les véhicules: Avec la Responsabilité civile notre compagnie d’assurance couvrira en cas d’incident les préjudices que vous pourriez causer à un autre véhicule <strong>Responsabilité limitée en cas d’heurt de véhicule (Collision Damage Waiver)</strong>. Le locataire est assuré contre les préjudices causés en cas d’incident sur le véhicule loué pour un montant maximum de: Groupes А, В, C, M: 600  € (+TVA) Groupes D, E, F: 800 € (+TVA) Groupes U, L, spéciales mini: 1100 € (+TVA) Exonération de la responsabilité en cas de heurt de véhicule (SuperC.D.W. insurance) pour la plupart des automobiles (âge minimum requis du conducteur - 27 ans) Assurance Vol (TheftInsurance). Cette assurance couvre le véhicule pour le montant ci-dessus indiqué. Assurance vol et incendie (SuperTheftInsurance) sans taxe. Assurance personnelle Accident (PersonalAccidentInsurance) pour le conducteur du véhicule.</p>
<p><strong>ATTENTION:</strong> La couverture d’assurance n’est pas valable au cas où le conducteur serait sous l’effet d’alcool, toute sorte de substances stupéfiantes ou s’il aurait commis quelque infraction routière que ce soit. Le conducteur assume la responsabilité pour les infractions de stationnement ou pour celles commises durant la période de location. Il est formellement interdit que les véhuciles soient conduits sur du sable, sur des berges, au bord de la mer, dans des lacs asséchés ou off-road. Tous les véhicules (exception faite de jeeps appropriés) pourront être conduits uniquement sur des routes pavées ou bitumées. 
Veuillez noter qu’il n’existe pas de couverture pour les préjudices portés sur les pneus, les rétroviseurs, le châssis, clés perdues, panne de la boîte de vitesse.</p>

<p><strong>Prestations automobiles:</strong> <br />
Rails de toit: livrés après réservation, avec paiement additionnel et sur certains véhicules seulement. 
Dispositif GPS: pourra être ajouté après réservation et contre un paiement additionnel. 
Nettoyage d’automobile-Allergy free- pourra être effectué contre un paiement additionnel. </p>

<h3>LOCATION DE SCOOTEUR, MOTOCYCLETTE, ATV OU BUGGY</h3>

<p><strong>Exigences envers le conducteur: </strong>
Age minimum du conducteur- 21 ans 
Permis de conduire: le conducteur devra être titulaire de permis de conduire. Vous avez le droit de conduire un scooteur jusqu’à 150 m3, uniquement si les normes en vigueur dans votre pays le permettent. Pour une motocyclette il est nécessaire que vous possédiez un permis de conduire A. Pour les ATV et les buggies vous devez détenir un permis pour les voitures B. Un passeport et carte d’identité sont également requis. </p>
<p><strong>ATTENTION!</strong> Pour certains conducteurs qui ne sont pas citoyens d’un pays de l’UE, un permis de conduire international pourra être exigé. Si vous avez des doutes, merci de nous envoyer une copie de votre permis de conduire pour avoir notre avis. 
Assurances: Les assurances suivantes sont disponibles:</p>

<p><strong>Responsabilité civile pour tous les véhicules:</strong> Avec la Responsabilité civile notre compagnie d’assurance couvrira en cas d’accident les préjudices que vous pourriez causer à un autre véhicule -Assurance Vol (Theft Insurance). Cette assurance couvre le véhicule pour le montant ci-dessus indiqué. Assurance vol e incendie (SuperTheftInsurance) sans taxe. Responsabilité limitée lors d’heurt de véhicule (Collision Damage Waiver). Le locataire est assuré contre les préjudices causés sur le véhicule loué en cas d’incident pour un montant (à définir sur place) Assurance lors de crevaison de pneu (FlatTiresInsurance) l’assurance lors de crevaison de pneu couvre la réparation et le transport du véhicule sans frais complémentaires</p>
<p><strong>ATTENTION:</strong> La couverture d’assurance n’est pas valable au cas où le conducteur serait sous l’effet d’alcool, toute sorte de substances stupéfiantes ou s’il ait commis quelque infraction routière que ce soit. Le conducteur assume la responsabilité pour les infractions de stationnement ou pour celles commises durant la période de location. Il est formellement interdit que les véhuciles soient conduits sur du sable, sur des berges, au bord de la mer, dans des lacs asséchés ou off-road. Tous les véhicules (exception faite de jeeps appropriés) pourront être conduits uniquement sur des routes pavées ou bitumées.</p>

<p>Veuillez noter qu’il n’existe pas de couverture pour les préjudices portés sur les pneus, les rétroviseurs, le châssis, clés perdues, panne de la boîte de vitesse.</p>


		<center><img src="/images/horizontal.jpg" /></center>

                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				