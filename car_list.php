<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

include("includes/connection.php");
include("includes/func.php");

$date_options = get_date_options();



$error_msg = "";


/*
echo "<pre>";
print_r($_POST);
echo "</pre>";
exit();

Array
(
    [flag] => submit
    [temp_company_web] => 
    [temp_company_name] => motonaxos
    [temp_company_id] => 1
    [session_id] => 838c35e91c4a4446159201fa4300c8a8
    [km_cost] => 0
    [start_date] => 13-07-2020
    [end_date] => 20-07-2020
    [H1] => 15:00
    [H2] => 09:00
    [pickup] => 29
    [dropoff] => 29
    [car_type] => scooter
    [Submit] => SEARCH VEHICLES
)

*/

$session_id = $_POST['session_id'];
$pickup = $_POST['pickup'];
$dropoff = $_POST['dropoff'];
$temp_company_id = $_POST['temp_company_id'];

if ($session_id=="") { header("Location: index.htm"); }

if ($pickup=="" || $pickup=="aaa") {$pickup=3;}
if ($dropoff=="" || $dropoff=="aaa") {$dropoff=3;}



$car_type = $_POST['car_type'];
$car_show = 'Unknown car';
$query1a="SELECT * FROM vehicles WHERE veh_code='$car_type' LIMIT 1  ";
$result1a = mysql_query($query1a)  or die(mysql_error().'<p>'.$query1a.'</p>');
while ($myrow1a = mysql_fetch_array($result1a))
{
	$car_show = $myrow1a['veh_name'];
}	




if (isset($_POST['flag']) && $_POST['flag']=="submit")
{
	//
	// check submitted discount code
	//
	if ( isset($_POST['discount_code']) && trim($_POST['discount_code'])!="")
	{
		$query2="SELECT * FROM members WHERE discount_code='".mysql_real_escape_string(trim($_POST['discount_code']))."' LIMIT 1 ";
		$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
		if (mysql_num_rows($result2)>0)
		{
			$discount_code = trim($_POST['discount_code']);
		}
		else
		{
			$discount_code = "";
		}
	}
	else
	{
		$discount_code = "";
	}


	//
	// date/time calculations
	//


	$start = $_POST['start_date'];
	$end = $_POST['end_date'];
	
	$h1 = explode(":", $_POST['H1']);
	$h2 = explode(":", $_POST['H2']);
	
	$hour1 = $h1[0];
	$min1 = $h1[1];
	
	$hour2 = $h2[0];
	$min2 = $h2[1];

	$temp_start_date_db = $start."-".$hour1.":".$min1;
	$temp_end_date_db = $end."-".$hour2.":".$min2;
	

	$temp_start_date_real = $start." at ".$hour1.":".$min1;
	$temp_end_date_real = $end." at ".$hour2.":".$min2;
	
	$a = explode("-",$temp_start_date_db);
	$mystart = mktime ($hour1, $min1, 1, $a[1], $a[0], $a[2]);
	
	$b = explode("-",$temp_end_date_db);
	$myend = mktime ($hour2, $min2, 1, $b[1], $b[0], $b[2]);
	
	
	//
	// minimum day to allow the request t start
	//
	$first_date_unix = mktime(0, 0, 1, date("m"), date("d")+$date_options['date_from_today'], date("Y"));
	$first_date = date("d-m-Y", $first_date_unix);
	
	if ($mystart<$first_date_unix)
	{
	$error_msg .= "You are not allow to rent a car earlier than ".$first_date.".<br>Please go back and select some other dates.<br>";
	}
	

	
	if ($myend<$mystart)
	{
		$error_msg .= "Drop off date is earlier than the pickup date.<br>Please go back and select some other dates.<br>";
	}
	
	$dur = $myend - $mystart;

	
	if ($hour2>$hour1) { $temp_duration = floor($dur/86400)+1; }
	else { $temp_duration = floor($dur/86400); }	


	//
	// minimum allowed duration of booking based on season
	//
	$min_book_dur = 0;
	
	$query3="SELECT period_name FROM company_periods WHERE (start_date1<='$mystart' AND end_date1>='$mystart') OR (start_date2<='$mystart' AND end_date2>='$mystart') ORDER BY period_id";
	$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
	while ($myrow3 = mysql_fetch_array($result3))
	{
	$period_name = $myrow3['period_name'];
	}
	
	$query4="SELECT min_book_dur FROM company_date_options WHERE period='$period_name' LIMIT 1 ";
	$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
	$myrow4 = mysql_fetch_array($result4);

	$min_book_dur = $myrow4['min_book_dur'];
	

	if ($temp_duration<$min_book_dur && $min_book_dur>0)
	{
		$error_msg .= "There is a minimum booking of ".$min_book_dur." days.<br>Please go back and fix this.<br>";
	}


	// select periods from db and do some basic calculations to determine the period the user rents his car
	$b = get_start_end_period($mystart,$myend);

	$b1 = explode("@@",$b);
	$temp_period_start = $b1[0];
	$temp_period_end = $b1[1];

//
// area/locations calculations
//	

	$query1="SELECT * FROM main_special_location WHERE special_id='$pickup' ";
	$result1 = mysql_query($query1) or die(mysql_error().'<p>'.$query1.'</p>');
	while ($myrow1 = mysql_fetch_array($result1))
	{
	$pick_up_location = $myrow1['special_name'];
	$pick_up_location_id = $pickup;
	}

	$query1="SELECT * FROM main_special_location WHERE special_id='$dropoff' ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($myrow1 = mysql_fetch_array($result1))
	{
	$drop_off_location = $myrow1['special_name'];
	$drop_off_location_id = $dropoff;
	}

	$query1="SELECT * FROM rental WHERE session_id='$session_id' ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	$num_results_distance = mysql_num_rows($result1);
	if ($num_results_distance>0)
	{
	}
	else
	{
		$sql997 = "INSERT INTO  rental (session_id) VALUES ('".trim($session_id)."') ";
		$result777 = mysql_query($sql997)  or die(mysql_error().'<p>'.$sql997.'</p>');					
	}
	$sql996 = "UPDATE  rental SET 
	affiliate_id='1',
	start_date='$temp_start_date_db', 
	end_date='$temp_end_date_db', 
	pickup_area='$pickup', 
	dropoff_area='$dropoff', 
	company='$temp_company_id', 
	discount_code='$discount_code' 
	WHERE session_id='$session_id' ";
	$result996 = mysql_query($sql996)  or die(mysql_error().'<p>'.$sql996.'</p>');		
}

$query1="SELECT * FROM rental WHERE session_id='$session_id' ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($myrow1 = mysql_fetch_array($result1))
{
$temp_start_date_real = $myrow1['start_date'];
$temp_end_date_real = $myrow1['end_date'];
$pick_up_location_id = $myrow1['pickup_area'];
$drop_off_location_id = $myrow1['dropoff_area'];
$company_id = $myrow1['company'];
$base_cost = $myrow1['base_cost'];
$category = $myrow1['category'];
$discount_code = $myrow1['discount_code'];
}

//
// category data
//
$query2="SELECT * FROM company_category WHERE category='$category' ";
$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
while ($myrow2 = mysql_fetch_array($result2))
{
$cat_id = $myrow2['cat_id'];
$cat_name = $myrow2['category'];
$cat_description = $myrow2['description'];
$cat_cars = $myrow2['cat_cars'];
$cat_num_cars = $myrow2['num_cars'];
$cat_image = $myrow2['image'];	
$car_type = $myrow2['car_type'];
}

//
// area/locations calculations
//	
	$query1="SELECT * FROM main_special_location WHERE special_id='$pick_up_location_id' ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($myrow1 = mysql_fetch_array($result1))
	{
	$pick_up_location = $myrow1['special_name'];
	}

	$query11="SELECT * FROM main_special_location WHERE special_id='$drop_off_location_id' ";
	$result11 = mysql_query($query11)  or die(mysql_error().'<p>'.$query11.'</p>');
	while ($myrow11 = mysql_fetch_array($result11))
	{
	$drop_off_location = $myrow11['special_name'];
	}



//
// date/time calculations
//
	$a = explode("-",$temp_start_date_real);
	$h1 = explode(":",$a[3]);
	$hour1 = $h1[0];
	$mystart = mktime ($h1[1], $h1[1], 0, $a[1], $a[0], $a[2]);
	
	$b = explode("-",$temp_end_date_real);
	$h2 = explode(":",$b[3]);
	$hour2 = $h2[0];
	$myend = mktime ($h2[0], $h2[1], 0, $b[1], $b[0], $b[2]);
	
	$dur = $myend - $mystart;
	
	if ($hour2>$hour1) { $temp_duration = floor($dur/86400)+1; }
	else { $temp_duration = floor($dur/86400); }	

	$b = get_start_end_period($mystart,$myend);
	$b1 = explode("@@",$b);
	$temp_period_start = $b1[0];
	$temp_period_end = $b1[1];



$ex_type = 'C'; // default



$car_show = 'Unknown car';
$query1a="SELECT * FROM vehicles WHERE veh_code='$car_type' LIMIT 1  ";
$result1a = mysql_query($query1a)  or die(mysql_error().'<p>'.$query1a.'</p>');
while ($myrow1a = mysql_fetch_array($result1a))
{
	$car_show = $myrow1a['veh_name'];
}	



	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container">
 		
        	
        	<div class="float-l mar-top25 color474">
				<h1 class="size50">STEP <strong class="rb-bold">01</strong></h1>
				<p align="right">Select your car</p>
			</div>
 
         	<div class="float-r mar-top25 color474 goback">
				<p>You can: <a href="index.htm">Make a new search</a></p>
			</div>
           
            <hr  style="clear:both; visibility:hidden;"/>
            
            <div class="row">
                <div class="rental-data col-md-12">
                    <h2>Rental Data</h2>
                    <div class="data-desc">
                        <p><strong>Pickup Data:</strong> From <?php echo $pick_up_location;?> on <?php echo $temp_start_date_real;?></p>
                        <p><strong>Drop off data:</strong> To <?php echo $drop_off_location;?> on <?php echo $temp_end_date_real;?></p>
                        <p><strong>Rental duration:</strong> <?php echo $temp_duration;?> days in total</p>
                        <p><strong>Vehicle Type:</strong> <?php echo $car_show;?></p>
                   </div>
                </div>
             </div>  
                
			<div class="carlist mar-top25">
			
			<?php
			if ($error_msg!="")
			{
			?>
			<div align="center">
				<span style=" color:#FF0000; font-weight:bold;"><?php echo $error_msg;?></span>
			</div>
			<?php
			}
			else
			{
			?>			

                <ul>	
				<?php
				$car_id_stop_sales = get_car_stop_sales($mystart, $myend);
				$discount = 0;
				
				$query101="SELECT * FROM members WHERE discount_code='$discount_code' LIMIT 1 ";
				$result101 = mysql_query($query101)  or die(mysql_error().'<p>'.$query101.'</p>');
				$member_found = mysql_num_rows($result101);
				if ($member_found>0)
				{
					$query102="SELECT * FROM member_discount WHERE id=1 ";
					$result102 = mysql_query($query102)  or die(mysql_error().'<p>'.$query102.'</p>');
					$myrow102 = mysql_fetch_array($result102);
					
					$discount = (100-$myrow102['discount'])/100;
				}

				$query200="SELECT * FROM company_category WHERE car_type='$car_type' AND cat_id NOT IN ($car_id_stop_sales) ORDER BY category ";
				$result200 = mysql_query($query200)  or die(mysql_error().'<p>'.$query200.'</p>');
				$vehicles_found = mysql_num_rows($result200);
				if ($vehicles_found>0)
				{
					while ($myrow200 = mysql_fetch_array($result200))
					{
					$cat_id = $myrow200['cat_id'];			
				?>
				
					<li class="col-md-12 col-xs-12 col-sm-12 col-lg-12" style="background-color:#ffffff;">
                    <div class="row">
						<div class="col-md-3 col-sm-4 col-lg-3 col-xs-12 car-img"><img src="car_images/<?php echo $myrow200['image'];?>" alt="Car Rental Category <?php echo $myrow200['category'];?>" name="car image of <?php echo $myrow200['category'];?>" /></div>
						<div class="col-md-6 col-sm-8 col-lg-6 col-xs-12 car-desc">
							
							<p><span class="title"><strong><?php echo $myrow200['cat_cars'];?></strong></span> | <?php echo $myrow200['category'];?></p>
							<hr  style="clear:both; visibility:hidden; margin:0px;"/>	
							<?php if ($myrow200['mileage']!=''){?><div class="carInfoPart"><img src="/images/icon-milage.png" title="Mileage"/> <?php echo $myrow200['mileage'];?></div><?php } ?>
							<?php if ($myrow200['min_age']>0){?><div class="carInfoPart"><img src="/images/icon-age.png" title="Minimum Age"/> <?php echo $myrow200['min_age'];?> years</div><?php } ?>
							<?php if ($myrow200['transmission']!=''){?><div class="carInfoPart"><img src="/images/icon-transmission.png" title="Transmission"/> <?php echo ucfirst($myrow200['transmission']);?></div><?php } ?>
							<?php if ($myrow200['doors']>0){?><div class="carInfoPart"><img src="/images/icon-cardoor.png" title="Doors"/> <?php echo $myrow200['doors'];?></div><?php } ?>
							<?php if ($myrow200['adults']>0){?><div class="carInfoPart"><img src="/images/icon-adults.png" title="Adults"/> <?php echo $myrow200['adults'];?></div><?php } ?>
							<?php if ($myrow200['children']>0){?><div class="carInfoPart"><img src="/images/icon-children.png" title="Children"/> <?php echo $myrow200['children'];?></div><?php } ?>
							<?php if ($myrow200['large_bag']>0){?><div class="carInfoPart"><img src="/images/icon-lug_lar.png" title="Large Luggage"/> <?php echo $myrow200['large_bag'];?></div><?php } ?>
							<?php if ($myrow200['small_bag']>0){?><div class="carInfoPart"><img src="/images/icon-lug.png" title="Small Luggage"/> <?php echo $myrow200['small_bag'];?></div><?php } ?>
							
							<?php if ($myrow200['has_ac']=='Y'){?><div class="carInfoPart"><img src="/images/icon-aircondition.png" title="Air Conditioning"/> Yes</div><?php } ?>
							<?php if ($myrow200['has_music']=='Y'){?><div class="carInfoPart"><img src="/images/icon-music.png" title="Music/Radio player"/> Yes</div><?php } ?>
							<hr  style="clear:both; visibility:hidden;margin:0px;"/>	
							<p class="desc"><img src="/images/icon-description.png" title="Description"/> <?php echo $myrow200['description'];?></p>
							
						</div>
						<div class="col-md-3 col-sm-12 col-lg-3 col-xs-12 car-rate">
							<?php
							////////////////////////////////////////////////////
							/// find if all duration belongs to same period or not
							///////////////////////////////////////////////////

							if ($temp_period_start!="" && $temp_period_end!="")	
							{
							
								if ($temp_period_start==$temp_period_end) // start and end dates belong to same period
								{
									$a = get_cost_same_period($temp_period_start,$temp_duration,$cat_id);
									$a1 = explode("@@",$a);
									$cost = $a1[0];
									$cost_basic = $a1[0];
									$pre_payment = $a1[1];
								}
								else // if start period and end period don't match, we must find how many days from duration in each period
								{
									$a = get_cost_diff_period($mystart,$temp_duration,$cat_id);
									$a1 = explode("@@",$a);
									$cost = $a1[0];
									$cost_basic = $a1[0];
									$pre_payment = $a1[1];
								} // end if of same periods check

								/// end calculation of base price
								//
								//
								//
								// increasing basic cost depending on pickup/dropoff locations/areas
								//
								$query2="SELECT * FROM main_special_location WHERE special_id='$pick_up_location_id' ";
								$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
								while ($myrow2 = mysql_fetch_array($result2))
								{
								$pickup_location_cost = $myrow2['special_cost'];
								}
								$cost = $cost + $pickup_location_cost; // increased by fixed amount based on pickup special location
								$pre_payment = $pre_payment + $pickup_location_cost;	
								//
								// drop off in selected list of locations
								//
								$query21="SELECT * FROM main_special_location WHERE special_id='$drop_off_location_id' ";
								$result21 = mysql_query($query21)  or die(mysql_error().'<p>'.$query21.'</p>');
								while ($myrow21 = mysql_fetch_array($result21))
								{
								$dropoff_location_cost = $myrow21['special_cost'];
								}
								$cost = $cost + $dropoff_location_cost; // increased by fixed amount based on dropoff special location
								$pre_payment = $pre_payment + $dropoff_location_cost;	
							
								
								if ($cost_basic>0)
								{
							?>	
								<span><?php echo $cost; ?> &#8364;</span> 
							<?php
								}
							}
							else
							{
								echo '<span>N/A</span>';
							}
							?>
							<br />
							
							<?php
							$discount_cost=$cost-($cost*0.05);
							if ($temp_period_start!="" && $temp_period_end!="" && $cost_basic>0)	
							{
							?>
								<form name="form1<?php $cat_id; ?>" method="post" action="car-extra.htm">
								<input name="flag" type="hidden" value="insert">
								<input name="session_id" type="hidden" value="<?php echo $session_id;?>">
								<input name="cat" type="hidden" value="<?php echo $myrow200['category'];?>">
								<input name="cost" type="hidden" value="<?php echo $cost;?>">
								<input name="prepay" type="hidden" value="<?php echo $pre_payment;?>">
								<input name="temp_period_start" type="hidden" value="<?php echo $temp_period_start;?>">
								<input name="temp_period_end" type="hidden" value="<?php echo $temp_period_end;?>">
								<input name="temp_company_id" type="hidden" value="<?php echo $temp_company_id;?>">								
								<input name="original_cost" type="hidden" value="<?php echo $cost;?>">
								<div align="center"><input class="booknow col-sm-6" type="submit" name="Submit" value="Book/Request Now"></div>
								</form>
							<?php
							}
							else
							{
							?>	
							To book this vehicle ,please contact us by e-mail.

							<?php
								/*
								$query22="SELECT * FROM company_periods WHERE period_id='1' ";
								$result22 = mysql_query($query22)  or die(mysql_error().'<p>'.$query22.'</p>');
								while ($myrow22 = mysql_fetch_array($result22))
								{
								$start1 = $myrow22['start_date1'];
								$start2 = $myrow22['start_date2'];
								}	
									if ($start1<=$start2) {$low_start1 = $start1;}
									else {$low_start1 = $start2;}
						
								$query23="SELECT * FROM company_periods WHERE period_id='2' ";
								$result23 = mysql_query($query23)  or die(mysql_error().'<p>'.$query23.'</p>');
								while ($myrow23 = mysql_fetch_array($result23))
								{
								$start1 = $myrow23['start_date1'];
								$start2 = $myrow23['start_date2'];
								}	
									if ($start1<=$start2) {$low_start2 = $start1;}
									else {$low_start2 = $start2;}	
								
								if ($low_start1<=$low_start2) { echo date("d-m-Y",$low_start1); }
								else { echo date("d-m-Y",$low_start2); }
								*/
							}
							?>							
						</div>
						<hr  style="clear:both; visibility:hidden;"/>
                        </div>
					</li>
		   <?php	
				} // close connection 200		   
			}
			else // if no cars found in selected dates/category
			{
			?>
			   </ul>
			<div align="center">
			No available vehicles found in the selected locations/dates/vehicle type.<br>
			Please <a href="javascript:history.back()"><u>go back</u></a> and try a diferent combination.<br>
			</div>
            
			<?php
			} // end if ($vehicles_found>0)		   
		   ?>
           <hr  style="clear:both; visibility:hidden; "/>
				<form action="car-list.htm" method="post" class="carlist-form">
				<input name="flag" type="hidden" value="<?php echo $_POST['flag'];?>">
				<input name="temp_company_web" type="hidden" value="<?php echo $_POST['temp_company_web'];?>">
				<input name="temp_company_name" type="hidden" value="<?php echo $_POST['temp_company_name'];?>">
				<input name="temp_company_id" type="hidden" value="<?php echo $_POST['temp_company_id'];?>">
				<input name="session_id" type="hidden" value="<?php echo $_POST['session_id'];?>">
				<input name="km_cost" type="hidden" value="<?php echo $_POST['km_cost'];?>">
				<input name="start_date" type="hidden" value="<?php echo $_POST['start_date'];?>">
				<input name="H1" type="hidden" value="<?php echo $_POST['H1'];?>">
				<input name="M1" type="hidden" value="<?php echo $_POST['M1'];?>">
				<input name="pickup" type="hidden" value="<?php echo $_POST['pickup'];?>">
				<input name="end_date" type="hidden" value="<?php echo $_POST['end_date'];?>">
				<input name="H2" type="hidden" value="<?php echo $_POST['H2'];?>">
				<input name="M2" type="hidden" value="<?php echo $_POST['M2'];?>">
				<input name="dropoff" type="hidden" value="<?php echo $_POST['dropoff'];?>">
				<input name="car_type" type="hidden" value="<?php echo $_POST['car_type'];?>">
				<input name="discount_code" type="hidden" value="<?php echo $discount_code;?>">
				<div class="col-lg-6 col-md-6 col-sm-6 txt"><strong>Didn't find what you were looking for? Try a different vehicle type:</strong></div>
				<div class="col-lg-6 col-md-6 col-sm-6">
				<select name="car_type" class="car_type2">
				<?php
				$q1a="SELECT * FROM vehicles ORDER BY veh_order   ";
				$r1a = mysql_query($q1a)  or die(mysql_error().'<p>'.$q1a.'</p>');
				while ($ro1a = mysql_fetch_array($r1a))
				{
				?>
				<option value="<?php echo $ro1a['veh_code'];?>"><?php echo $ro1a['veh_name'];?></option>
				<?php
				}	
				?>				
				</select>
				<input name="submit1" type="submit" value="Find" class="findnow"></div>
                
                <hr  style="clear:both; visibility:hidden;"/>
				</form>	
				<br />			   
		   <?php
		   }  // end if ($error_msg!="")
		   ?>
            </div>
           <hr  style="clear:both; visibility:hidden;"/>
        	
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				
