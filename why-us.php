<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Why <strong>choose us</strong></h1>
					<h4>Book with Motonaxos!!!</h4>
	                <ul class="checklist">
                    
<li> <strong>More than 30 years of knowledge and experience</strong> at the business community of car and bike rental vehicles in Naxos Island. </li> 

<li> <strong>Your safety is our primary goal</strong>, with continuous renovations of our car models, excellent service and the largest insurance coverages in the market. Offering also extra comforts such as baby seats, helmets free of charge. </li>

<li> <strong>We are always by your side</strong>, wherever and whenever you many need us. In case of damage or accident, call 0030 694 46 22 555 and a member of our specialized staff will be with you. </li>

<li> <strong>Service of high standards with</strong> our well-trained staff. Always available to assist you and help you find the most suitable vehicle for you and provide you a detailed map with tips of Naxos. </li>

<li> <strong>We take care all of your needs</strong> by offering excellent modern vehicles. You can choose among the biggest range of rental cars and motorbikes in Naxos island.</li>

<li> <strong>We come wherever you are</strong> on the island (Naxos hotels, Naxos airport, Naxos port). </li>

<li> <strong>We understand you</strong> and offer the best vehicles to hire in the market of Naxos at competitive prices and services. No hidden charges and the kilometers are free of charge. </li>

              </ul>      
                  
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				