<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Welcome</h1>
	                <p>Motonaxos offers great service and low rates for car and motorbike rental in Naxos Island! Rent a car in Naxos Island with the top car rental company! Compare car rental prices, Book online your car fast and easy, Rent a quad or a buggy in Naxos, hire a bike or motorcycle explore Naxos Island!  Safe, new models, Low rental prices.</p>
					<h4>Rent a car in Naxos Island with the top car rental company!</h4>
	                <p>Do you want to rent car in Naxos Island? Hire a car with the biggest car rental company and travel around Naxos, explore the elegant villages and the beautiful beaches. Organize your holidays on Naxos, rent a car at the port of Naxos and make your transportation easy and comfortable. Enjoy your vacations; don’t waste time by planning your car rental at Naxos airport. Check our car rental prices, see all the offers for online bookings and make the best car rental deal in Naxos by choosing between a wide range of vehicles.  Book a 4x4 jeep and feel the adventure of the rugged roads or a convertible car and visit the long sandy beaches. If you travel with friends, you can rent a mini bus which will gather you all together. Manual transmission or automatic car, luxury cabriole or economy car, open top jeep or full size family car, choose online through our website and we promise to deliver you the best car rental experience in Naxos island!</p>
					<h4>Rent a quad bike, buggy or scooter in Naxos Island and live the adventure!</h4>
	                <p>For all those who love adventure we provide a big selection of ATVs for rent. We offer an exciting and unique way of exploring our natural beauties of Naxos via our 4 wheel motor bikes. What better way to discover the dusty roads and hidden paths of the island than by renting an ATV? Motonaxos offers a wide variety of quads for rent in Naxos. Our Sporty design ATV models with automatic transition, luggage box, and the extra comfortable seat, make your ride around Naxos Island an unforgettable experience!</p>
                    <p>Want to live an unforgettable experience? Rent a buggy in Naxos Island and feel the adventure! Our Buggies offer you the freedom of a quad and the comfort of a car. With bug riders you can visit many popular beaches and historical sites and attractions around the island of Naxos. We have all types of buggies for rent which make your transportation in Naxos fun.</p>
                    <p>If you are looking for an economic way to travel around Naxos Island, you can rent a motorbike, a scooter or a moped. Economical, easy to park, the motorbike or the scooter are great for riding through the beautiful villages of Naxos or to enjoy your time around the beaches of Naxos. Motonaxos is one of the leading companies at the motorbike rental in Naxos island. In our rental agency you can hire scooters with automatic transition or you can hire off round motorbikes with large displacement. We provide you with safety motorbike helmets and road maps of Naxos and give all travel tips to discover each secret trail. Rent a bike or a scooter online and take advantage of our special motorbike rental offers!</p>
                    <!--<ul class="checklist"><li>Lorem ipsum dolor sit amet</li><li>Lorem ipsum dolor sit amet</li><li>Lorem ipsum dolor sit amet</li></ul>
                    <div class="gallery mar-top20">
                    <ul class="gallerylist">
                    	<li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                        <li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                        <li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                    	<li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                    	<li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                        <li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                        <li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                    	<li class="col-md-4 col-lg-3 col-sm-4 col-xs-12"><a href="#"><img src="/images/car1.jpg" /></a></li>
                    </ul>
                    
                    </div>-->
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				