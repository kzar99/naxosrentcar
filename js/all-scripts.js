//slider car carousel
$('#slider-stage').carousel('#previous', '#next');

$('input,textarea').focus(function()
{
   $(this).data('placeholder',$(this).attr('placeholder'))
   //$(this).attr('placeholder','');
});
$('input,textarea').blur(function(){
   $(this).attr('placeholder',$(this).data('placeholder'));
});

function isValidEmailAddress(emailAddress) {
	//var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);

	var pattern=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	
	return pattern.test(emailAddress);
}




$(document).ready(function() {
		$('.gal').venobox({bgcolor: '#000'}); 
		/* custom settings */
		$('.map1').venobox({
			framewidth: '550px',        // default: ''
			frameheight: '648px',       // default: ''
			titleattr: 'data-title',    // default: 'title'
			bgcolor: '#000',         // default: '#fff'
			numeratio: true,            // default: false
			infinigall: false            // default: false
		});
		$('.map2').venobox({
			framewidth: '550px',        // default: ''
			frameheight: '751px',       // default: ''
			titleattr: 'data-title',    // default: 'title'
			bgcolor: '#000',         // default: '#fff'
			numeratio: true,            // default: false
			infinigall: false            // default: false
		});
		$('.map3').venobox({
			framewidth: '550px',        // default: ''
			frameheight: '750px',       // default: ''
			titleattr: 'data-title',    // default: 'title'
			bgcolor: '#000',         // default: '#fff'
			numeratio: true,            // default: false
			infinigall: false            // default: false
		});
		$('.map4').venobox({
			framewidth: '550px',        // default: ''
			frameheight: '648px',       // default: ''
			titleattr: 'data-title',    // default: 'title'
			bgcolor: '#000',         // default: '#fff'
			numeratio: true,            // default: false
			infinigall: false            // default: false
		});
		$('.map5').venobox({
			framewidth: '550px',        // default: ''
			frameheight: '515px',       // default: ''
			titleattr: 'data-title',    // default: 'title'
			bgcolor: '#000',         // default: '#fff'
			numeratio: true,            // default: false
			infinigall: false            // default: false
		});
		$('.map6').venobox({
			framewidth: '550px',        // default: ''
			frameheight: '550px',       // default: ''
			titleattr: 'data-title',    // default: 'title'
			bgcolor: '#000',         // default: '#fff'
			numeratio: true,            // default: false
			infinigall: false            // default: false
		});
});

$(".pickup-dd").dropkick();
$(".dropoff-dd").dropkick();
$(".car_type").dropkick();
$(".cartype-dd").dropkick();

function vali2()
{
	var temp

	if (document.form1.start_date.value=="")
	{
	//alert("Please select the pickup date")
	$('#error').html("Please select the pickup date")
	return false;
	}
	

	if (document.form1.end_date.value=="")
	{
	//alert("Please select the dropoff date")
	$('#error').html("Please select the dropoff date")
	return false;
	}


	if (document.form1.pickup.value==0)
	{
	//alert("Please select the pickup location")
	$('#error').html("Please select the pickup location")
	return false;
	}


	if (document.form1.dropoff.value==0)
	{
	//alert("Please select the dropoff location")
	$('#error').html("Please select the dropoff location")
	return false;
	}

	if (document.form1.car_type.value==0)
	{
	//alert("Please select your vehicle type")
	$('#error').html("Please select your vehicle type")
	return false;
	}


}

function DatePickedFrom(durDays, fromField, toField)	
{
	var arrivalDate = $("#"+fromField).datepicker("getDate"); // the "From" date	
	var minDepartureDate = new Date(arrivalDate);
	var maxDepartureDate = new Date(arrivalDate);
	var minAllowedDate = new Date(arrivalDate);
	minDepartureDate.setDate(arrivalDate.getDate() + durDays); // +xxx days from selected "From" date 
	maxDepartureDate.setDate(arrivalDate.getDate() + 365); // +365 days from seleced "From: date
	minAllowedDate.setDate(arrivalDate.getDate() + durDays); // allow minimum of +X days from "From" date		
	$("#"+toField).datepicker('option', 'minDate', minAllowedDate);
	$("#"+toField).datepicker('option', 'maxDate', maxDepartureDate);
};

$(document).ready(function()
{
	var noOfMonths ;
	var today = new Date();
	if($(window).width() < 630)
	{
		noOfMonths = 1;
	}
	else
	{
		noOfMonths = 2;
	}

	$( "#start_date" ).datepicker({
		minDate: parseInt($("#min_date_hidden").html()),
		showOn: "both",
		numberOfMonths: noOfMonths,
		stepMonths: noOfMonths,
		dateFormat: "dd-mm-yy",
		onClose:function(dateText, inst)
		{				
			$("#end_date").focus().trigger('open');
		},
		onSelect: function(date)
		{            
			var date1 = $('#start_date').datepicker('getDate');           
			var date = new Date( Date.parse( date1 ) ); 
			date.setDate( date.getDate() + 1 );        
			var newDate = date.toDateString(); 
			newDate = new Date( Date.parse( newDate ) );                      
			$('#end_date').datepicker("option","minDate",newDate);      
			
			
			DatePickedFrom(parseInt($("#min_dur_hidden").html()), 'start_date', 'end_date'); // number of days to create default duration
			      
		}
	});
	
	$( "#end_date" ).datepicker({
		showOn: "both",
		numberOfMonths: noOfMonths,
		stepMonths: noOfMonths,
		maxDate:31,
		dateFormat: "dd-mm-yy"
	});
	
	$("#start_date").attr( 'readOnly' , 'true' );
	$("#end_date").attr( 'readOnly' , 'true' );

	$( "#H1, #H2" ).timepicker({ 
		timeSeparator: ':',
		defaultTime: '09:00', 
		showOn: 'focus',  
		showPeriodLabels: false, 
		minutes: { interval: 30 },
	});
	$("#H1, #H2").attr( 'readOnly' , 'true' );
});
