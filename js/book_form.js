function vali2()
{
	var temp

	if (document.form1.start_date.value=="")
	{
	//alert("Please select the pickup date")
	$('#error').html("Please select the pickup date")
	return false;
	}
	

	if (document.form1.end_date.value=="")
	{
	//alert("Please select the dropoff date")
	$('#error').html("Please select the dropoff date")
	return false;
	}


	if (document.form1.pickup.value==0)
	{
	//alert("Please select the pickup location")
	$('#error').html("Please select the pickup location")
	return false;
	}


	if (document.form1.dropoff.value==0)
	{
	//alert("Please select the dropoff location")
	$('#error').html("Please select the dropoff location")
	return false;
	}

	if (document.form1.car_type.value==0)
	{
	//alert("Please select your vehicle type")
	$('#error').html("Please select your vehicle type")
	return false;
	}


}

function DatePickedFrom(durDays, fromField, toField)	
{
	var arrivalDate = $("#"+fromField).datepicker("getDate"); // the "From" date	
	var minDepartureDate = new Date(arrivalDate);
	var maxDepartureDate = new Date(arrivalDate);
	var minAllowedDate = new Date(arrivalDate);
	minDepartureDate.setDate(arrivalDate.getDate() + durDays); // +xxx days from selected "From" date 
	maxDepartureDate.setDate(arrivalDate.getDate() + 365); // +365 days from seleced "From: date
	minAllowedDate.setDate(arrivalDate.getDate() + durDays); // allow minimum of +X days from "From" date		
	$("#"+toField).datepicker('option', 'minDate', minAllowedDate);
	$("#"+toField).datepicker('option', 'maxDate', maxDepartureDate);
};

$(document).ready(function()
{
	//$(".pickup-dd,.dropoff-dd").selectBoxIt();
	//$(".pickup-dd,.dropoff-dd,.car_type").selectBoxIt({ autoWidth: false, copyClasses: "container" });
	//$(".car_type").selectBoxIt({ autoWidth: false, copyClasses: "scr-row" });
	//$(".car_type").selectBoxIt();

	var noOfMonths ;
	var today = new Date();
	if($(window).width() < 630)
	{
		noOfMonths = 1;
	}
	else
	{
		noOfMonths = 2;
	}

	$( "#start_date" ).datepicker({
		minDate: parseInt($("#min_date_hidden").html()),
		showOn: "both",
		numberOfMonths: noOfMonths,
		stepMonths: noOfMonths,
		dateFormat: "dd-mm-yy",
		onClose:function(dateText, inst)
		{				
			$("#end_date").focus().trigger('open');
		},
		onSelect: function(date)
		{            
			var date1 = $('#start_date').datepicker('getDate');           
			var date = new Date( Date.parse( date1 ) ); 
			date.setDate( date.getDate() + 1 );        
			var newDate = date.toDateString(); 
			newDate = new Date( Date.parse( newDate ) );                      
			$('#end_date').datepicker("option","minDate",newDate);      
			
			
			DatePickedFrom(parseInt($("#min_dur_hidden").html()), 'start_date', 'end_date'); // number of days to create default duration
			      
		}
	});
	
	$( "#end_date" ).datepicker({
		showOn: "both",
		numberOfMonths: noOfMonths,
		stepMonths: noOfMonths,
		maxDate:31,
		dateFormat: "dd-mm-yy"
	});
	
	$("#start_date").attr( 'readOnly' , 'true' );
	$("#end_date").attr( 'readOnly' , 'true' );

	$( "#H1, #H2" ).timepicker({ 
		timeSeparator: ':',
		defaultTime: '12:00', 
		showOn: 'focus',  
		showPeriodLabels: false, 
		minutes: { interval: 30 },
	});
	$("#H1, #H2").attr( 'readOnly' , 'true' );
});

