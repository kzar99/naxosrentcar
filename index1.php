<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Naxos Europcar: Book your car and bike on Naxos island - Motonaxos</title>
<meta name="description" content="Motonaxos Rentals from 5€/day. Rent a car in Naxos from the Airport, Naxos town port, Agia Anna, Agios Prokopios or directly at your hotel. Special offers, Free delivery, full insurance for all the rental vehicles." />
<meta name="keywords" content="naxos europcar, Rent A Car & bike, naxos rent a car, auto tour rent a car, naxos car rental, car, rentals, naxos island, bike, naxos bikes, naxos bike, naxos bike rentals, Rent a car in naxos" />
<meta name="google-site-verification" content="_f9rmLQilialVy33S9VpfhqH1RhLuD502y9PrhQfs7E" />
<meta property="og:image" content="https://www.naxosrentcar.com/images/car-rental-1.jpg" />
<meta property="og:type" content="place" />
<meta property="og:url" content="https://naxosrentcar.com" /> 
<meta property="og:title" content="Motonaxos, Naxos Europcar, Naxos Rent a Car & bike, Economy car rental in Naxos" /> 
<meta property="og:description" content="Motonaxos rental from 5&#8364;/day. Rent a car in Agia Anna-Naxos. Rent ATV, Bug Rider. Naxos Europcar Greece. Great service from the biggest car & bike rental company in naxos. Naxos Auto rent - Naxos moto rent, Rent a car at  naxos airport, at  naxos port and all naxos hotels Special offers, Free delivery,full incurance for all the rental vechicles. Also you can see all the  Naxos apartments  & hotels, travel information’s, naxos map and all tips about naxos island" /> 
<meta property="place:location:latitude" content="37.101442" />
<meta property="place:location:longitude" content="25.3786124" />


<?php include("includes/_head_css.php");?>

</head>

<body class="homepage">
<?php include("includes/_head.php");?>

<!--Banner section-->
<section class="banner">
    <div id="slidecaption" class="load-item"></div>
	<div class="container">
	<ul id="slide-list" class="load-item"></ul>
		<?php //echo create_search_form();?>
	</div>
	<div class="shadow"></div>
	<div class="slider"></div>

</section>

<!--Welcome section-->
<section class="welcome">
	<div class="container">
		<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
			<div class="carousel slide" id="myCarousel">
				<div class="carousel-inner">
	
					<div class="item active">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"><a href="#" class="thumbnail"><img src="/images/car1.jpg" alt="Image"></a></div>
							<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"><a href="#" class="thumbnail"><img src="/images/car1.jpg" alt="Image"></a></div>
						</div>
					</div><!--.item-->
	
					<div class="item">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"><a href="#" class="thumbnail"><img src="/images/car1.jpg" alt="Image"></a></div>
							<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"><a href="#" class="thumbnail"><img src="/images/car1.jpg" alt="Image"></a></div>
						</div>
					</div>
					
				</div>
				<a id="previous" class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
				<a id="next" class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
			</div>
		</div>
	
		<div class="welcome-text col-md-6 col-xs-12 col-sm-6 col-lg-6">
			<h1>Go for more with the experts</h1>
			<p>Motonaxos offers great service and low rates for car and motorbike rental in Naxos Island! Rent a car in Naxos Island with the top car rental company! Compare car rental prices, Book online your car fast and easy, Rent a quad or a buggy in Naxos, hire a bike or motorcycle explore Naxos Island!  Safe, new models, Low rental prices. <a href="welcome.htm">more...</a></p>
	  </div>
	</div>
</section>


<section class="content">
<div class="container">
	<div class="row">
    	<div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box orange">
                <div style="padding:0px;">
                <a href="/images/map-naxos.pdf" target="_blank" style="padding:0px;"><img src="/images/motonaxos-cover2015.jpg" class="image"/></a>
                <a href="naxos.htm" style="padding:0px;"><h4 style="padding:85px 30px 84px;"><i class="fa fa-compass" style="margin-bottom:0px;"></i><br />explore<strong>naxos</strong></h4></a>
                </div>
            </div>
        </div>

    	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="box white">
				<div class="wrap">
				<i class="fa fa-edit"></i>
				<h4>View, Modify, Cancel<br /> <strong class="green">Your Booking</strong><p class="mar-top15">Search for your bookings<br /> and make changes or<br />
cancellations.</p></h4>
				 </div>
			</div>
			<div class="box white">
				<a href="why-us.htm">
				<i class="fa fa-check-circle"></i>
				<h4>Why choose<br /><strong class="green">MotoNaxos?</strong></h4>
				</a>
			</div>
			<!--box-->
			<div class="box white">
				<a href="faq.htm">
				<i class="fa fa-question-circle"></i>
				<h4><strong>FAQ</strong></h4><p style="margin-bottom:0px;"><!--Get answers to our most<br />--> frequently answered questions.</p>
				</a>
		   </div>
        </div>
    	<div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box white">
                <a href="http://www.apartmentsenosis.com/" target="_blank">
                <img src="/images/apartmentsenosis.jpg" class="mar-bot20" />
                <h4 style="margin-bottom:10px;">Accommodation + vehicle up to 
<strong>30% discount </strong>
</h4>
                </a>
            </div>
            <!--box-->
			<div class="box orange">
				<a href="hotels.htm" style="padding:52px 31px;">
				<i class="fa fa-suitcase" style="font-size:60px;"></i>
				<h4>Hotel<strong>Partners</strong></h4>
				</a>
			</div>
			<!--box-->
        </div>
    </div><!--row-->   
        
	<div class="row">


    	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="box orange">
				<a href="opinions.htm" style="padding:31px;">
				<i class="fa fa-map-marker" style="font-size:60px;"></i>
				<h4>Places to <strong>Visit</strong></h4>
				</a>
			</div>
			<!--box-->
        </div>
 
     	<div class="col-md-4 col-sm-4 col-xs-12">
			
			<div class="box white">
				<!--<a href="opinions.htm" style="padding:31px;">-->
                <div class="wrap" style="padding:31px;">
				<i class="fa fa-comment" style="font-size:60px;"></i>
				<h4>Give us <strong>your opinion!</strong></h4>
				</div>
			</div>
			<!--box-->
        </div>
        
     	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="box orange">
				<div class="wrap" style="padding:31px;">
				<i class="fa fa-plane" style="font-size:60px;"></i>
				<h4>Travel agent <strong>partners</strong></h4>
				</div>
			</div>
			<!--box-->
        </div>
    </div><!--row-->

	<div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="gallery.htm">
                        <i class="fa fa-camera boxicon"></i>
                        <h4>Naxos <strong>Photogallery</strong></h4>
                    </a>
                </div><!--box-->
            </div>			

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="guides.htm">
                        <i class="fa fa-info-circle boxicon"></i>
                        <h4>Naxos <strong>Travel info</strong></h4>
                    </a>
                </div>
                <!--box-->
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <div class="wrap">
                        <i class="fa fa-tags"></i>
                        <h4>Other <strong>Islands Deals</strong></h4>
                    </div>
                </div>
                <!--box-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="more.htm">
                        <i class="fa fa-asterisk boxicon"></i>
                        <h4>Naxos <strong>plus</strong></h4>
                    </a>
                </div>
                <!--box-->
            </div>
    </div><!--row-->


</div>
</section>

<!--<section class="content">
<div class="container">
	<div class="row">
    	<div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box white">
                <a href="contact.htm"><i class="fa fa-envelope boxicon"></i>
                <h4>Easy book <strong>by Email</strong></h4></a>
                <ul class="mar-top15">
                    <li><i class="fa fa-check"></i> 24 hours service</li>
                    <li><i class="fa fa-check"></i> Full Insurance</li>
                    <li><i class="fa fa-check"></i> Theft Waiver</li>
                    <li><i class="fa fa-check"></i> Personal Insurance</li>
                    <li><i class="fa fa-check"></i> Delivery/ Collection</li>
                    <li><i class="fa fa-check"></i> Unlimited KM</li> 
                    <li><i class="fa fa-check"></i> Road Maps</li>
                </ul>
            </div>
        </div>

    	<div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box white mar-bot20">
            <a href="discounts.htm">
                <i class="fa fa-users boxicon"></i>
                <h4>Become a member<br /> <strong class="green">and take the discount</strong><br /> from the first reservation!</h4>
             </a>
            </div>
			<div class="box white">
			<a href="offers.htm">
				<i class="fa fa-exclamation-circle boxicon"></i>
				<h4><strong>Special</strong> offers<br /> From <strong class="green">0 euros</strong> per day!</h4>
			 </a>
			</div>
        </div>
    	<div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box white">
                <a href="http://www.apartmentsenosis.com/" target="_blank">
                <img src="/images/thumb.jpg" class=" mar-bot20" />
                <h4 style="margin-bottom:13px;"><strong>Book now</strong> your apartment<br /> and take your vehicle<br /> <strong>FREE!</strong></h4>
                </a>
            </div>
        </div>
    </div>row   
        
	<div class="row">
    	<div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box orange">
                <a href="naxos.htm" style="padding:0px;">
                <img src="/images/thumb2.jpg" class="image"/>
                <h4 style="padding:30px;"><i class="fa fa-compass" style="margin-bottom:0px;"></i><br />explore<strong>naxos</strong></h4>
                 </a>
            </div>
        </div>


    	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="box white">
				<a href="modify.htm">
				<i class="fa fa-edit"></i>
				<h4>View, Modify, Cancel<br /> <strong>Your Booking</strong><p class="mar-top15">Search for your bookings<br /> and make changes or<br />
cancellations.</p></h4>
				 </a>
			</div>
            
			<div class="box white">
				<a href="faq.htm">
				<i class="fa fa-question-circle"></i>
				<h4><strong>FAQ</strong></h4><p>Get answers to our most<br /> frequently answered questions.</p>
				 </a>
		   </div>
            
        </div>
 
     	<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="box orange">
				<a href="why-us.htm" style="padding:38px;">
				<i class="fa fa-check-circle" style="font-size:60px;"></i>
				<h4>Why choose<br /><strong>MotoNaxos?</strong></h4>
				</a>
			</div>
			box
			
			<div class="box orange">
				<a href="opinions.htm" style="padding:31px;">
				<i class="fa fa-comment" style="font-size:60px;"></i>
				<h4>Give us <strong>your opinion!</strong></h4>
				</a>
			</div>
			box
        </div>
                
    </div>row

	<div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="guides.htm">
                        <i class="fa fa-map-marker boxicon"></i>
                        <h4><strong>places</strong>to<strong>visit</strong></h4>
                    </a>
                </div>box
            </div>			

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="hotels.htm">
                        <i class="fa fa-suitcase boxicon"></i>
                        <h4><strong>hotels</strong></h4>
                    </a>
                </div>
                box
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="more.htm">
                        <i class="fa fa-asterisk boxicon"></i>
                        <h4>more<strong>services</strong></h4>
                    </a>
                </div>
                box
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box yellow">
                    <a href="gallery.htm">
                        <i class="fa fa-camera"></i>
                        <h4><strong>photo</strong>gallery</h4>
                    </a>
                </div>
                box
            </div>
    </div>row


</div>
</section>-->



<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script src="js/supersized.3.2.7.min.js" type="text/javascript"></script>
<script src="js/supersized.shutter.min.js" type="text/javascript"></script>
<script>    
//supersized
jQuery(function ($) 
{
	$.supersized({
		// Functionality
		slide_interval: 5500, // Length between transitions
		transition: 1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
		transition_speed: 1500, // Speed of transition
		// Components
		progress_bar:	1,					
		slide_links: 'blank', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
		slides: [ // Slideshow Images
			{
				image: 'images/car-rental-1.jpg', title : 'Get up to 30% off, online'
			},
			{
				image: 'images/buggy-2.jpg', title : 'Make your day unforgettable!'
			},
			{
				image: 'images/car-3.jpg', title : 'Explore Naxos from 14€/day'
			},
			{
				image: 'images/atv-4.jpg', title : 'Feel the summer from 12€/day'
			}
			]
	});
});

</script>
</body>
</html>
		