<?php
include("includes/connection.php"); 
include("includes/func.php"); 

$user_dir = "gallery_images";
$thumb_dir = $user_dir."/thumbs";
$test_dir = dir($thumb_dir);

$array_thumb = array();
$total_pages = 1;

while(($file3 = $test_dir->read()) !== false)
{
$ext3=explode('.',$file3);
	if($file3 != '.' && $file3 != '..' && $ext3[1] && $file3!="Thumbs.db")
	{
		$array_thumb[]=$file3;
	}
}

if (isset($_POST['page'])) { $page = (int)$_POST['page']; } else {$page = 1;}
$limit = 12; 
$set_limit 	= $page * $limit - ($limit);


if ( count($array_thumb)>0 )
{
	$total_pages = ceil(count($array_thumb) / $limit);
}

$start = $set_limit;
$end = $start + $limit - 1;


// <a href="javascript: void(0);" onclick="document.SubmitUsers.submit();return false;">

$pagination = "";
if ($total_pages>1)
{

	$pagination = "<div id=\"pagination\"><ul><li>Select page:&nbsp;|&nbsp;</li>";
	
	for ($k=1;$k<=$total_pages;$k++)
	{
	$pagination .= "<li>";
		if ($k!=$page)
		{
		
		$pagination .= "
		<form action=\"gallery.htm\" method=\"post\" name=\"GalleryForm".$k."\">
		<input name=\"page\" type=\"hidden\" value=\"".$k."\">
		<a href=\"javascript:document.GalleryForm".$k.".submit();\">".$k."</a>&nbsp;|&nbsp;
		</form>	
		";
		}
		else
		{
		$pagination .= $k."&nbsp;|&nbsp;";
		}
	$pagination .= "</li>";
	}

	$pagination .= "</ul><hr></div>";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Gallery</h1>
                    <div id="gallery2">
					<?php
                    if ($array_thumb!="")
                    {
                    ?>
                    <ul>
					<?php
                    for ($i=$start; $i<=$end; $i++)
                    {
                    if (isset($array_thumb[$i]))
                    {
                    $show_image = $user_dir."/".$array_thumb[$i];
                    $show_thumb = $thumb_dir."/".$array_thumb[$i];
                    
                        if (is_file($show_thumb))
                        {
                    ?>
                    	<li><a href="<?php echo $show_image;?>" data-gall="myGallery" class="normal gal"><span class="rollover"></span><img src="<?php echo $show_thumb;?>" /></a></li>
                       
					<?php
                        }
                    }
                    }
                    ?>
                    </ul>
					<?php
                    }
                    else
                    {
                    ?>
                    <div align="center" class="bigtxt7">
                    No images found</div>
                    <?php
                    }
                    ?>
                    </div>
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				