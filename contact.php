<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Contact <strong>Us</strong></h1>
					<h2>Address:</h2>
                    <p>Court Square, Chora - Naxos, 84300 | Cyclades Greece<br />
                    <i class="fa fa-phone"></i> +30 22850 23420   <i class="fa fa-fax"></i> +30 22850 26393<br />
                    Agia Anna, <strong>tel./fax:</strong> +30 22850 41404, <strong>road service:</strong> +30 6944 622555<br />
                    <i class="fa fa-envelope"></i>  <a href="mailto:info@motonaxos.com">info@motonaxos.com</a>                    </p>
	       			
                    
                    
					<div class="carform row" style="border:0px; margin-bottom:30px;">
					 <p class="col-md-12"><strong>As an alternative, you can use the following form, to contact us.</strong></p>
                		<form id="form-contact" action="#successResponce" method="POST" name="form-contact">
                        <ul>
                        <li class="col-md-6">
                            <div class="labels">Full Name</div>
                            <div class="inputs"><input type="text" name="fullname" id="fullname" class="field"></div>
                        </li>
                         <li class="col-md-6">
                            <div class="labels">Your E-mail</div>
                            <div class="inputs"><input type="text" name="email" id="email" class="field"></div>
                        </li>
                        <li class="col-md-6">
                            <div class="labels">Your Phone</div>
                            <div class="inputs"><input type="text" name="phone" id="phone" class="field"></div>
                        </li>
                        <li class="col-md-6">
                            <div class="labels">Town</div>
                            <div class="inputs"><input type="text" name="town" id="town" class="field"></div>
                        </li>
						
						<li class="col-md-6">
							 <div class="labels">Country</div>
							 <div class="inputs"><input type="text" name="country" id="country" class="field"></div>
						</li>	

						<li class="col-md-6">
							 <div class="labels">Your message here</div>
							 <div class="inputs"><textarea name="comments" id="comments" class="textarea"></textarea>	</div>
						</li>	
                        
                        				
                        <li id="error" class="col-md-6">
                            
                            
                        </li>
                        <li class="col-md-6">
                        	<img id="loading" src="/images/loading2.gif">
                        	<input id="submit-form2" name="Submit" type="submit" class="booknow" value="SEND"/>
                        </li>
                       </ul>
                        </form>
                        <hr style="visibility:hidden; clear:both;"/>
                       
                  </div>
                      <h4 id="successResponce" style="display:none;">Your request has been submitted <strong>successfully!</strong><br>You will be contacted as soon as possible<br>Thank you!</h4>              
              	</div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();


	$("#submit-form2").live('click', function(e) {
		e.preventDefault();
		var fullname = $("#fullname").val();
		var email = $("#email").val();
		var phone = $("#phone").val();
		var town = $("#town").val();
		var country = $("#country").val();			
		if ( fullname == '' )
		{
			$('#error').html("Please type your Full name");
			$('#fullname').focus();
		}
		else if ( email == '' || isValidEmailAddress(email) != true)
		{
			$('#error').html("Invalid E-mail Address! Please re-enter");
			$('#email').focus();		
		}
		else if ( phone == '' )
		{
			$('#error').html("Please type your Phone");
			$('#phone').focus();		
		}
		else if ( town == '' )
		{
			$('#error').html("Please enter your town");
			$('#town').focus();		
		}
		else if ( country == '' )
		{
			$('#error').html("Please enter your country");
			$('#postText').focus();		
		}
		else
		{			
			var dataStringAdd = $("form#form-contact").serialize();
			$("#loading").show();
			
			$.ajax({
				type: "POST",
				url: 'submit_contact.php',
				data: dataStringAdd,
				cache: false,
				success: function(htmlResponce){
					$(".carform").hide();
					$("#loading").hide();
					$("#successResponce").show();
					
				}
			});	
		}
	return false;
	});
			
	$("#phone").keypress(function(e) {
		var a = [];
		var k = e.which;
		
		for (i = 48; i < 58; i++)
			a.push(i);
		
		if (!(a.indexOf(k)>=0))
			e.preventDefault();
		
	});

});
</script>

</body>
</html>
				