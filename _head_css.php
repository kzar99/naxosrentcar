<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,700&subset=greek-ext,latin-ext' rel='stylesheet' type='text/css'>
<link href="css/custom.css" rel="stylesheet" type="text/css" media="all"/>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="css/jquery.ui.timepicker.css">
<!--[if gte IE 9]>
<link rel="stylesheet" type="text/css" href="css/ie9.css" />
<![endif]-->
<link rel="stylesheet" href="css/supersized.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/supersized.shutter.css" type="text/css" media="screen" />
<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

<!--<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />-->
<link rel="stylesheet" href="venobox/venobox.css" type="text/css" media="screen" />

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-75492625-1', 'auto');
  ga('send', 'pageview');
 
</script>