<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Frequently Asked Questions</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    
					<h1 style="margin-bottom:15px;">Do you need any Help?</h1>
					<p>Questions on making a rental car or bike reservation on motonaxos.com.Our answers below tell you what you need to know. </p>
					
					
                    <div class="faq-list">
                    <ul>
						<li><h2>Concerning the reservation</h2></li>
						<li>
							<h4>How will I know that my reservation is confirmed?</h4>
							<div class="first"><p>At the end of the reservation process you will receive a confirmation e-mail containing the details of your reservation. You should print these and keep them for your records.</p></div>
						</li>
						<li>
							<h4>Is it possible to modify my reservation after it is confirmed? </h4>
							<div><p>Yes, you can modify or cancel your reservation by e-mail or through the automatic system after it is confirmed, provided it is done up to 48 hours prior to rental time.</p></div>
						</li>
						<li>
							<h4>Is it possible to cancel my prepaid reservation after it is confirmed?</h4>
							<div><p>Yes, you can cancel your prepaid reservation by e-mail after it is confirmed, provided it is done up to 5 days prior to rental time. </p></div>
						</li>
                   </ul>
                    </div>
					
					
                    <div class="faq-list">
                    <ul>
						<li><h2>INSURANCES</h2></li>
						<li>
							<h4>What is the third party insurance?</h4>
							<div class="first"><p>All the vehicles are supplied with third party insurance which means that in case of an accident our insurance company covers the cost of the damage you have caused to another vehicle.</p></div>
						</li>
						<li>
							<h4>What is CDW (Collision Damage Waiver) insurance?</h4>
							<div><p>The Collision Damage Waiver covers any damage caused on the rental vehicle regardless of who is at fault with an excess depending on the type of the vehicle.</p></div>
						</li>
						<li>
							<h4>What is the FDW (Full Damage Waiver) insurance?</h4>
							<div><p>The Full Damage Waiver insurance reduces the excess amount the renting person may be obliged to pay for collision damages.</p></div>
						</li>
						<li>
							<h4>How old I have to be in order to buy the CDW or FDW?</h4>
							<div><p>In order to purchase the CDW or the FDW you have to be over 25 years old and up to 75 years old for the most of the rental vehicles. Please pay attention that you have to have your driver license over 2 years in order to buy the CDW or FDW.</p></div>
						</li>	
						<li>
							<h4>Can I buy any extra insurance for renting a scooter, an Atv or a buggy?</h4>
							<div><p>In the prices is already included the thirty party insurance. For renting a scooter, an Atv or a buggy you may purchase an extra insurance locally.</p></div>
						</li>	
						<li>
							<h4>Is it possible to buy FDW insurance for a scooter, an Atv or a buggy?</h4>
							<div><p>Extra insurance for scooters, Atvs and buggies are available only locally and the maximum protection you can have is by purchasing the CDW, as FDW does not exist for such kind of vehicle. </p></div>
						</li>																		
                   </ul>
                    </div>					
					
					
                    <div class="faq-list">
                    <ul>
						<li><h2>Necessary documents</h2></li>
						<li>
							<h4>Is my driver’s license valid in Greece in order to drive a scooter or an Atv?</h4>
							<div class="first"><p>For European citizens check the link below <a href="http://eur-lex.europa.eu/legal-content/ES/TXT/?uri=CELEX:32014D0209" target="_blank">http://eur-lex.europa.eu/legal-content/ES/TXT/?uri=CELEX:32014D0209</a>. For non-European citizen please contact us by email.</p></div>
						</li>
						<li>
							<h4>Can I proceed to a reservation without having a credit card?</h4>
							<div><p>Of course you may proceed to a reservation without having a credit card by sending a bank deposit. Please note that you will need a security deposit in order to rent the vehicle locally.</p>
							<p>For any other question or demand do not hesitate to contact at any time on the following email address info@motonaxos.com</p>
							</div>
						</li>
                   </ul>
                    </div>					
					
					
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();

//	$(document).ready(function() {
//		$('.faq-list h4').each(function() {
//			var tis = $(this), state = true, answer = tis.next('div').slideUp();
//			tis.click(function() {
//				state = !state;
//				answer.slideToggle(state);
//				tis.toggleClass('active',state);
//			});
//		});
//	});	
	//$(".faq-list div.first").show();
	//$(".faq-list div").hide();
    $(".faq-list h4").click(function () {
        $(this).next(".faq-list div").slideToggle(500);
        $(this).toggleClass("expanded");
		$(".faq-list h4").toggleClass(".fa");
    });
	
});
</script>

</body>
</html>
				