<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container terms">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                	
                  <h1>Terms & <strong>Conditions</strong><p class="pull-right">Languages: <a href="terms.htm"><strong>EN</strong></a> | <a href="terms-gr.htm">GR</a> | <a href="terms-de.htm">DE</a> | <a href="terms-fr.htm">FR</a> | <a href="terms-it.htm">IT</a> | <a href="terms-ru.htm">RUS</a></p>
                  </h1>
	              
     	<h2>TERMS &amp; CONDITIONS TO RENT A CAR OR MOTORBIKE</h2>
     <p>Welcome to  Motonaxos.com the biggest online system   for car and motorbike rental  on Naxos.  We  offer  a wide range of cars for rent in Naxos, new and safe models of motorbikes and quads, the  top quality of the hired vehicles , VIP customer service and excellent car  &amp; motorbike rental deals. For fast and easy car rental and bike rental in Naxos Island,  make your reservation through our online system. </p>
   	 <h3>CANCELLATION POLICY:</h3>
     <ul class="checklist2">
			
     	<li>NO FEE IS CHARGED FOR CANCELLATIONS WITHIN: <strong>48hrs</strong></li>
     	<li>before the first rental day for  Groups: Cars,  Scooters, ATV.</li>
     	<li><strong>5 days</strong> before the first rental day for  Groups: 
     	Special  Cars, motorcycles &amp; buggies </li>
        </ul>
		<p><strong>For any Cancellation made after, the deposit will be charged.</strong> </p>
     	<p>The Rental Company is authorised to cancel the reservation in any case  of  misstatements, such as driver’s  licence and credit card details etc. and is entitled  to charge the entire deposit.</p>
     	<h3>PRICE POLICY:</h3>
     	<p>We guarantee the best available rates and best  possible insurance coverage on top quality vehicles with high  standard service.<br>
	<strong>Attention: </strong>Mentioned rates and  offers are valid only for reservations made directly through our homepage.</p>
  
     
    
     	<h3>TERMS AND  CONDITIONS TO HIRE A VEHICLE </h3>
		<strong>Items and Services  included in the rates:</strong>
            <ul class="checklist2">
     	<li>Road Assistance</li>
     	<li>One Additional driver.</li>
     	<li>Baby Seats (Have to be pre-booked and confirmed by the rental station).</li>
     	<li>Unlimited mileage.</li>
     	<li>Delivery and drop off at the airport, port &amp; hotels  during our <br />
     	office hours  (08.30h – 21.30h). Beside these hours an additional fee is applicable.</li>
     	<li>Road maps.</li>
     	<li>Helmets</li>
	
     	<li><strong>TAXES</strong>:  All taxes are included in the basic rates.</p>
     	<p><strong>Payment:</strong> For payments made in person, the following credit cards are accepted: MasterCard /  VISA.</li>
	 	<li>For payments made only through our wesbsite, the following cards are accepted: credit cards, debit cards, and prepaid cards from Visa, MasterCard, Maestro, American Express.</li>
     	<li>Remaining Vehicle rental fees are payable upon receiving.</li>
     	<li>A deposit is required for all rentals. Credit cards are acceptable.</li>
        </ul>
	<p><strong>Returning the Vehicle:</strong><br>
	<strong>The hirer is obliged to return the vehicle the date, time, location and  in the same condition as expressed in the contract.</strong> The vehicle has to be returned with the same tank  contents as received upon delivery. Please note, that there is NO REFUND<br>
     	for unused fuel.</p>
        
	<h3>RENTING A CAR</h3>
	<strong>Driver  requirements:</strong><br>
     	- Minimum age of driver is 23 years for the groups A, B, C, M, F, mini special,  E, D 
     	and 27 years  old for the groups U, K, L.<br>
	<strong>Driver's licence:</strong> The Driver must hold a valid car driver's licence for at least 1 year.<br>
     	Passport or ID card is additionally required.<br>
	<strong>ATTENTION!</strong> For some non  European Drivers maybe is needed an International Drivers License. For any  hesitation you can send us a copy of your license to assist you.<br>
	<br>
	<strong>Insurances: The following insurances are  available.</strong>
    <ul class="checklist2">
	<li> Third party insurance (3rd.P.I.) for all  the vehicles: with 3rd party insurance in case of an accident our  insurance company covers the cost of the damage you have caused to another  vehicle</li>
	<li> Insurance C. D. W.(Collision Damage  Waiver) The renter is insured for damages to the rented vehicle in the event of  an accident with an excess of maximum:</li>
     <ul class="checklist2">
     	<li>Groups  A, B, C ,M: 600€ (+VAT)</li>
			<li>Groups  D, E ,F: 800€ (+VAT)</li>
			<li>Groups  U, L ,mini special: 1100€ (+VAT)</li>
		</ul>
     </li>
     <li>Super C.D.W. insurance (F.D.I.) for the  most of the cars is available locally (minimum age of driver 27 years)</li>
			<li> Theft Insurance (T.W.) </strong><strong>the  insurance covers the car with  the above  excess</strong></li>
			<li> Super Theft Insurance(F.T.W.) </strong><strong>with no  excess.</strong></li>
			<li>Personal Accident Insurance (P.A.I.) </strong><strong>for the  driver.</strong></li>
		</ul>
     <p><strong>ATTENTION:</strong> There is no  insurance coverage if the driver is under the influence of alcohol, any kind of  drugs or commits any traffic violations. Driver is liable for all parking and  traffic violations during the rental period. Absolutely no vehicle is allowed  to be driven in sand, on the beach, in dry lakes or off roads. All cars (except  suitable jeeps) are only allowed to be driven on paved roads. <br>
				<strong>Please NOTE, that  there is no insurance coverage for damages caused to the tires, mirrors, loss  keys, underside damage, damage to the gear box.</strong>
		</p>
        
     <h3>Car Services:</h3>
			<p><strong>Roof Rack: </strong>Is provided, following a reservation, with an extra charge and for  specific cartype.<br>
			<strong>GPS System</strong>: Can be provided, following a confirmed reservation with an extra<br>
			<strong>Pure car cleaning – allergy free: </strong>can be provided with an extra  charge.</p>
            
     <h3>RENTING A SCOOTER, MOTORBIKE, ATV, OR BUGGY</h3>
			<strong>Driver requirements:</strong><br>
     	Minimum age of driver is 23 years.<br>
	<strong>Driver's license:</strong> The Driver must hold a valid driver's license. For an atv or a buggy is obligated a car driver license. For a scooter up to 100cc you need a driver’s license category AM and for a scooter or motorcycle over 100cc category A driver’s license.<br>
     	Passport or ID card is additionally required.<br>
	<strong>ATTENTION!</strong> For some non European Drivers maybe is needed an International Drivers License. For any hesitation you can send us a copy of your license to assist you.</p>
     <strong>Insurances: The following insurances are  available.</strong>
     <ul class="checklist2">
			<li> T<strong>hird party insurance (3rd.P.I.)</strong> for all  the vehicles: with 3rd party insurance in case of an accident our  insurance company covers the cost of the damage you have caused to another  vehicle.</li>
			<li> <strong>Theft Insurance (T.W.)</strong> the  insurance covers the car with  the above  excess</li>
			<li> <strong>Super Theft Insurance(F.T.W.)</strong> with no  excess.</li>
			<li> <strong>Insurance C. D. W.(Collision Damage  Waiver)</strong> The renter is insured for damages to the rented vehicle in the event of  an accident with an excess(arranged it locally).</li>
			<li> <strong>Flat Tires Insurance (F.T.I)</strong> flat tire  insurance covers reparations and transportation of the vehicle without  additional cost. </li>
    </ul>
		<strong>ATTENTION:</strong> There is no  insurance coverage if the driver is under the influence of alcohol, any kind of  drugs. Driver is liable for all parking and traffic violations during the  rental period. Absolutely no vehicle is allowed to be driven in sand, on the  beach, dry lakes and off roads.<br>
		<strong>Please NOTE, that  there is no insurance coverage for damages caused to the tires, mirrors, loss  keys, damage to gear box.</strong></strong><br /><br />
		<center><img src="/images/horizontal.jpg" /></center>

                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				