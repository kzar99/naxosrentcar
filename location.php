<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Naxos Europcar Rent A Car & bike, car rentals Naxos</title>
<meta name="description" content="Naxos europcar is a Naxos rent a car & bike company offering car rentals reliable services all over Naxos island " />
<meta name="keywords" content="naxos europcar, Rent A Car & bike, naxos rent a car, auto tour rent a car, naxos car rental, car, rentals, naxos island, bike, naxos bikes, naxos bike, naxos bike rentals, travel information, Agia anna, Agios prokopios, Plaka beach, naxos photos, about naxos, agia anna" />
<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Location</h1>
					<h4>Motonaxos operates car and motorbike hire stations at many points in Naxos Island. Find the station which is more convenient for you (<a href="#my_map">see map</a>) :</h4>
	                <br />
                    <ul class="location-list">
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        	<div class="row">
                            <div class="col-md-4 col-lg-3 col-sm-3 col-xs-12"><img src="/images/location1_thumb.jpg" class="thumb"></div>
                            <div class="col-md-8 col-lg-9 col-sm-9 col-xs-12"><p><strong>Motonaxos rental office in Naxos town:</strong><br /> You can find us in the middle of Naxos town, on the most popular square (Platia Protodikiou), just a small walk from Naxos port. Our qualified staff will help you to choose from a big variety of rental vehicles and will give you all tips to venture Naxos Island. Court square, Chora – Naxos 84300 Cyclades Hellas.<br /> <i class="fa fa-phone"></i> (0030) 22850 23420 | <i class="fa fa-fax"></i> (0030) 22850 26393</p></div>
                            </div>
                            <hr/>
                        </li>
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        	<div class="row">
                            <div class="col-md-4 col-lg-3 col-sm-3 col-xs-12"><img src="/images/location2_thumb.jpg" class="thumb"></div>
                            <div class="col-md-8 col-lg-9 col-sm-9 col-xs-12"><p><strong>Motonaxos rental office in Agios Prokopios:</strong><br /> In the center of Agios Prokopios resort in front of the main road just few meters from the beach our new Naxos car and bike rental office is now open to fulfill your expectations. Stands out for its big place with the great variety of cars, motorbikes, atvs for rent in Naxos.<br /> <i class="fa fa-phone"></i> (0030) 22850 41404/200</p></div>
                            </div>
                            <hr/>
                        </li>
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        	<div class="row">
                            <div class="col-md-4 col-lg-3 col-sm-3 col-xs-12"><img src="/images/location3_thumb.jpg" class="thumb"></div>
                            <div class="col-md-8 col-lg-9 col-sm-9 col-xs-12"><p><strong>Motonaxos rental office in Agia Anna in Naxos:</strong><br /> At the most well-known point of Agia Anna resort, in front of the small port is located our Agia Anna rental office. Agia Anna car and bike rental office includes an enormous private garage and a big open parking place where you can find the most suitable rental car or motorbike and the biggest collection of rental Atv (Quads) in Naxos island. <br /> <i class="fa fa-phone"></i> (0030) 22850 41404/200 | <i class="fa fa-fax"></i> (0030) 22850 41404/200 </p></div>
                            </div>
                            <hr/>
                        </li>
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        	<div class="row">
                            <div class="col-md-4 col-lg-3 col-sm-3 col-xs-12"><img src="/images/location4_thumb.jpg" class="thumb"></div>
                            <div class="col-md-8 col-lg-9 col-sm-9 col-xs-12"><p><strong>Car rental on Naxos port:</strong><br /> Rent a car at the port of Naxos and make your transportation easy and comfortable. We deliver the car in Naxos port just 150 meters from the ferry. You will find us only 150m from the Naxos port pier on your left hand at the allowed parking space areas. <strong>Motorbike rental on Naxos port</strong> – If you prefer to rent a bike at the port of Naxos then an employee from our company will be at the port of Naxos holding a sign with your name and guide you towards our main office, just a few meters from the port of Naxos.</p></div>
                            </div>
                            <hr/>
                        </li>
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        	<div class="row">
                            <div class="col-md-4 col-lg-3 col-sm-3 col-xs-12"><img src="/images/location5_thumb.jpg" class="thumb"></div>
                            <div class="col-md-8 col-lg-9 col-sm-9 col-xs-12"><p><strong>Rent a car in Naxos Airport:</strong><br /> Coming by plane? Make your holidays on Naxos simple, easy and comfortable. Arrange your car rental on Naxos airport and a representative from our rental company will be there to assist you. </p></div>
                            </div>
                            <hr/>
                        </li>
						
						 <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">			
							<div id="my_map" style="width:868px; height:500px;"></div>
						</li>
						
                    </ul>
                    
                    <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?language=<?php echo str_replace("lang_", "", $googleLang);?>"></script>
<script type="text/javascript" src="/js/jquery.ui.map.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	// we set center as the first point in array
	$("#my_map").gmap({
		"center": '37.05736900011469, 25.477294921875', 
		"zoom": 12,
		"mapTypeControl": true,
		"disableDefaultUI": false, 
		"bounds": true
	})
	.bind("init", function() 
	{ 		
		$(this).gmap('addMarker', { 
			'position': '37.101879, 25.376259', 
			'title': 'Motonaxos rental office in Naxos town', 
			'bounds': true
		}).click(function() 
		{
			$("#my_map").gmap('openInfoWindow', { 'content': 'Motonaxos rental office in Naxos town' }, this);
		});
		
		$(this).gmap('addMarker', { 
			'position': '37.067169, 25.356362', 
			'title': 'Motonaxos rental office in Agia Anna in Naxos', 
			'bounds': true
		}).click(function() 
		{
			$("#my_map").gmap('openInfoWindow', { 'content': 'Motonaxos rental office in Agia Anna in Naxos' }, this);
		});	
		
		$(this).gmap('addMarker', { 
			'position': '37.073918, 25.351870', 
			'title': 'Motonaxos rental office in Agios Prokopios', 
			'bounds': true
		}).click(function() 
		{
			$("#my_map").gmap('openInfoWindow', { 'content': 'Motonaxos rental office in Agios Prokopios' }, this);
		});				
		
	});		
});
</script>

</body>
</html>
				