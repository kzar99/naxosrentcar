<?php
include("includes/connection.php"); 
include("includes/func.php"); 

$car_type = "";
$car_show = "";
if (isset($_POST['car_type2'])) { $car_type = stripslashes($_POST['car_type2']); }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>
    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>

    <!--content section-->
	<section class="contentin">
        <div class="container models">


            <div class="row">
            <div class="carlist">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Models</h1>
                    

				<form action="models.htm" method="post" class="carlist-form">
				<input name="car_type2" type="hidden" value="<?php echo $car_type;?>">
				<div class="col-lg-6 col-md-6 col-sm-6 txt"><strong>Select vehicle type:</strong></div>
				<div class="col-lg-6 col-md-6 col-sm-6">
                <select name="car_type2" class="car_type2">
	<?php
	$query1 = "SELECT * FROM vehicles WHERE veh_code<>'' ORDER BY veh_order ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($row1 = mysql_fetch_array($result1))
	{
	?>
	<option value="<?php echo stripslashes($row1['veh_code']);?>" 
	<?php
	if ($car_type==$row1['veh_code'])
	{
		echo " selected";
		$car_show = stripslashes($row1['veh_name']);
	}
	?>
	><?php echo stripslashes($row1['veh_name']);?></option>				
	<?php
	}
	?>
				</select>
				<input name="submit1" type="submit" value="Find" class="findnow">
                </div>
                
                <hr  style="clear:both; visibility:hidden;"/>
				</form>	
				<br />	
                
				<?php
                $query200="SELECT * FROM company_category WHERE car_type='$car_type' ORDER BY category ";
                $result200 = mysql_query($query200)  or die(mysql_error().'<p>'.$query200.'</p>');
                $vehicles_found = mysql_num_rows($result200);
                if ($vehicles_found>0)
                {
                    while ($myrow200 = mysql_fetch_array($result200))
                    {
                    $cat_id = $myrow200['cat_id'];
                    
                        $season_array = array("low", "medium", "high", "peak", "other");
                        $price_array = array();
                        foreach ($season_array as $season)
                        {
                            $query1="SELECT * FROM company_car_list_".$season." WHERE cat_id='$cat_id'  ";
                            $result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
                            while ($myrow1 = mysql_fetch_array($result1))
                            {	
                                for ($i=1; $i<=8; $i++)
                                {
                                    if ($myrow1['day'.$i]>0)
                                    {
                                    $price_array[] = $myrow1['day'.$i];
                                    }
                                }
                            }		
                        }
                        
                        if (count($price_array)>0)		
                        {
                        sort($price_array);
                        $lowest_price = $price_array[0]." &#8364;";
                        }
                        else
                        {
                        $lowest_price = "N/A";
                        }

						
                    if ($myrow200['image']!="")
                    {
                    ?>
                <ul>	
                    <li class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-3 col-lg-4 col-xs-12 car-img">
                             <img src="car_images/<?php echo $myrow200['image'];?>" alt="Car Rental Category <?php echo $myrow200['category'];?>" name="car image of <?php echo $myrow200['category'];?>" border="0">
    </div>
                            <div class="col-md-8 col-sm-9 col-lg-8 col-xs-12 car-desc">
                                <p><strong>Info:</strong>   <?php echo stripslashes($myrow200['description']);?></p>
                                <p><strong><?php echo $car_show;?>:</strong> <?php echo stripslashes($myrow200['cat_cars']);?></p>
                                <p><strong>From:</strong> <?php echo $lowest_price;?></p>
                                <!--<p><strong>Price:</strong> <span class="txt12-1"><?php echo $myrow200['offer_price'];?>&#8364;</span></p> -->
                            </div>
                        </div>
					</li>
                </ul>
			<?php
			}
			?>

				<?php
                    $price_array = NULL;
                    }
                }
                else
                {
                ?>
                <div align="center" class="bigtxt7">No vehicles found for category <?php echo $car_show;?></div>
                <?php
                }
                ?>
                
                </div><!--col-left-->
                
                </div>
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 

<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				