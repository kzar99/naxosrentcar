<?php
include("includes/connection.php"); 
include("includes/func.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>
    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>

    <!--content section-->
	<section class="contentin">
        <div class="container plain">


            <div class="row">
            <div class="carlist">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Travel <strong>Info</strong></h1>
                    <ul>	
                        <li class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                 <img src="/images/naxostours.jpg">
                                </div>
                                <div class="car-desc col-md-9 col-sm-9 col-lg-9 col-xs-12">
                                    <h3><a href="mailto:info@naxostours.net">Naxos Tours</a></h3>
                                    <p>Naxos Tours: Agency on Naxos Island Greece offering travel services, hotels naxos, accommodations greece, rent a car, tours, offers, wedding proposals, ship tickets, naxos tours, naxos, paros island, greece and more</p>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                 <img src="/images/olympic.jpg">
                                </div>
                                <div class="car-desc col-md-9 col-sm-9 col-lg-9 col-xs-12">
                                    <h3><a href="http://www.olympicairlines.com/" target="_blank">Olympic Airlines S.A.</a></h3>
                                    <p>Our widespread network reaches 36 domestic and 37 international destinations throughout 28 countries in Europe, Africa, the Middle East and North America. Our modern fleet consists of 41 airliners, from wide-bodied Airbus A340-300s to DASH-8-100s turboprops. Code shares with major airlines, as well as numerous commercial partnerships, offer our passengers various advantages and benefits.</p>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                 <img src="/images/ferries.jpg">
                                </div>
                                <div class="car-desc col-md-9 col-sm-9 col-lg-9 col-xs-12">
                                    <h3><a href="http://www.ferriesingreece.com/" target="_blank">Ferries in Greece</a></h3>
                                    <p><strong>Greek ferries:</strong> Information and online ferries booking in Greece and the Greek Islands</p>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                 <img src="/images/zas-travel-naxos.jpg">
                                </div>
                                <div class="car-desc col-md-9 col-sm-9 col-lg-9 col-xs-12">
                                    <h3><a href="mailto:zas-travel@nax.forthnet.gr">Zas Travel</a></h3>
                                    <p>Naxos zas-travel | All travel and tourim arrangements - Tour Operators Naxos Accomodation Car rentals  Excursion in Naxos</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                
                </div><!--col-left-->
                
                </div>
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 

<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				