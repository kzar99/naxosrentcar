<?php
session_start();
include("../includes/connection.php");

$error_msg="";

if (isset($_POST['flag']) && $_POST['flag']=="insert")  // user has submit the form
{
	
	if ($_POST['loc_name']=="") 	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Location Name can't be empty.</b></font></div><br>"; }
	else if ($_POST['cost']=="") 	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Please provide the cost.</b></font></div><br>"; }
	else 							{ $error_msg="ok"; }
	
	if ($error_msg=="ok") // insert into db
	{
		$query1="SELECT * FROM main_special_location WHERE special_name='".$_POST['loc_name']."' ";
		$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
		$num_results1=mysql_num_rows($result1);
		if ($num_results1==1) // if extra is found in db
		{
		$error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Location already found in database.Please insert another</b></font></div><br>";
		}
		else // if extra not found, insert it to db
		{
		$sql2 = "INSERT INTO main_special_location (special_name,special_cost) VALUES ('".$_POST['loc_name']."','".$_POST['cost']."')";
		$result2 = mysql_query($sql2)  or die(mysql_error().'<p>'.$sql2.'</p>');
		$error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Extra added.</b></font></div><br>";
		}
	}
}

?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Add Pickup-Dropoff location</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="menu_title">Location Name</span> : give the name of the location</li>
<li><span class="menu_title">Cost</span> : type the cost of the specific location (fixed cost, NOT per day). Use DOT (.) for decimals. Price is in euro</li>
</ul>
</td>
</tr>
</table>
<?php
if ($error_msg!="")
{
echo $error_msg;
}
?>
<form name="form1" method="post" action="">
<input type="hidden" name="flag" value="insert">
<table width="50%"  border="0" align="center" cellpadding="5" cellspacing="0">
<tr>
<td width="40%">Location Name </td>
<td width="60%"><input name="loc_name" type="text" id="loc_name" size="30" maxlength="200" value="".$loc_name.""></td>
</tr>
<tr>
<td valign="top">Cost</td>
<td><input name="cost" type="text" sizee="30" id="cost" value="".$cost.""></td>
</tr>
<tr align="center">
<td colspan="2"><input name="Submit" type="submit" class="submit_button" value="Submit"></td>
</tr>
</table>
</form>

</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
