<?php
session_start();
include("../includes/connection.php");



//////////////////////
/// submit checks ////
/////////////////////
if ( isset($_POST['flag']) && $_POST['flag']=='add_new' )
{

	$sql1 = "INSERT INTO  vehicles ( veh_name, veh_code, veh_order ) 
	VALUES 
	( '".mysql_real_escape_string($_POST['veh_name'])."', '".mysql_real_escape_string(strtolower($_POST['veh_code']))."', '".mysql_real_escape_string($_POST['veh_order'])."' )";
	$result1 = mysql_query($sql1)  or die(mysql_error().'<p>'.$sql1.'</p>');

	header("Location: categories_types.php");
}


if ( isset($_POST['flag']) && $_POST['flag']=='update' )
{

	$sql1 = "UPDATE  vehicles SET 
	veh_name='".mysql_real_escape_string($_POST['veh_name'])."', 
	veh_code='".mysql_real_escape_string(strtolower($_POST['veh_code']))."', 
	veh_order='".mysql_real_escape_string($_POST['veh_order'])."'
	WHERE veh_id='".mysql_real_escape_string($_POST['veh_id'])."'
	";
	$result1 = mysql_query($sql1)  or die(mysql_error().'<p>'.$sql1.'</p>');

	header("Location: categories_types.php");
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Add new Vehicle Types</p>


<table width="60%" align="center">
<tr>
<td align="left">
	<strong>Name:</strong> the name of the vehicle type, as it will appear to front pages, to customers.<br>
	<strong>Code:</strong> short, 1 word, code name for the vehicle. Used to filter vehicles in admin lists.<br>
	<strong>Order:</strong> define the order of appearance for each type.<br>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<form name="form1" method="post" action="">
	<strong>Add new vehicle type</strong><br>
	<br>
	<input name="flag" type="hidden" id="flag" value="add_new">
	Type Name: <input name="veh_name" type="text" style="width:200px;"><br>
	Type code: <input name="veh_code" type="text" style="width:100px;"><br>
	Order of appearance: <input name="veh_order" type="text" style="width:50px;"><br>
	<input name="submit1" type="submit" value="Save new type">
	</form>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p><strong>Edit existing types</strong></p>
	<table>
	<thead>
	<tr>
	<th>Order</th>
	<th>Type</th>
	<th>Code</th>
	<th>Actions</th>
	</tr>
	</thead>
	<tbody>
	
	<?php
	$query1 = "SELECT * FROM vehicles ORDER BY veh_order ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($row1 = mysql_fetch_array($result1))
	{
	?>
	<form name="form<?php echo $row1['veh_id']?>" method="post" action="">
	<input name="veh_id" type="hidden" value="<?php echo $row1['veh_id'];?>">
	<input name="flag" type="hidden" id="flag" value="update">
	<tr>
	<td><input name="veh_order" type="text" style="width:50px;" value="<?php echo $row1['veh_order'];?>"></td>
	<td><input name="veh_name" type="text" style="width:200px;" value="<?php echo stripslashes($row1['veh_name']);?>"></td>
	<td><input name="veh_code" type="text" style="width:100px;" value="<?php echo stripslashes($row1['veh_code']);?>"></td>
	<td><input name="submit<?php echo $row1['veh_id']?>" type="submit" value="Edit"></td>
	</tr>
	</form>
	<?php
	}
	?>
	</tbody>
	</table>


</td>
</tr>
</table>


</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
