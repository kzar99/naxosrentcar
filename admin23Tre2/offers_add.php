<?php
session_start();
include("../includes/connection.php");

$error_msg = '';
$flag = ( isset($_POST['flag'])? $_POST['flag'] : '');

//////////////////////
/// submit checks ////
/////////////////////
if ($flag=="submit")
{
	$category = ( isset($_POST['category'])? $_POST['category'] : '');
	$start1 = ( isset($_POST['start1'])? $_POST['start1'] : '');
	$end1 = ( isset($_POST['end1'])? $_POST['end1'] : '');
	$offer_text = ( isset($_POST['offer_text'])? $_POST['offer_text'] : '');
	$offer_price = ( isset($_POST['offer_price'])? $_POST['offer_price'] : '');
	$offer_duration = ( isset($_POST['offer_duration'])? $_POST['offer_duration'] : '');
	$is_winter = ( isset($_POST['is_winter'])? $_POST['is_winter'] : 'N');
	
	if ($category=="")
	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Please select a Category.</b></font></div>"; }
	else if ($start1=="")
	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Select the Starting Date of the Offer</b></font></div>"; }
	else if ($end1=="")
	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Select the Ending Date of the Offer</b></font></div>"; }	
	else if ($offer_text=="")
	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Please type some text regarding the offer</b></font></div>"; }	
	else if ($offer_price=="")
	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Please type the price of the offer</b></font></div>"; }
	else if ($offer_duration=="")
	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Please type the duration of the offer (in days)</b></font></div>"; }
	else
	{ $error_msg="ok"; }

	if ($error_msg=="ok") // do insert
	{
		// start1 date
		$a1 = explode("-",$start1);
		$mya1 = mktime (0, 0, 0, $a1[1], $a1[0], $a1[2]);
	
		// end1 date
		$b1 = explode("-",$end1);
		$myb1 = mktime (0, 0, 0, $b1[1], $b1[0], $b1[2]);
		
	$sql1 = "INSERT INTO  company_offers (date_start,date_end,offer_dur,car_cat,offer_text,offer_price,is_winter) VALUES 
	('$mya1','$myb1','$offer_duration','$category','$offer_text','$offer_price','$is_winter')";
	$result1 = mysql_query($sql1)  or die(mysql_error().'<p>'.$sql1.'</p>');
	$cat_id = mysql_insert_id(); // the last category id in table				


	$error_msg = "<div align=\"center\">Offer added succesfully!<br></div>";
	}

}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">&nbsp;</p>
<p align="center" class="font_bold">In this page you will add various offers you have.<br>
Remember to change the car prices according to your offers</p>
<?php
if ($error_msg!="")
{
echo $error_msg;
}
?>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" name="form1">
<input name="flag" type="hidden" value="submit">
<table width="90%"  border="0" cellspacing="0" cellpadding="3">
<tr>
<td width="30%" class="font_bold">Select category to apply offer</td>
<td width="70%">
<select name="category">
<option value="">Select Category</option>
<?php
$query1="SELECT * FROM company_category ORDER BY category ASC ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
$num_results1=mysql_num_rows($result1);
while ($myrow1 = mysql_fetch_array($result1))
{
?>
<option value="<?php echo $myrow1['cat_id'];?>" ><?php echo $myrow1['car_type'];?> - <?php echo $myrow1['category'];?> - <?php echo $myrow1['cat_cars'];?></option>
<?php
}
?>
</select></td>
</tr>
<tr>
  <td colspan="2" valign="top" class="font_bold">Give Price <input name="offer_price" type="text" size="10" value=""> &#8364; 
  and duration  <input type="textfield" name="offer_duration" size="10"  value=""> in days</td>
</tr>
<tr>
<td colspan="2" valign="top" class="font_bold">Offer starts 
from 
  <input name="start1" type="Text" id="start1" size="12"  value="">
<a href="javascript:cal1.popup();">
<img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a> 
until <input name="end1" type="Text" id="end1" size="12" value="">
<a href="javascript:cal2.popup();">
<img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>
</td>
</tr>
<tr>
<td colspan="2" valign="top" class="font_bold">
Is this a Winter Offer? 
<select name="is_winter" size="1">
<option value="Y">Yes</option>
<option value="N" selected="selected">No</option>
</select>
</td>
</tr>
<tr>
<td valign="top" class="font_bold">Type the text of offer</td>
<td><textarea name="offer_text" cols="50" rows="10"></textarea></td>
</tr>
<tr align="center">
<td colspan="2" valign="top" class="font_bold"><input type="submit" name="Submit" value="Register Offer"></td>
</tr>
</table>
</form>
<script language="JavaScript">
<!-- 
var cal1 = new calendar1(document.forms['form1'].elements['start1']);
cal1.year_scroll = true;
cal1.time_comp = false;
var cal2 = new calendar1(document.forms['form1'].elements['end1']);
cal2.year_scroll = true;
cal2.time_comp = false;
//-->
</script>	

</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
