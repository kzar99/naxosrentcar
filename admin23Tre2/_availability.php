<?php
session_start();
include("../includes/connection.php");

unset($company_areas);
unset($area_array);
unset($cat_array);

$company_areas = "";
$query1="SELECT location_id FROM ".$_SESSION['ses_company']."_locations ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
$total_areas = mysql_num_rows($result1); // total areas in company
while ($myrow1 = mysql_fetch_array($result1))
{
$company_areas .= $myrow1['location_id'].",";
}
$company_areas = substr($company_areas,0,-1); // area's ids to check


$area_array[] = " @@ ";
$query2="SELECT location_id,location_name FROM main_location_list WHERE location_id IN ($company_areas) ORDER BY location_name ASC ";
$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
while ($myrow2 = mysql_fetch_array($result2))
{
$loc_id = $myrow2['location_id'];
$loc_name = $myrow2['location_name'];
$val_to_insert = $loc_id."@@".$loc_name;
$area_array[] = $val_to_insert; // array with area_id@@area_name to create first column of table
}

$query21="SELECT location_id FROM main_location_list ORDER BY location_id DESC LIMIT 1 ";
$result21 = mysql_query($query21)  or die(mysql_error().'<p>'.$query21.'</p>');
while ($myrow21 = mysql_fetch_array($result21))
{ $max_area_id = $myrow21['location_id']; }


$query3="SELECT cat_id,category FROM ".$_SESSION['ses_company']."_category  ORDER BY category ASC ";
$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
while ($myrow3 = mysql_fetch_array($result3))
{
$cat_id = $myrow3['cat_id'];
$cat_name = $myrow3['category'];
$cat_to_insert = $cat_id."@@".$cat_name;
$cat_array[] = $cat_to_insert; // array with cat_id@@cat_name to create all the rest columns of table
}

$query22="SELECT cat_id FROM ".$_SESSION['ses_company']."_category ORDER BY cat_id DESC LIMIT 1 ";
$result22 = mysql_query($query22)  or die(mysql_error().'<p>'.$query22.'</p>');
while ($myrow22 = mysql_fetch_array($result22))
{ $max_cat_id = $myrow22['cat_id']; }


if ($flag=="insert")
{
$a = explode("-",$date_from);
$b = explode("-",$date_to);
$date_start = mktime(0,0,0,$a[1],$a[0],$a[2]);
$date_end = mktime(0,0,0,$b[1],$b[0],$b[2]);

	for ($i=1;$i<=$max_area_id;$i++)
	{
		for ($j=1;$j<=$max_cat_id;$j++)
		{
			$test_var = "avail_".$i."_".$j;
			$test_var_value = $$test_var;
			if ($test_var_value!="")
			{
			$sql5 = "INSERT INTO ".$_SESSION['ses_company']."_availability (date_from,date_to,car_cat,area,car_number) VALUES 
			('$date_start','$date_end','$j','$i','$test_var_value')  ";
			$result5 = mysql_query($sql5)  or die(mysql_error().'<p>'.$sql5.'</p>');	
			}
		}
	}
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
<style>
.font_small {
font-size:9px;
font-family:Arial;
}
</style>
</head>

<body leftmargin="5" topmargin="5">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="3"><?php include("elements_top.php"); ?></td>
</tr>
<tr valign="top">
<td width="200" rowspan="2"><?php include("menu_left.php"); ?></td>
<td width="800" align="center"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td colspan="2" valign="top">
<p class="font_bold" align="center"><br>
<a href="<?php echo $_SERVER['PHP_SELF'];?>?action=add">Add New</a></p>
<?php
if ($action=="add")
{
$k=0;
?>
	<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" name="form_add">
	<input name="flag" type="hidden" value="insert">
	<div align="center"> <span class="menu_title">Please fill the following form so as to setup the company's availability</span><br>
	Type the available car numbers per city per category in the selected dates<br><br>
	Choose date From: <input name="date_from" type="text" size="12"> 
	Choose date To: <input name="date_to" type="text" size="12"> 
	(example (dd-mm-YYY) <?php echo date("d-m-Y");?>)<br><br>
	
	
	<table width="99%"  border="0" cellspacing="0" cellpadding="0">
	<?php
	foreach ($area_array as $temp_area) // create rows
	{
	$a = explode("@@",$temp_area);
	$area_id = $a[0];
	$area_name = $a[1];
		if (trim($area_name)=="") // create first row of table
		{
	?>
	<tr>
	<td class="menu_title">Cities</td>
	<?php
			foreach ($cat_array as $category)
			{
			$b = explode("@@",$category);
			$cat_name = $b[1];
	?>
	<td class="menu_title" align="center"><?php echo $cat_name;?></td>
	<?php
			}
	?>
	</tr>
	<?php	
		}
		else
		{
		$k++;
	?>
	<tr>
	<td><strong><?php echo $k;?>.</strong> <?php echo $area_name;?></td>
	<?php
			foreach ($cat_array as $category1)
			{
			$b1 = explode("@@",$category1);
			$cat_id = $b1[0];
	?>
	<td align="center"><input name="avail_<?php echo $area_id;?>_<?php echo $cat_id;?>" type="text" size="5"></td>
	<?php
			}
	?>
	</tr>
	<?php
		} // end if area_name==""
	} // end creating rows
	?>
	<tr>
	<td>&nbsp;</td>
	<?php
			foreach ($cat_array as $category2)
			{
			$b2 = explode("@@",$category2);
			$cat_name = $b2[1];
	?>
	<td class="menu_title" align="center"><?php echo $cat_name;?></td>
	<?php
			}
	?>
	</tr>	
	</table>
	<input name="submit1" type="submit" value="Insert Data">
	</div>
	</form>
<?php
}
else // if action != "add"
{
?>
<div align="center"><span class="font_bold">You are seeing the car availabilities based on dates, cities and categories.<br>
  The number in parenthesis in the car colum, is the available cars for 
  that particular category in the selected dates</span><br>
  <br>
	  </div>
<table width="99%"  border="0" align="center" cellpadding="0" cellspacing="0" id="info_table">
	<tr align="center">
	<td width="18%" class="menu_title">Dates</td>
	<td width="45%" class="menu_title">Areas</td>
	<td width="30%" class="menu_title">Cars</td>
	<td width="7%" class="menu_title">Action</td>
	</tr>
<?php
	$query6="SELECT date_from,date_to FROM ".$_SESSION['ses_company']."_availability GROUP BY date_from,date_to ORDER BY date_from ";
	$result6 = mysql_query($query6)  or die(mysql_error().'<p>'.$query6.'</p>');
	while ($myrow6 = mysql_fetch_array($result6))
	{
	$date_from = date("d-m-Y",$myrow6['date_from']);
	$date_to = date("d-m-Y",$myrow6['date_to']);
	$date_from_db = $myrow6['date_from'];
	$date_to_db = $myrow6['date_to'];
?>
	<tr valign="top">
	<td><?php echo $date_from." - ".$date_to;?></td>
	<td class="font_small">
	<?php
		$query7="SELECT a.area,b.location_name,b.location_id FROM ".$_SESSION['ses_company']."_availability a, main_location_list b WHERE 
		a.date_from='$date_from_db' AND a.date_to='$date_to_db' AND a.area=b.location_id GROUP BY a.area ORDER BY b.location_name  ";
		$result7 = mysql_query($query7)  or die(mysql_error().'<p>'.$query7.'</p>');
		while ($myrow7 = mysql_fetch_array($result7))
		{
		echo $myrow7['location_name'].", ";
		}
	?>
	</td>
	<td>
	<?php
		$query8="SELECT a.car_cat,a.car_number,b.cat_id,b.category FROM ".$_SESSION['ses_company']."_availability a, ".$_SESSION['ses_company']."_category b WHERE 
		a.date_from='$date_from_db' AND a.date_to='$date_to_db' AND a.car_cat=b.cat_id GROUP BY a.car_cat ORDER BY b.category  ";
		$result8 = mysql_query($query8)  or die(mysql_error().'<p>'.$query8.'</p>');
		while ($myrow8 = mysql_fetch_array($result8))
		{
		echo $myrow8['category']."(".$myrow8['car_number']."), ";
		}
	?>	</td>
	<td align="center">
	<img src="images/button_properties.png" border="0"> | 
	<img src="images/button_drop.png" border="0"></td>
	</tr>
<?php
	}
?>
	</table>
<?php
} // end if action==add
?>


<p>&nbsp;</p></td>
</tr>
<tr align="center" valign="top">
<td colspan="3"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
