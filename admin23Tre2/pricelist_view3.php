<?php
session_start();
include("../includes/connection.php");
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Edit Price List</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="font_bold">Select Season</span> : Select a season from the drop down menu to open it's price table for editing.
There are five maximum seasons (low,medium,high,peak,other) for each company. You may use as many as you like, up to five</li>
<li><span class="font_bold">Select Car Category</span> : The prices you are about to enter are given for a specific car category (A,A1,A2,B,...)</li>
<li><span class="font_bold">% Payment</span> : write the percentage of money (&#8364;) the customer has to pay in advance, in order to rent the car 
through the web site. So if a company requires 10% of money up-front and you get a 10% commission of the sale, the user must pay a total of 20% of value 
in advance. So write down 0.2 (which is 20%). <span class="font_bold">NOTE:</span> you must write this number as 0.xx - a zero, A DOT and two digits 
(so a value of 0.28 means 28% while a value of 28.45% IS NOT POSSIBLE.</li>
<li><span class="font_bold">7 Money fields</span> : In each field (7) write down a price. Each price is taken from company's price list 
and must be unique for each season-car category combination. Total cost for the duration.</li>
<li><span class="font_bold">NOTE</span>: each price may be up to 9999.99 &#8364; (note the DOT(.) that seperates the decimal number. 
If there are no decimals, just write the number) Examples: 34 , 34.02 , 34.55 etc. </li>
<li><span class="font_bold">DAY 8+</span> : write the cost of a single day . For example in DEMO Company in Low season for category A, we will write 27 
(the last cost of low season). This number will be multiplied with the number of days from 8+ (8,9,10,...).</li>
</ul> 
</td>
</tr>
</table>

<?php
$query3="SELECT * FROM company_car_list_".$_POST['season']." WHERE cat_id='".$_POST['cat_id']."'  ";
$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
while ($myrow3 = mysql_fetch_array($result3))
{
	$dpososto = $myrow3['pososto'];
	$dprofit = $myrow3['profit'];
	$dday1 = $myrow3['day1'];
	$dday2 = $myrow3['day2'];
	$dday3 = $myrow3['day3'];
	$dday4 = $myrow3['day4'];
	$dday5 = $myrow3['day5'];
	$dday6 = $myrow3['day6'];
	$dday7 = $myrow3['day7'];
	$dday8 = $myrow3['day8'];

} // close connection3
?>
<form name="form2" method="post" action="pricelist_view4.php">
<div align="center">Edit Prices for Season <span class="user"><?php echo $_POST['season'];?></span> and Category <span class="user"><?php echo $_POST['cat_name'];?></span><br></div>
<input name="cat_id" type="hidden"  value="<?php echo $_POST['cat_id'];?>">
<input name="season" type="hidden"  value="<?php echo $_POST['season'];?>">
<input name="car_type" type="hidden"  value="<?php echo $_POST['car_type'];?>">
  <table width="98%"  border="0" align="center" cellpadding="3" cellspacing="0">
    <tr>
      <td colspan="3">Persentage of Payment </td>
      <td colspan="5"><input name="mypososto" type="text" id="mypososto" size="10" value="<?php echo $dpososto;?>"></td>
      </tr>
    <tr>
      <td colspan="3">Our Profit </td>
      <td colspan="5"><input name="myprofit" type="text" id="myprofit" size="10" value="<?php echo $dprofit;?>"></td>
      </tr>
    <tr>
      <td>Day1</td>
      <td>Day2</td>
      <td>Day3</td>
      <td>Day4</td>
      <td>Day5</td>
      <td>Day6</td>
      <td>Day7</td>
      <td>Day8+</td>
      </tr>
    <tr>
      <td><input name="myday1" type="text" id="myday1" size="10" value="<?php echo $dday1;?>"></td>
      <td><input name="myday2" type="text" id="myday2" size="10" value="<?php echo $dday2;?>"></td>
      <td><input name="myday3" type="text" id="myday3" size="10" value="<?php echo $dday3;?>"></td>
      <td><input name="myday4" type="text" id="myday4" size="10" value="<?php echo $dday4;?>"></td>
      <td><input name="myday5" type="text" id="myday5" size="10" value="<?php echo $dday5;?>"></td>
      <td><input name="myday6" type="text" id="myday6" size="10" value="<?php echo $dday6;?>"></td>
      <td><input name="myday7" type="text" id="myday7" size="10" value="<?php echo $dday7;?>"></td>
      <td><input name="myday8" type="text" id="myday8" size="10" value="<?php echo $dday8;?>"></td>
      </tr>
    <tr align="center">
      <td colspan="8">
	  <input name="Submit" type="submit" class="submit_button" value="Update Prices for Category <?php echo $_POST['car_type'];?>-<?php echo $_POST['cat_name'];?> in <?php echo $_POST['season'];?> Season"></td>
      </tr>
  </table>
</form>  



</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
