<?php
session_start();
include("../includes/connection.php");
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Edit Price List</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="font_bold">Select Season</span> : Select a season from the drop down menu to open it's price table for editing.
There are five maximum seasons (low,medium,high,peak,other) for each company. You may use as many as you like, up to five</li>
<li><span class="font_bold">Select Car Category</span> : The prices you are about to enter are given for a specific car category (A,A1,A2,B,...)</li>
<li><span class="font_bold">% Payment</span> : write the percentage of money (&#8364;) the customer has to pay in advance, in order to rent the car 
through the web site. So if a company requires 10% of money up-front and you get a 10% commission of the sale, the user must pay a total of 20% of value 
in advance. So write down 0.2 (which is 20%). <span class="font_bold">NOTE:</span> you must write this number as 0.xx - a zero, A DOT and two digits 
(so a value of 0.28 means 28% while a value of 28.45% IS NOT POSSIBLE.</li>
<li><span class="font_bold">31 Money fields</span> : In each field (31 total fields) write down a price. Each price is taken from company's price list 
and must be unique for each season-car category combination. So if you have a company with 4 price seasons and 19 car categories, you must write a total 
of 2356 prices (4x19x31). </li>
<li><span class="font_bold">NOTE</span>: each price may be up to 9999.99 &#8364; (note the DOT(.) that seperates the decimal number. 
If there are no decimals, just write the number) Examples: 34 , 34.02 , 34.55 etc. </li>
<li><span class="font_bold">DAY 31</span> : write the cost of a single day . For example in DEMO Company in Low season for category A, we will write 18.27 
(the last cost of low season). This number will be multiplied with the number of days from 31+ (31,32,33,...). Although DEMO Company has this limit in 
28th day, I set it at 31 (in case some other company has a greater range of days in it's price lists) </li>
</ul> 
</td>
</tr>
</table>

<div align="left" class="font_bold">
Select a Price Season/Period to Edit prices for cars
</div>
<form name="form1" method="post" action="pricelist_view2.php">

<select name="season">
<option value="aaa" selected>Select a season</option>
<option value="low">Low</option>
<option value="medium">Medium</option>
<option value="high">High</option>
<option value="peak">Peak</option>
<option value="other">Other</option>
</select>
<br>
<br>


	<select name="car_type">
	<?php
	$query1 = "SELECT * FROM vehicles ORDER BY veh_order ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($row1 = mysql_fetch_array($result1))
	{
	?>
	<option value="<?php echo stripslashes($row1['veh_code']);?>"><?php echo stripslashes($row1['veh_name']);?></option>
	<?php
	}
	?>

	</select>
<br>
<br>
<input name="Submit" type="submit" class="submit_button" value="Edit Prices of Selected Season/Category">
</form>




</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
