<?php
session_start();
include("../includes/connection.php");

$year = date("Y");
if ( isset($_GET['year']) ) { $year = trim($_GET['year']); }

$flag = '';
if (isset($_GET) && isset($_GET['flag'])) { $flag = $_GET['flag']; }

$id = '';
if (isset($_GET) && isset($_GET['id'])) { $id = $_GET['id']; }

if ($flag=="delete")
{
$query100 = "DELETE FROM rental WHERE id='$id' ";
$result100 = mysql_query($query100)  or die(mysql_error().'<p>'.$query100.'</p>');
}

if ($flag=="cancel")
{
$query100 = "UPDATE rental SET status='waiting approve in DeltaPay Merchant Panel' WHERE id='$id' ";
$result100 = mysql_query($query100)  or die(mysql_error().'<p>'.$query100.'</p>');
}

if ($flag=="ok")
{
$query100 = "UPDATE rental SET status='Rental Finished' WHERE id='$id' ";
$result100 = mysql_query($query100)  or die(mysql_error().'<p>'.$query100.'</p>');
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function NewWindow(mypage, myname, w, h, scroll) {
var winl = (screen.width - w) / 2;
var wint = (screen.height - h) / 2;
winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable = no'
win = window.open(mypage, myname, winprops)
if (parseInt(navigator.appVersion) >= 4) { win.window.focus(); }
}
</script>
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">&nbsp;</p>
<p align="center" class="font_bold">In this page you will see rentals that are complete but haven't been fullfield yet (date has not come).<br>
<span class="user">Delete</span> will remove the rental permanently from database - no recovery possible<br>
<span class="user">Cancel</span> will put the rental in the Unfinished Rentals List - meaning the rental company has to cancel/accept the rental <br>
<span class="user">Confirm</span> will put the rental in Closed Rental List - meaning the rental is fulfilled.</p>
<p align="center" class="font_bold">Finalized Rentals (Approved by company)</p>

<p><br>
<strong>Select year to see rentals</strong>: 
<?php 
$q1="
SELECT YEAR( STR_TO_DATE( start_date,  '%d-%m-%Y' ) ) AS yearList
FROM rental
WHERE STATUS =  'Company Approved'
AND session_id NOT LIKE  '' 
AND order_number>0 
GROUP BY yearList 
ORDER BY yearList  ";
$r1 = mysql_query($q1)  or die(mysql_error().'<p>'.$q1.'</p>');
while ($row1 = mysql_fetch_array($r1))
{
?>
<a href="?year=<?php echo $row1['yearList'];?>"><?php echo $row1['yearList'];?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
<?php
}
?><br><br>
</p>

<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr class="font_bold">
<td width="11%">&nbsp;</td>
<td width="11%">Requested</td>
<td width="15%">Company</td>
<td width="15%">Car cat. </td>
<td width="15%">Duration</td>
<td width="20%">Total Cost </td>
<td width="20%">Discount Code</td>
<td width="24%">Bank Status </td>
</tr>
<tr valign="top">
<td colspan="7"><hr></td>
</tr>
<?php
$query1="
SELECT * 
FROM rental 
WHERE status='Company Approved' 
AND session_id NOT LIKE '' 
AND YEAR( STR_TO_DATE( start_date,  '%d-%m-%Y' ) )='".$year."' 
AND order_number>0 
ORDER BY order_number DESC ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($myrow1 = mysql_fetch_array($result1))
{
$session_id = $myrow1['session_id'];
$company_id = $myrow1['company'];
$affiliate_id = $myrow1['affiliate_id'];
$discount_code = $myrow1['discount_code'];


	if ($affiliate_id>1)
	{
		$query_assoc="SELECT * FROM affiliates WHERE affiliate_id='$affiliate_id' ";
		$result_assoc = mysql_query($query_assoc)  or die(mysql_error().'<p>'.$query_assoc.'</p>');
		while ($myrow_assoc = mysql_fetch_array($result_assoc))
		{ $username = $myrow_assoc['username']; }
	}
	else { $username = ""; }
	
	$query_company="SELECT * FROM main_company_list WHERE comp_id='$company_id' ";
	$result_company = mysql_query($query_company)  or die(mysql_error().'<p>'.$query_company.'</p>');
	while ($myrow_company = mysql_fetch_array($result_company))
	{
	$company_name = $myrow_company['comp_name_title'];
	}
	
	
	$original_cost = $myrow1['total_cost'];
	$total_cost = $original_cost;
	
	if ($discount_code!="")
	{
		$query13="SELECT * FROM members WHERE discount_code='$discount_code' LIMIT 1 ";
		$result13 = mysql_query($query13)  or die(mysql_error().'<p>'.$query13.'</p>');
		if (mysql_num_rows($result13)>0)
		{	
		
			$query14="SELECT * FROM member_discount WHERE id=1 ";
			$result14 = mysql_query($query14)  or die(mysql_error().'<p>'.$query14.'</p>');
			while ($myrow14 = mysql_fetch_array($result14))
			{
			$discount_show = $myrow14['discount'];
			}
			
			$discount = (100-$discount_show)/100;
		}
		else
		{
		$discount = 0;
		}
				
		if ($discount>0)
		{
			$total_cost = number_format($total_cost*$discount,2);		
		}		
				
	}
	
?>
<tr valign="top">
<td nowrap><a href="view_request.php?session_id=<?php echo $session_id;?>" onClick="NewWindow(this.href,'name','600','500','yes');return false;"><strong>View Voucher <?php echo $myrow1['order_number'];?></strong></a></td>
<td><?php if ($myrow1['date_rent']>0) {echo date("D d M Y", $myrow1['date_rent']);}?></td>
<td><?php echo $company_name;?></td>
<td><?php echo $myrow1['category'];?></td>
<td><?php echo $myrow1['start_date'];?><br>      <?php echo $myrow1['end_date'];?></td>
<td><?php echo $total_cost;?> <?php if ($original_cost!=$total_cost) {?><br><?php echo $discount_show;?>% discount to original <?php echo $original_cost;?><?php } ?></td>
<td><?php echo $discount_code;?></td>
<td><?php echo $myrow1['bank_status'];?></td>
</tr>
<tr valign="top">
<td colspan="7"><?php echo $myrow1['full_name'];?>, Driver's age: <?php echo $myrow1['driver_age'];?>, Phone: <?php echo $myrow1['phone'];?>, Mobile: <?php echo $myrow1['mobile'];?>, Email: <?php echo $myrow1['email'];?>
<?php
if ($username!="")
{
?>
&nbsp;&nbsp;&nbsp;<span class="menu_title">from Affiliate: <?php echo $username;?></span>
<?php
}
?>
</td>
</tr>
<tr valign="top" class="font_bold">
<td colspan="7">Rental Status : <?php echo $myrow1['status'];?></td>
</tr>	
<tr valign="top">
<td colspan="7">
<a href="rentals_finished.php?flag=delete&id=<?php echo $myrow1['id'];?>">Delete Rental</a>
| <a href="rentals_finished.php?flag=cancel&id=<?php echo $myrow1['id'];?>">Cancel Rental</a>
| <a href="rentals_finished.php?flag=ok&id=<?php echo $myrow1['id'];?>">Confirm Rental</a>	
</td>
</tr>
<tr valign="top">
<td colspan="7"><hr></td>
</tr>
<?php
} // end connection 1
?> 
</table>




</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
