<?php
session_start();
include("../includes/connection.php");

if (isset($_POST['flag']) && $_POST['flag']=="pososto")
{
	$query2="UPDATE company_car_list_".$_POST['season']." SET pososto='".$_POST['mypososto']."'  ";
	$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
}

?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Edit Price List</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="font_bold">Select Season</span> : Select a season from the drop down menu to open it's price table for editing.
There are five maximum seasons (low,medium,high,peak,other) for each company. You may use as many as you like, up to five</li>
<li><span class="font_bold">Select Car Category</span> : The prices you are about to enter are given for a specific car category (A,A1,A2,B,...)</li>
<li><span class="font_bold">% Payment</span> : write the percentage of money (&#8364;) the customer has to pay in advance, in order to rent the car 
through the web site. So if a company requires 10% of money up-front and you get a 10% commission of the sale, the user must pay a total of 20% of value 
in advance. So write down 0.2 (which is 20%). <span class="font_bold">NOTE:</span> you must write this number as 0.xx - a zero, A DOT and two digits 
(so a value of 0.28 means 28% while a value of 28.45% IS NOT POSSIBLE.</li>
<li><span class="font_bold">7 Money fields</span> : In each field (7) write down a price. Each price is taken from company's price list 
and must be unique for each season-car category combination. Total cost for the duration.</li>
<li><span class="font_bold">NOTE</span>: each price may be up to 9999.99 &#8364; (note the DOT(.) that seperates the decimal number. 
If there are no decimals, just write the number) Examples: 34 , 34.02 , 34.55 etc. </li>
<li><span class="font_bold">DAY 8+</span> : write the cost of a single day . For example in DEMO Company in Low season for category A, we will write 27 
(the last cost of low season). This number will be multiplied with the number of days from 8+ (8,9,10,...).</li>
</ul> 
</td>
</tr>
</table>

<?php
if ($_POST['season']!="aaa" && $_POST['car_type']!="aaa")
{
?>
<table width="98%"  border="0" cellspacing="0" cellpadding="3" align="center">

<form action="" method="post">
<input name="flag" type="hidden" value="pososto">
<input name="season" type="hidden" value="<?php echo $_POST['season'];?>">
<input name="car_type" type="hidden" value="<?php echo $_POST['car_type'];?>">
<tr>
<td colspan="11" class="font_bold">Define Pre-payment for all categories in season: 
<input name="mypososto" type="text" id="mypososto" size="10" value="">  <input type="submit" name="Submit2" value="Set Profint in Season"></td>
</tr>
</form>
<tr>
<td class="font_bold">&nbsp;</td>
<td class="font_bold">&nbsp;</td>
<td class="font_bold">&nbsp;</td>
<td colspan="8" align="center">&nbsp;</td>
</tr>
<tr>
<td class="font_bold">category</td>
<td class="font_bold">Pre- Payment </td>
<td class="font_bold">Profit</td>
<td colspan="8" align="center">
<span class="menu_title">Prices  for <?php echo $_POST['car_type'];?> for <?php echo $_POST['season'];?> Season</span><br>
click on category name to edit it</td>
</tr>
<tr>
<td colspan="11" valign="middle" class="font_bold"><hr color="#993333" size="2">
<hr color="#993333" size="2"></td>
</tr>

<?php
$query2="SELECT * FROM company_category WHERE car_type='".$_POST['car_type']."' ORDER BY category ASC  ";
$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
while ($myrow2 = mysql_fetch_array($result2))
{
$cat_name = $myrow2['category']; 
$cat_id = $myrow2['cat_id'];

	$query1="SELECT * FROM company_car_list_".$_POST['season']." WHERE cat_id='$cat_id'  ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($myrow1 = mysql_fetch_array($result1))
	{
?>
		<tr>
		<td rowspan="2" align="center" valign="middle" class="font_bold">
		<form name="form<?php echo $cat_id; ?>" method="post" action="pricelist_view3.php">
		<input name="cat_id" type="hidden" value="<?php echo $cat_id; ?>">
		<input name="cat_name" type="hidden" value="<?php echo $cat_name; ?>">
		<input name="season" type="hidden" value="<?php echo $_POST['season']; ?>">
		<input name="car_type" type="hidden" value="<?php echo $_POST['car_type']; ?>">
		<input name="Submit" type="submit" class="submit_button" value="<?php echo $cat_name; ?>">
		</form>
		</td>
        <td rowspan="2" align="center" valign="middle" class="font_bold"><?php echo $myrow1['pososto'] * 100 ;?>%</td>
        <td rowspan="2" align="center" valign="middle" class="font_bold"><?php echo $myrow1['profit'] * 100 ;?>%</td>
        <td class="font_bold">Day1</td>
        <td class="font_bold">Day2</td>
        <td class="font_bold">Day3</td>
        <td class="font_bold">Day4</td>
        <td class="font_bold">Day5</td>
        <td class="font_bold">Day6</td>
        <td class="font_bold">Day7</td>
        <td class="font_bold">Day8+</td>
        </tr>
      	<tr>
        <td><?php echo $myrow1['day1']; ?></td>
        <td><?php echo $myrow1['day2']; ?></td>
        <td><?php echo $myrow1['day3']; ?></td>
        <td><?php echo $myrow1['day4']; ?></td>
        <td><?php echo $myrow1['day5']; ?></td>
        <td><?php echo $myrow1['day6']; ?></td>
        <td><?php echo $myrow1['day7']; ?></td>
        <td><?php echo $myrow1['day8']; ?></td>
        </tr>
      	<tr>
        <td colspan="11" valign="middle" class="font_bold"><hr color="#993333" size="2"><hr color="#993333" size="2"></td>
        </tr>
<?php
	} // close connection 2
} // close connection 1
?>
</table>
<?php
} // end if season!=aaa
?>



</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
