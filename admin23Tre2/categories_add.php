<?php
session_start();
include("../includes/connection.php");

$error_msg = '';

//////////////////////
/// submit checks ////
/////////////////////
if (isset($_POST) && isset($_POST['flag']) && $_POST['flag']=="submit")
{
	
	/*
	Array
	(
		[flag] => submit
		[car_type] => car
		[category] => test
		[description] => test andreas
		[cars] => test andreas
		[adults] => 1
		[children] => 1
		[large_bag] => 1
		[small_bag] => 1
		[has_ac] => Y
		[has_music] => Y
		[mileage] => test andreas
		[min_age] => 18
		[transmission] => manual
		[doors] => 0
		[Submit] => Register Category to 
	)

	*/
	
	$car_type 		= ( isset($_POST['car_type']) 		? $_POST['car_type'] 	: ''  );
	$category 		= ( isset($_POST['category']) 		? $_POST['category'] 	: ''  );
	$description 	= ( isset($_POST['description']) 	? $_POST['description'] : ''  );
	$cars 			= ( isset($_POST['cars']) 			? $_POST['cars'] 		: ''  );
	$adults 		= ( isset($_POST['adults']) 		? $_POST['adults'] 		: '1'  );
	$children 		= ( isset($_POST['children']) 		? $_POST['children'] 	: '0'  );
	$large_bag 		= ( isset($_POST['large_bag']) 		? $_POST['large_bag'] 	: '0'  );
	$small_bag 		= ( isset($_POST['small_bag']) 		? $_POST['small_bag'] 	: '0'  );
	$has_ac 		= ( isset($_POST['has_ac']) 		? $_POST['has_ac'] 		: 'N'  );
	$has_music 		= ( isset($_POST['has_music']) 		? $_POST['has_music'] 	: 'N'  );
	$mileage 		= ( isset($_POST['mileage']) 		? $_POST['mileage'] 	: ''  );
	$min_age 		= ( isset($_POST['min_age']) 		? $_POST['min_age'] 	: '18'  );
	$transmission 	= ( isset($_POST['transmission']) 	? $_POST['transmission'] : ''  );
	$doors 			= ( isset($_POST['doors']) 			? $_POST['doors'] 		: '0'  );
	
	
	if ($category=="")	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Provide the name of the Category (example: A1).</b></font></div>"; }
	else if ($cars=="")	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Write what cars the category has</b></font></div>"; }
	else				{ $error_msg="ok"; }



	if ($error_msg=="ok") // do insert
	{
	$sql1 = "INSERT INTO  company_category 
	(
		category, 
		description, 
		cat_cars, 
		car_type, 
		adults, 
		children, 
		large_bag, 
		small_bag, 
		has_ac, 
		has_music, 
		mileage,
		min_age,
		transmission,
		doors
	) 
	VALUES 
	(
		'".mysql_real_escape_string($category)."', 
		'".mysql_real_escape_string($description)."', 
		'".mysql_real_escape_string($cars)."', 
		'".mysql_real_escape_string($car_type)."', 
		'".mysql_real_escape_string($adults)."', 
		'".mysql_real_escape_string($children)."', 
		'".mysql_real_escape_string($large_bag)."', 
		'".mysql_real_escape_string($small_bag)."', 
		'".mysql_real_escape_string($has_ac)."', 
		'".mysql_real_escape_string($has_music)."',
		'".mysql_real_escape_string($mileage)."', 
		'".mysql_real_escape_string($min_age)."',
		'".mysql_real_escape_string($transmission)."',
		'".mysql_real_escape_string($doors)."' 
	)";
	$result1 = mysql_query($sql1)  or die(mysql_error().'<p>'.$sql1.'</p>');
	$cat_id = mysql_insert_id(); // the last category id in table

		//////////////////////////////////////////////////////////////////////////
		//// now will insert the category to all the season prices tables ////////
		//////////////////////////////////////////////////////////////////////////
		$sql2 = "INSERT INTO  company_car_list_low (cat_id) VALUES ('$cat_id')";
		$result2 = mysql_query($sql2)  or die(mysql_error().'<p>'.$sql2.'</p>');
		$sql3 = "INSERT INTO  company_car_list_high (cat_id) VALUES ('$cat_id')";
		$result3 = mysql_query($sql3)  or die(mysql_error().'<p>'.$sql3.'</p>');
		$sql4 = "INSERT INTO  company_car_list_medium (cat_id) VALUES ('$cat_id')";
		$result4 = mysql_query($sql4)  or die(mysql_error().'<p>'.$sql4.'</p>');
		$sql5 = "INSERT INTO  company_car_list_peak (cat_id) VALUES ('$cat_id')";
		$result5 = mysql_query($sql5)  or die(mysql_error().'<p>'.$sql5.'</p>');				
		$sql6 = "INSERT INTO  company_car_list_other (cat_id) VALUES ('$cat_id')";
		$result6 = mysql_query($sql6)  or die(mysql_error().'<p>'.$sql6.'</p>');						


	$error_msg = "<div align=\"center\">Category added succesfully!<br></div>";
	}

}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Add new Car Categories</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="font_bold">Category</span> : write the category's name, for example A, A1,A2,B, ...</li>
<li><span class="font_bold">Description</span> : Write a short descriptin if you like </li>
<li><span class="font_bold">Cars</span> : write the types of cars that are included in category, for example, Fiat seicento1.1, ... </li>
<li><span class="font_bold">Number of Cars</span> : write how many cars we have available for rent. Default value is 10000 (unlimited) </li>
</ul>
</td>
</tr>
</table>
<?php
if ($error_msg!="")
{
echo $error_msg;
}
?>
<form name="form1" method="post" action="".$_SERVER['PHP_SELF']."">
<input name="flag" type="hidden" id="flag" value="submit">
<table width="50%"  border="0" align="center" cellpadding="3" cellspacing="0">
<tr valign="top">
<td>Type</td>
<td>
	<select name="car_type">
	<?php
	$query1 = "SELECT * FROM vehicles ORDER BY veh_order ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($row1 = mysql_fetch_array($result1))
	{
	?>
	<option value="<?php echo stripslashes($row1['veh_code']);?>"><?php echo stripslashes($row1['veh_name']);?></option>
	<?php
	}
	?>

	</select>
</td>
</tr>
<tr valign="top">
<td width="50%">Category</td>
<td width="50%"><input name="category" type="text" id="category" size="10" maxlength="50"></td>
</tr>
<tr valign="top">
<td>Description</td>
<td><textarea name="description" rows="3" id="description"></textarea></td>
</tr>
<tr valign="top">
<td>Cars</td>
<td><textarea name="cars" rows="2" id="cars"></textarea></td>
</tr>
<tr>
<td>Number of Adults </td>
<td>
<select name="adults">
<?php
for ($j=1;$j<=10;$j++)
{
?>
<option value="<?php echo $j;?>"><?php echo $j;?></option>
<?php
}
?>
</select>
</td>
</tr>
<tr>
<td>Number of Children</td>
<td>
<select name="children">
<?php
for ($k=0;$k<=5;$k++)
{
?>
<option value="<?php echo $k;?>"><?php echo $k;?></option>
<?php
}
?>
</select>
</td>
</tr>
<tr>
<td>Number of large baggages</td>
<td>
<select name="large_bag">
<?php
for ($l=0;$l<=10;$l++)
{
?>
<option value="<?php echo $l;?>"><?php echo $l;?></option>
<?php
}
?>
</select
></td>
</tr>
<tr>
<td>Number of small baggages</td>
<td>
<select name="small_bag">
<?php
for ($m=0;$m<=10;$m++)
{
?>
<option value="<?php echo $m;?>"><?php echo $m;?></option>
<?php
}
?>
</select>
</td>
</tr>
<tr>
<td>Has A/C</td>
<td>
Yes <input name="has_ac" type="radio" value="Y"> 
No <input name="has_ac" type="radio" value="N" checked>
</td>
</tr>
<tr>
<td>Has Music</td>
<td>
Yes <input name="has_music" type="radio" value="Y"> 
No <input name="has_music" type="radio" value="N" checked>
</td>
</tr>

<tr>
<td>Mileage</td>
<td><input name="mileage" type="text"></td>
</tr>

<tr>
<td>Minimum Age</td>
<td>
<select name="min_age">
<?php
for ($a=18;$a<=60;$a++)
{
?>
<option value="<?php echo $a;?>"><?php echo $a;?></option>
<?php
}
?>
</select>
</td>
</tr>

<tr>
<td>Transmission </td>
<td>
<select name="transmission">
<option value="manual">Manual</option>
<option value="automatic">Automatic</option>
</select>
</td>
</tr>

<tr>
<td>Number of doors</td>
<td>
<select name="doors">
<?php
for ($d=0;$d<=10;$d++)
{
?>
<option value="<?php echo $d;?>"><?php echo $d;?></option>
<?php
}
?>
</select>
</td>
</tr>


<tr align="center" valign="top">
<td colspan="2"><input name="Submit" type="submit" class="submit_button" value="Register Category to <?php echo $_SESSION['ses_company'];?>"></td>
</tr>
</table>
</form>



</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
