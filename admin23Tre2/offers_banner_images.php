<?php
session_start();
include("../includes/connection.php");
include "snapshot.class.php";
$my_width = 173;
$my_height = 115;
$maximum_images = 4;
$main_folder = '../special_offer_images';

if ($_POST['flag']=="upload")
{
		$myimage = new ImageSnapshot;
		$myimage->ImageField = $_FILES['userfile'];
		//$myimage->ImageFile = "images/".$test_image;
		$myimage->Width = $my_width;
		$myimage->Height = $my_height;
		$myimage->Resize = "true"; //if false, snapshot takes a portion from the unsized image.
		$myimage->ResizeScale = 100;
		$myimage->Position = "center";
		$myimage->Compression = 100;
		
		//saving to a filename
		if ($myimage->SaveImageAs($main_folder."/".$_FILES['userfile']['name']))
		{
		$old_name = $_FILES['userfile']['name'];
		$new_name = strtolower(str_replace(" ","_",$old_name));
		rename($main_folder."/".$old_name, $main_folder."/".$new_name);
		
			$sql_edit = "INSERT INTO special_offers_images (image_name) VALUES ('$new_name') ";
			$result_edit = mysql_query($sql_edit)  or die(mysql_error().'<p>'.$sql_edit.'</p>');			
		}
		else
		{
			$error_msg .= $myimage->Err;
		}		
}

if ($_GET['flag']=="delete_image")
{
unlink($main_folder."/".$_GET['image_name']);

			$sql_edit = "DELETE FROM special_offers_images  WHERE image_name='".$_GET['image_name']."' ";
			$result_edit = mysql_query($sql_edit)  or die(mysql_error().'<p>'.$sql_edit.'</p>');

clearstatcache();
}

if ($_GET['flag']=="active")
{

	$sql_edit = "UPDATE special_offers_images  SET selected='N' ";
	$result_edit = mysql_query($sql_edit)  or die(mysql_error().'<p>'.$sql_edit.'</p>');

	$sql_edit2 = "UPDATE special_offers_images  SET selected='Y' WHERE image_name='".$_GET['image_name']."' ";
	$result_edit2 = mysql_query($sql_edit2)  or die(mysql_error().'<p>'.$sql_edit2.'</p>');
clearstatcache();
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">&nbsp;</p>
<p align="center" class="font_bold">
In this page you may edit/change the image of the special offers link.<br>
Image will be resized to <?php echo $my_width;?> x <?php echo $my_height;?><br>
You are allowed to upload up to <?php echo $maximum_images;?> images<br>
Only jpg images are allowed<br>
</p>
<?php
if ($error_msg!="")
{
echo $error_msg;
}
?>
<p>&nbsp;</p>
<?php
$query201="SELECT * FROM special_offers_images ORDER BY image_name ";
$result201 = mysql_query($query201)  or die(mysql_error().'<p>'.$query201.'</p>');
$total_images_found = mysql_num_rows($result201);

if ($total_images_found<$maximum_images)
{
?>
<form name="test" method="post" action="" enctype="multipart/form-data">
<input name="flag" type="hidden" value="upload">
<input type="file" name="userfile">
<input type="submit" name="Submit" value="Upload Image">
</form>
<?php
}

if ($total_images_found>0)
{
?>
<table width="41%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="65%" align="center" nowrap class="menu_title">Uploaded Images </td>
<td width="26%" align="center" nowrap class="menu_title">Make Active </td>
<td width="9%" align="center" nowrap class="menu_title">Delete</td>
</tr>
<?php
	while ($row201 = mysql_fetch_array($result201))
	{
?>
<tr>
<td align="center"><img src="<?php echo $main_folder;?>/<?php echo $row201['image_name'];?>"></td>
<td align="center">
<?php
if ($row201['selected']=="N")
{
?>
<a href="?flag=active&image_name=<?php echo $row201['image_name'];?>">active</a>
<?php
}
else
{
?>
<span class="user">ACTIVE</span>
<?php
}
?></td>
<td align="center"><a href="?flag=delete_image&image_name=<?php echo $row201['image_name'];?>">delete</a></td>
</tr>
<?php
	}
?>
</table>
<?php
}
?>


</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
