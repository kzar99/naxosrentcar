<?php
session_start();
include("../includes/connection.php");


$error_message = "";

$max_width = 800;
$min_width = 300;

$thumb_width = 100;
$thumb_height = 75;

$flag = (empty($_POST['flag'])) ? '' : $_POST['flag'];



/////////////////////////////////////////////////
//
// check folders
//
$user_dir = "../gallery_images"; // gallery directory
if (!is_dir($user_dir)) { mkdir ($user_dir, 0777); }
$thumb_dir = $user_dir."/thumbs"; // thumbs directory
if (!is_dir($thumb_dir)) { mkdir ($thumb_dir, 0777); }
clearstatcache();



if ($flag=="upload_image")
{

	$test_file_name = strtolower(str_replace(" ", "_", $_FILES['picture']['name']));
	$a = explode(".", $test_file_name);

	
	//
	// check file type to be valid
	//
	if (strtolower($a[1])=="jpg" || strtolower($a[1])=="jpeg") {} 
	else { $error_message .= "<p>Wrong file type. Please use only jpg images</p>"; }

	
	if ($error_message=="")
	{
		$uploadfile = $user_dir."/".$test_file_name;
		if (!copy($_FILES['picture']['tmp_name'], $uploadfile))
		{
		$error_message .= "<p>Upload failed. Please retry</p>";
		}
		else
		{
			// https://www.verot.net/php_class_upload.htm
			$foo = new \Verot\Upload\Upload($_FILES['picture']); 
			if ($foo->uploaded) {
				
				$original_width = $foo->image_src_x;
				$original_height = $foo->image_src_y;			
			
				// check for lower than $min_width (defined in top of this page) width and deny if true
				if ($original_width<$min_width)	{
					
					unlink($user_dir."/".$test_file_name);
					$error_msg .= "<p>Image width smaller than ".$min_width." pixels. Upload stoped and image deleted</p>";
					
				}			
				else 
				{
			
					//
					// check for greater than $max_width (defined in top of this page) width and resize if true
					//			
					if ($original_width>$max_width || $original_height>$max_width) {
						
						// reseting after Proccess
						$foo->file_new_name_body 	= $a[0];
						$foo->file_new_name_ext 	= 'jpg';
						$foo->file_safe_name = true; // formats the filename (spaces changed to _)
						$foo->file_force_extension = true; // forces an extension if there is't any
						$foo->file_overwrite = true; // sets behaviour if file already exists
						$foo->auto_create_dir = true; // automatically creates destination directory if missing
						$foo->dir_auto_chmod = true; // automatically attempts to chmod the destination directory if not writeable
						$foo->dir_chmod = 0777; // chmod used when creating directory or if directory not writeable
						$foo->jpeg_quality = 100;
						$foo->image_convert = 'jpg'; // if set, image will be converted (possible values : ''|'png'|'jpeg'|'gif'|'bmp'; default: '')
						$foo->allowed = array('image/*'); // array of allowed mime-types (or one string). wildcard accepted, as in image/* (default: check Init)
						$foo->mime_check = true; // sets if the class check the MIME against the allowed list
						$foo->image_resize = true;
						$foo->image_x = $max_width;
						$foo->image_ratio_y = true;


						$foo->Process($user_dir);
						if ($foo->processed) 
						{
						} 
						else 
						{
							$foo->Clean(); // clean up before showing error
						
							$error_message .= $image_name.': '.$foo->error;
						}					
					}
					
					
					//
					// create thumb
					//			
					$foo->file_new_name_body 	= $a[0];
					$foo->file_new_name_ext 	= 'jpg';
					$foo->file_safe_name = true; // formats the filename (spaces changed to _)
					$foo->file_force_extension = true; // forces an extension if there is't any
					$foo->file_overwrite = true; // sets behaviour if file already exists
					$foo->auto_create_dir = true; // automatically creates destination directory if missing
					$foo->dir_auto_chmod = true; // automatically attempts to chmod the destination directory if not writeable
					$foo->dir_chmod = 0777; // chmod used when creating directory or if directory not writeable
					$foo->jpeg_quality = 100;
					$foo->image_convert = 'jpg'; // if set, image will be converted (possible values : ''|'png'|'jpeg'|'gif'|'bmp'; default: '')
					$foo->allowed = array('image/*'); // array of allowed mime-types (or one string). wildcard accepted, as in image/* (default: check Init)
					$foo->mime_check = true; // sets if the class check the MIME against the allowed list
					$foo->image_resize = true;
					$foo->image_x = $thumb_width;
					$foo->image_y = $thumb_height;

					$foo->Process($thumb_dir);
					if ($foo->processed) 
					{
					} 
					else 
					{
						$foo->Clean(); // clean up before showing error
					
						$error_message .= $image_name.': '.$foo->error;
					}					
								
				}
			}



		} // end if (!copy($_FILES['picture']['tmp_name'], $uploadfile))
	} // end if ($error_msg=="")

	clearstatcache();
} // end if ($flag=="upload_image")





if ($flag=="delete_image")
{

	unlink($user_dir."/".$_POST['image_name']);
	unlink($thumb_dir."/".$_POST['image_name']);

clearstatcache();
}



$my_dir = dir($thumb_dir);
while(($file2 = $my_dir->read()) !== false)
{
$ext2=explode('.',$file2);
	if($file2 != '.' && $file2 != '..' && $ext2[1] && $file2!="Thumbs.db")
	{
	$array_image[] = $file2;
	}
}

if ($array_image!="")
{
sort($array_image);
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function CaricaFoto(img)
{
  foto1= new Image();
  foto1.src=(img);
  Controlla(img);
}
function Controlla(img)
{
  if((foto1.width!=0)&&(foto1.height!=0))
  {
    viewFoto(img);
  }
  else
  {
    funzione="Controlla('"+img+"')";
    intervallo=setTimeout(funzione,20);
  }
}
function viewFoto(img)
{
  largh=foto1.width+20;
  altez=foto1.height+20;
  stringa="width="+largh+",height="+altez;
  finestra=window.open(img,"",stringa);
}
//  End -->
</script>

<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">&nbsp;</p>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data" name="form_upload">
<input name="flag" type="hidden" value="upload_image">
<table width="98%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" valign="middle"><input name="picture" type="file">  <input name="Submit1" type="submit" id="Submit1" class="td_orange_bg font_dark_blue_bold" value="Upload Image"></td>
</tr>
</table>
</form>
<p>&nbsp;</p>
<?php
if ($array_image!="")
{
?>
	<table width="95%"  border="0" cellspacing="0" cellpadding="0">
	<?php		
	foreach ($array_image as $img)
	{
	$image_info = getimagesize($user_dir."/".$img);
	?>
	<tr>
	<td width="200" align="center" valign="top" style="border-bottom:1px solid #FFFFFF; ">
	<a href="javascript:CaricaFoto('<?php echo $user_dir?>/<?php echo $img;?>')" border="0"><img src="<?php echo $thumb_dir?>/<?php echo $img;?>" border="0"></a>
	</td>
	<td align="left" valign="top" style="border-bottom:1px solid #FFFFFF; ">
	
		<table width="55%"  border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td width="67%" nowrap class="greeka">
		<strong>File name:</strong> <?php echo $img;?><br>
		<strong>Dimensions:</strong> <?php echo $image_info[0];?> x <?php echo $image_info[1];?> pixels<br>	</td>
		<td width="33%" nowrap class="greeka">
		<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
		<input name="flag" type="hidden" value="delete_image">
		<input name="image_name" type="hidden" value="<?php echo $img;?>">
		<input name="submit2" type="submit" value="Delete Image">
		</form>
		</td>
		</tr>
		</table>
	<?php
	unset($image_info);
	?>	</td>
	</tr>
	<?php
	}
	?>
	</table>

<?php
}
?>
<p>&nbsp;</p>

</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
