<?php
error_reporting(E_ALL);
session_start();
include("../includes/connection.php");



//////////////////////////////
//////// delete //////////////
//////////////////////////////
if (isset($_GET['flag']) && $_GET['flag']=="delete")
{
	$sql2 = "DELETE FROM company_extras WHERE ex_id='".$_GET['id']."' ";
	$result2 = mysql_query($sql2)  or die(mysql_error().'<p>'.$sql2.'</p>');
}


//////////////////////////////
//////// update //////////////
//////////////////////////////
if (isset($_POST['flag']) && $_POST['flag']=="update_basic")
{
	$sql5 = "UPDATE company_extras SET 
	ex_type='".$_POST['ex_type']."',
	profit='".$_POST['profit']."',
	low_price='".$_POST['low_price']."',
	max_price='".$_POST['max_price']."',
	extra_description='".$_POST['extra_description']."' 
	WHERE ex_id='".$_POST['id']."' ";
	$result15 = mysql_query($sql5)  or die(mysql_error().'<p>'.$sql5.'</p>');
}


if (isset($_POST['flag']) && $_POST['flag']=="update_cover")
{
	/*
	echo '<pre>';
	print_r($_POST);
	exit();
	

	Array
	(
		[flag] => update_cover
		[id] => 67
		[extras_id] => 67
		[name] => 0.00
		[max_price] => 25.00
		[description] =>  Road Service
		[categories] => Array
			(
				[0] => 80
				[1] => 65
				[2] => 81
				[3] => 105
				[4] => 99
				[5] => 30
				[6] => 97
				[7] => 115
				[8] => 82
				[9] => 106
				[10] => 36
			)

		[low_price1] => 0.00
		[low_price2] => 0.00
		[low_price3] => 0.00
		[low_price4] => 0.00
		[low_price5] => 0.00
		[Submit] => Update Info
	)
	)
	*/
	
	$car_array="";
	if (isset($_POST['categories']) && count($_POST['categories'])>0)
	{
		/*
		foreach ($_POST['categories'] as $catg)
		{
		$car_array .= $catg.",";
		}
		*/
		
		$car_array = implode("," , $_POST['categories']);
	}

	if (isset($_POST['is_new']) && $_POST['is_new']=="Y")
	{
		$sql4 = "INSERT INTO  company_extras 
		(
			extras_id, 
			max_price, 
			profit, 
			extra_description, 
			type, 
			car_cat, 
			low_price1, 
			low_price2, 
			low_price3, 
			low_price4, 
			low_price5
		) 
		VALUES 
		(
			'".$_POST['extras_id']."',
			'".$_POST['max_price']."',
			'".$_POST['profit']."',
			'".$_POST['description']."',
			'coverage',
			'$car_array',
			'".$_POST['low_price1']."',
			'".$_POST['low_price2']."',
			'".$_POST['low_price3']."',
			'".$_POST['low_price4']."',
			'".$_POST['low_price5']."' 
		)";
		$result4 = mysql_query($sql4)  or die(mysql_error().'<p>'.$sql4.'</p>');
	}
	else
	{
		$sql5 = "UPDATE company_extras SET 
		max_price='".$_POST['max_price']."',
		profit='".$_POST['profit']."',
		low_price1='".$_POST['low_price1']."',
		low_price2='".$_POST['low_price2']."',
		low_price3='".$_POST['low_price3']."',
		low_price4='".$_POST['low_price4']."',
		low_price5='".$_POST['low_price5']."',
		extra_description='".$_POST['description']."',
		car_cat='$car_array' 
		WHERE ex_id='".$_POST['id']."' ";
		$result15 = mysql_query($sql5)  or die(mysql_error().'<p>'.$sql5.'</p>');
	}
}

?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">&nbsp;</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="font_bold">Prices</span> : money format must be xxx or xxx<span class="font_bold">.</span>yy (note the <span class="font_bold">dot (.)</span> ) <span class="font_bold">Max number possible is 99999.99. </span>All prices must be in euros (&#8364;)</li>
<li><span class="font_bold">Low Price</span> : this is the cost per day for the extra </li>
<li><span class="font_bold">Max Price</span> : this is the max allowed cost. If duration of rental x low cost is greater than the max price, the max price will be used as a fixed cost (one time charge only). Use something like 1,000&#8364; (in which case it represents a rental of 330 days if the extra costs 3&#8364; per day) OR a normal max price that will be used if needed. </li>
<li><span class="font_bold">Zero costs</span> : Zero cost extras are given for free by the company</li>
<li><span class="font_bold">Profit</span> : use this option to increase or decrease the basic cost of the selected extra</li>
<li><span class="font_bold">Insert as new record</span> : check this option (when you edit coverage extras) to insert the selected extra as new record to database with different categories selected for it </li>
</ul>
</td>
</tr>
</table>


<?php
//////////////////////////////
//////// edit basic //////////
//////////////////////////////
if (isset($_GET['flag']) && $_GET['flag']=="edit_basic")
{

	$query4="SELECT * FROM company_extras WHERE ex_id='".$_GET['id']."' ";
	$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
	while ($myrow4 = mysql_fetch_array($result4))
	{
?>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
<input name="flag" type="hidden" value="update_basic">
<input name="id" type="hidden" value="<?php echo $_GET['id'];?>">
<table width="50%"  border="0" align="center" cellpadding="3" cellspacing="0">
<tr>
<td>Name</td>
<td><?php echo $exname;?></td>
</tr>
<tr>
<td>Category</td>
<td>
Car <input name="ex_type" type="radio" value="C" <?php if ($myrow4['ex_type']=="C"){echo ' checked';}?>> 
Other <input name="ex_type" type="radio" value="O" <?php if ($myrow4['ex_type']=="O"){echo ' checked';}?>>
</td>
</tr>
<tr>
<td>Profit</td>
<td><input name="profit" type="text" id="profit" value="<?php echo $myrow4['profit'];?>"></td>
</tr>
<tr>
<td width="50%">Low Price</td>
<td width="50%"><input name="low_price" type="text" id="low_price" value="<?php echo $myrow4['low_price'];?>"></td>
</tr>
<tr>
<td>Max Price</td>
<td><input name="max_price" type="text" id="max_price" value="<?php echo $myrow4['max_price'];?>"></td>
</tr>
<tr>
<td>Description</td>
<td><textarea name="extra_description" rows="4" id="extra_description"><?php echo $myrow4['extra_description'];?></textarea></td>
</tr>
<tr align="center">
<td colspan="2"><input name="Submit" type="submit" class="submit_button" value="Update Info"></td>
</tr>
</table>
</form>	
<?php
	}  // close 4
} // close if ($flag=="edit")







//////////////////////////////
//////// edit cover //////////
//////////////////////////////
if (isset($_GET['flag']) && $_GET['flag']=="edit_cover")
{
	$query4="
SELECT 
	* 
FROM 
	company_extras 
WHERE 
	ex_id='".$_GET['id']."' 
  ";
	$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
	while ($myrow4 = mysql_fetch_array($result4))
	{
	$extra_name = $myrow4['extras_name'];
	$max_price = $myrow4['max_price'];
	$extras_id = $myrow4['ex_id'];
	$profit = $myrow4['profit'];
	$description = $myrow4['extra_description'];
	$low_price1 = $myrow4['low_price1'];
	$low_price2 = $myrow4['low_price2'];
	$low_price3 = $myrow4['low_price3'];
	$low_price4 = $myrow4['low_price4'];
	$low_price5 = $myrow4['low_price5'];
	$car_cat = $myrow4['car_cat'];
	}
	
	$query5="SELECT * FROM company_periods  ";
	$result5 = mysql_query($query5)  or die(mysql_error().'<p>'.$query5.'</p>');
	$seasons = mysql_num_rows($result5);
?>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
<input name="flag" type="hidden" value="update_cover">
<input name="id" type="hidden" value="<?php echo $_GET['id'];?>">
<input name="extras_id" type="hidden" value="<?php echo $extras_id;?>">
<table width="70%"  border="0" align="center" cellpadding="0" cellspacing="0">
<tr class="font_bold">
<td colspan="2" class="submit_button">for <?php echo $extra_name;?> select to edit:</td>
</tr>
<tr>
<td valign="top"><span class="font_bold">Name :</span>  <input name="name" type="text" value="<?php echo $extra_name;?>" size="30" readonly><br><br>
<span class="font_bold">Profit :</span>  <input name="profit" type="text" value="<?php echo $profit;?>" size="30"><br><br>
<span class="font_bold">Max Price :</span>  <input name="max_price" type="text" value="<?php echo $max_price;?>" size="30"><br><br>
Insert as new record : <input name="is_new" type="checkbox" value="Y">
</td>
<td valign="top">
<span class="font_bold">Description :</span>  <textarea name="description" cols="40" rows="3" id="description"><?php echo $description;?></textarea>
</td>
</tr>
<tr class="font_bold">
<td>car categories to apply</td>
<td valign="top">the season price per day</td>
</tr>
<tr>
<td width="50%" valign="top">
<?php
	$query6="SELECT * FROM company_category ORDER BY category ";
	$result6 = mysql_query($query6)  or die(mysql_error().'<p>'.$query6.'</p>');
	while ($myrow6 = mysql_fetch_array($result6))
	{
	$category_name = $myrow6['category'];
	$category_id = $myrow6['cat_id'];
?>
<div style="width:45%; float:left; ">
<input name="categories[]" type="checkbox" value="<?php echo $category_id;?>"
	<?php
	$a = explode(",",$car_cat);
	$b = count($a);
	for ($i=0;$i<=$b;$i++)
	{
		if ( isset($a[$i]) && $a[$i]==$category_id) { echo " checked ";}
	}
	?>
> <?php echo $category_name;?> 
</div>  
<?php	
	}
?></td>
<td width="50%" valign="top">
	
<table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
<?php
if ($seasons>0)
{
?>
<tr>
<td>season1 <input name="low_price1" type="text" size="15" value="<?php echo $low_price1;?>"></td>
</tr>
<?php
	if ($seasons>1)
	{
?>
<tr>
<td>season2 <input name="low_price2" type="text" size="15" value="<?php echo $low_price2;?>"></td>
</tr>
<?php
		if ($seasons>2)
		{
?>
<tr>
<td>season3 <input name="low_price3" type="text" size="15" value="<?php echo $low_price3;?>"></td>
</tr>
<?php
			if ($seasons>3)
			{
?>
<tr>
<td>season4 <input name="low_price4" type="text" size="15" value="<?php echo $low_price4;?>"></td>
</tr>
<?php
				if ($seasons==5)
				{
?>
<tr>
<td>season5 <input name="low_price5" type="text" size="15" value="<?php echo $low_price5;?>"></td>
</tr>
<?php
				} // close if seasons=5
			} // close if seasons>3
		} // close if seasons>2
	} // close if seasons>1
} // close if seasons>0
?>
</table>
	
</td>
</tr>
</table>
<div align="center"><input name="Submit" type="submit" class="submit_button" value="Update Info"></div>
</form>	
<?php	
} // close if ($flag=="edit_cover")
?>




<table width="80%"  border="0" align="center" cellpadding="0" cellspacing="0">
<tr align="center" class="font_bold">
<td colspan="6" class="submit_button">BASIC EXTRAS</td>
</tr>
<tr class="font_bold">
<td width="36%">Extras Name</td>
<td width="12%">Profit(%)</td>
<td width="13%">Low Price</td>
<td width="17%">Max Price</td>
<td colspan="2" align="center">Action</td>
</tr>
<tr>
<td colspan="6"><hr color="#993333" size="2"></td>
</tr>
<?php
$query1="
SELECT 
	* 
FROM 
	company_extras  
WHERE 
	type='basic' 
ORDER BY 
	ex_type,extras_name 
	";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($myrow1 = mysql_fetch_array($result1))
{
$extra_name=$myrow1['extras_name'];
?>
<tr>
<td>(<?php if ($myrow1['ex_type']=="C"){echo 'car';} else { echo 'other';}?>) <strong><?php echo $extra_name;?></strong></td>
<td><?php echo $myrow1['profit'];?></td>
<td><?php echo $myrow1['low_price'];?></td>
<td><?php echo $myrow1['max_price'];?></td>
<td width="11%" align="center">
<a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=edit_basic&id=<?php echo $myrow1['ex_id'];?>&exname=<?php echo $extra_name;?>">
<img src="images/button_properties.png" alt="Edit Extra" width="18" height="13" border="0"></a></td>
<td width="11%" align="center">
<a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=delete&id=<?php echo $myrow1['ex_id'];?>">
<img src="images/button_drop.png" alt="Remove Extra" width="11" height="13" border="0"></a></td>
</tr>
<tr>
<td colspan="6"><?php echo $myrow1['extra_description'];?></td>
</tr>
<tr>
<td colspan="6"><hr color="#993333" size="2"></td>
</tr>
<?php
}
?>
</table>



<table width="80%"  border="0" align="center" cellpadding="0" cellspacing="0">
<tr align="center" class="font_bold">
<td colspan="11" class="submit_button">COVERAGE EXTRAS</td>
</tr>
<tr class="font_bold">
<td>&nbsp;</td>
<td align="center">&nbsp;</td>
<td align="center">&nbsp;</td>
<td align="center">&nbsp;</td>
<td colspan="5" align="center" bgcolor="#999999">price per day</td>
<td colspan="2" align="center">&nbsp;</td>
</tr>
<tr class="font_bold">
<td width="25%">Extras Name</td>
<td width="11%" align="center">Profit(%)</td>
<td width="11%" align="center">Max Cost </td>
<td width="11%" align="center">Car Cat.</td>
<td width="11%" align="center">Season1</td>
<td width="9%" align="center">Season2</td>
<td width="10%" align="center">Season3</td>
<td width="7%" align="center">Season4</td>
<td width="8%" align="center">Season5</td>
<td colspan="2" align="center">Action</td>
</tr>
<tr>
<td colspan="11"><hr color="#993333" size="2"></td>
</tr>
<?php
$query1="
SELECT 
	* 
FROM 
	company_extras  
WHERE 
	type='coverage' 
ORDER BY 
	extras_name, 
	car_cat 
	";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($myrow1 = mysql_fetch_array($result1))
{
$extra_name=$myrow1['extras_name'];
?>
<tr>
<td class="font_bold"><?php echo $extra_name;?></td>
<td align="center"><?php echo $myrow1['profit'];?></td>
<td align="center"><?php echo $myrow1['max_price'];?></td>
<td align="center" valign="top">
<?php
$a = explode(",",$myrow1['car_cat']);
$b = count($a);
for ($i=0;$i<=$b;$i++)
{
	if ( isset($a[$i]) )
	{
		$cat = $a[$i];
		$query7="SELECT * FROM company_category WHERE cat_id='$cat' ";
		$result7 = mysql_query($query7)  or die(mysql_error().'<p>'.$query7.'</p>');
		$myrow7 = mysql_fetch_array($result7);
		if ( isset( $myrow7['category']) &&  $myrow7['category']!='' )
		{
		echo $myrow7['category'].", ";
		}
	}
}
?></td>
<td align="center"><?php echo $myrow1['low_price1'];?></td>
<td align="center"><?php echo $myrow1['low_price2'];?></td>
<td align="center"><?php echo $myrow1['low_price3'];?></td>
<td align="center"><?php echo $myrow1['low_price4'];?></td>
<td align="center"><?php echo $myrow1['low_price5'];?></td>
<td width="3%" align="center">
<a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=edit_cover&id=<?php echo $myrow1['ex_id'];?>">
<img src="images/button_properties.png" alt="Edit Extra" width="18" height="13" border="0"></a></td>
<td width="5%" align="center">
<a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=delete&id=<?php echo $myrow1['ex_id'];?>">
<img src="images/button_drop.png" alt="Remove Extra" width="11" height="13" border="0"></a></td>
</tr>
<tr>
<td colspan="11"><?php echo $myrow1['extra_description'];?></td>
</tr>
<tr>
<td colspan="11"><hr color="#993333" size="2"></td>
</tr>
<?php
}
?>
</table>





</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
