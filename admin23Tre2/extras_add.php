<?php
session_start();
include("../includes/connection.php");

$error_msg="";

if (isset($_POST['flag']) && $_POST['flag']=="insert")
{
	if ($_POST['extra_name']=="") 		{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Extra Name can't be empty.</b></font></div><br>"; }
	else if ($_POST['extra_desc']=="") 	{ $error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Please provide a description.</b></font></div><br>"; }
	else 								{ $error_msg="ok"; }
	
	if ($error_msg=="ok") // insert into db
	{
		$query1="SELECT * FROM company_extras WHERE extras_name='".$_POST['extra_name']."' ";
		$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
		$num_results1=mysql_num_rows($result1);
		if ($num_results1==1) // if extra is found in db
		{
			$error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Extra already found in database.Please insert another</b></font></div><br>";
		}
		else // if extra not found, insert it to db
		{
			$sql2 = "INSERT INTO company_extras (ex_type,extras_name,extra_description,type) VALUES ('".$_POST['ex_type']."', '".$_POST['extra_name']."','".$_POST['extra_desc']."','".strtolower($_POST['type'])."')";
			$result2 = mysql_query($sql2)  or die(mysql_error().'<p>'.$sql2.'</p>');
			$extra_id = mysql_insert_id();
			$error_msg="<div align=\"center\"><font color=\"#FF0000\"><b>Extra added.</b></font></div><br>";
		}	
	}
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">&nbsp;</p>

<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="menu_title">Extra Name</span> : give the name of the extra, as it will appear on various tables in the web pages</li>
<li><span class="menu_title">Extra Description</span> : give a short general description (in case a user needs to learn what this extra is)</li>
<li><span class="menu_title">Type</span> : choose between <span class="user">Basic</span> (fixed cost per day) or <span class="user">Coverage</span> (cost per day/category/season)</li>
</ul>
</td>
</tr>
</table>

<?php
echo $error_msg;
?>
<form name="form1" method="post" action="">
<input type="hidden" name="flag" value="insert">
<table width="50%"  border="0" align="center" cellpadding="5" cellspacing="0">
<tr>
<td width="40%">Extra Name </td>
<td width="60%"><input name="extra_name" type="text" id="extra_name" size="30" maxlength="200"></td>
</tr>
<tr>
<td>Category</td>
<td>
Car <input name="ex_type" type="radio" value="C" checked> 
Other <input name="ex_type" type="radio" value="O">
</td>
</tr>
<tr>
<td>Type</td>
<td>
<select name="type">
<option>Basic</option>
<option>Coverage</option>
</select></td>
</tr>
<tr>
<td valign="top">Extra Description </td>
<td><textarea name="extra_desc" cols="28" rows="3" id="extra_desc"></textarea></td>
</tr>
<tr align="center">
<td colspan="2"><input name="Submit" type="submit" class="submit_button" value="Submit"></td>
</tr>
</table>
</form></td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
