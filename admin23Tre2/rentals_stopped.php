<?php
session_start();
include("../includes/connection.php");

$flag = '';
if (isset($_POST) && isset($_POST['flag'])) { $flag = $_POST['flag']; }

if ($flag=="delete")
{
$query100 = "DELETE FROM rental WHERE id='$id' ";
$result100 = mysql_query($query100)  or die(mysql_error().'<p>'.$query100.'</p>');
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function NewWindow(mypage, myname, w, h, scroll) {
var winl = (screen.width - w) / 2;
var wint = (screen.height - h) / 2;
winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable = no'
win = window.open(mypage, myname, winprops)
if (parseInt(navigator.appVersion) >= 4) { win.window.focus(); }
}
</script>
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">&nbsp;</p>
<p align="center" class="font_bold">In this page you will see failed rentals (either by bank or by rental company).<br>
<span class="user">Delete</span> will remove the rental permanently from database - no recovery possible </p>
<p align="center" class="font_bold">Stopped Rentals (Bank failed)</p>


<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr class="font_bold">
<td width="11%">&nbsp;</td>
<td width="11%">Requested</td>
<td width="15%">Company</td>
<td width="15%" align="center">Car cat. </td>
<td width="15%">Duration</td>
<td width="20%">Total Cost </td>
<td width="24%">Bank Status </td>
</tr>
<tr valign="top">
<td colspan="6"><hr></td>
</tr>
<?php
$query1="SELECT * FROM rental WHERE bank_status='Bank stopped' AND session_id NOT LIKE '' ORDER BY id DESC ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($myrow1 = mysql_fetch_array($result1))
{
$session_id = $myrow1['session_id'];
$company_id = $myrow1['company'];
$affiliate_id = $myrow1['affiliate_id'];

	if ($affiliate_id>1)
	{
		$query_assoc="SELECT * FROM affiliates WHERE affiliate_id='$affiliate_id' ";
		$result_assoc = mysql_query($query_assoc)  or die(mysql_error().'<p>'.$query_assoc.'</p>');
		while ($myrow_assoc = mysql_fetch_array($result_assoc))
		{ $username = $myrow_assoc['username']; }
	}
	else { $username = ""; }
	
	$query_company="SELECT * FROM main_company_list WHERE comp_id='$company_id' ";
	$result_company = mysql_query($query_company)  or die(mysql_error().'<p>'.$query_company.'</p>');
	while ($myrow_company = mysql_fetch_array($result_company))
	{
	$company_name = $myrow_company['comp_name_title'];
	}
?>
<tr valign="top">
<td nowrap><a href="view_request.php?session_id=<?php echo $session_id;?>" onClick="NewWindow(this.href,'name','600','500','yes');return false;"><strong>View Voucher <?php echo $myrow1['order_number'];?></strong></a></td>
<td><?php if ($myrow1['date_rent']>0) {echo date("D d M Y", $myrow1['date_rent']);}?></td>
<td><?php echo $company_name;?></td>
<td align="center"><?php echo $myrow1['category'];?></td>
<td><?php echo $myrow1['start_date'];?><br>      <?php echo $myrow1['end_date'];?></td>
<td><?php echo $myrow1['total_cost'];?> &#8364;</td>
<td><?php echo $myrow1['bank_status'];?></td>
</tr>
<tr valign="top">
<td colspan="6"><?php echo $myrow1['full_name'];?>, Driver's age: <?php echo $myrow1['driver_age'];?>, Phone: <?php echo $myrow1['phone'];?>, Mobile: <?php echo $myrow1['mobile'];?>, Email: <?php echo $myrow1['email'];?>
<?php
if ($username!="")
{
?>
&nbsp;&nbsp;&nbsp;<span class="menu_title">from Affiliate: <?php echo $username;?></span>
<?php
}
?>
</td>
</tr>
<tr valign="top" class="font_bold">
<td colspan="6">Rental Status : <?php echo $myrow1['status'];?></td>
</tr>	
<tr valign="top">
<td colspan="6"><a href="rentals_stopped.php?flag=delete&id=<?php echo $myrow1['id'];?>">Delete Rental</a></td>
</tr>
<tr valign="top">
<td colspan="6"><hr></td>
</tr>
<?php
} // end connection 1
?> 
</table> 





<p align="center" class="font_bold">Bank Errors (Error in Transaction)</p>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr class="font_bold">
<td width="11%">&nbsp;</td>
<td width="11%">Requested</td>
<td width="15%">Company</td>
<td width="15%" align="center">Car cat. </td>
<td width="15%">Duration</td>
<td width="20%">Total Cost </td>
<td width="24%">Bank Status </td>
</tr>
<tr valign="top">
<td colspan="6"><hr></td>
</tr>
<?php
$query4="SELECT * FROM rental WHERE bank_status='Error in Transaction' AND session_id NOT LIKE '' ORDER BY id DESC";
$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
while ($myrow4 = mysql_fetch_array($result4))
{
$session_id = $myrow4['session_id'];
$company_id = $myrow4['company'];
$affiliate_id = $myrow4['affiliate_id'];

	if ($affiliate_id>1)
	{
		$query_assoc="SELECT * FROM affiliates WHERE affiliate_id='$affiliate_id' ";
		$result_assoc = mysql_query($query_assoc)  or die(mysql_error().'<p>'.$query_assoc.'</p>');
		while ($myrow_assoc = mysql_fetch_array($result_assoc))
		{ $username = $myrow_assoc['username']; }
	}
	else { $username = ""; }
	
	$query_company="SELECT * FROM main_company_list WHERE comp_id='$company_id' ";
	$result_company = mysql_query($query_company)  or die(mysql_error().'<p>'.$query_company.'</p>');
	while ($myrow_company = mysql_fetch_array($result_company))
	{
	$company_name = $myrow_company['comp_name_title'];
	}
?>
<tr valign="top">
<td nowrap><a href="view_request.php?session_id=<?php echo $session_id;?>" onClick="NewWindow(this.href,'name','600','500','yes');return false;"><strong>View Voucher <?php echo $myrow4['order_number'];?></strong></a></td>
<td><?php if ($myrow4['date_rent']>0) {echo date("D d M Y", $myrow4['date_rent']);}?></td>
<td><?php echo $company_name;?></td>
<td align="center"><?php echo $myrow4['category'];?></td>
<td><?php echo $myrow4['start_date'];?><br>      <?php echo $myrow4['end_date'];?></td>
<td><?php echo $myrow4['total_cost'];?> &#8364;</td>
<td><?php echo $myrow4['bank_status'];?></td>
</tr>
<tr valign="top">
<td colspan="6"><?php echo $myrow4['full_name'];?>, Phone: <?php echo $myrow4['phone'];?>, Mobile: <?php echo $myrow4['mobile'];?>, Email: <?php echo $myrow4['email'];?>
<?php
if ($username!="")
{
?>
&nbsp;&nbsp;&nbsp;<span class="menu_title">from Affiliate: <?php echo $username;?></span>
<?php
}
?>
</td>
</tr>
<tr valign="top" class="font_bold">
<td colspan="6">Rental Status : <?php echo $myrow4['status'];?></td>
</tr>	
<tr valign="top">
<td colspan="6"><a href="rentals_stopped.php?flag=delete&id=<?php echo $myrow4['id'];?>">Delete Rental</a></td>
</tr>
<tr valign="top">
<td colspan="6"><hr></td>
</tr>
<?php
} // end connection 4
?> 
</table> 






<p align="center" class="font_bold">Canceled Rentals (Company Rejected)</p>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr class="font_bold">
<td width="11%">&nbsp;</td>
<td width="11%">Requested</td>
<td width="15%">Company</td>
<td width="15%" align="center">Car cat. </td>
<td width="15%">Duration</td>
<td width="20%">Total Cost </td>
<td width="24%">Bank Status </td>
</tr>
<tr valign="top">
<td colspan="6"><hr></td>
</tr>
<?php
$query3="SELECT * FROM rental WHERE status='Company Reject' AND session_id NOT LIKE '' ORDER BY id DESC ";
$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
while ($myrow3 = mysql_fetch_array($result3))
{
$session_id3 = $myrow3['session_id'];
$company_id = $myrow3['company'];
$affiliate_id = $myrow3['affiliate_id'];

	if ($affiliate_id>1)
	{
		$query_assoc="SELECT * FROM affiliates WHERE affiliate_id='$affiliate_id' ";
		$result_assoc = mysql_query($query_assoc)  or die(mysql_error().'<p>'.$query_assoc.'</p>');
		while ($myrow_assoc = mysql_fetch_array($result_assoc))
		{ $username = $myrow_assoc['username']; }
	}
	else { $username = ""; }
	
	$query_company="SELECT * FROM main_company_list WHERE comp_id='$company_id' ";
	$result_company = mysql_query($query_company)  or die(mysql_error().'<p>'.$query_company.'</p>');
	while ($myrow_company = mysql_fetch_array($result_company))
	{
	$company_name = $myrow_company['comp_name_title'];
	}
?>
<tr valign="top">
<td nowrap><a href="view_request.php?session_id=<?php echo $session_id;?>" onClick="NewWindow(this.href,'name','600','500','yes');return false;"><strong>View Voucher <?php echo $myrow3['order_number'];?></strong></a></td>
<td><?php if ($myrow3['date_rent']>0) {echo date("D d M Y", $myrow3['date_rent']);}?></td>
<td><?php echo $company_name;?></td>
<td align="center"><?php echo $myrow3['category'];?></td>
<td><?php echo $myrow3['start_date'];?><br>      <?php echo $myrow3['end_date'];?></td>
<td><?php echo $myrow3['total_cost'];?> &#8364;</td>
<td><?php echo $myrow3['bank_status'];?></td>
</tr>
<tr valign="top">
<td colspan="6"><?php echo $myrow3['full_name'];?>, Driver's age: <?php echo $myrow3['driver_age'];?>, Phone: <?php echo $myrow3['phone'];?>, Mobile: <?php echo $myrow3['mobile'];?>, Email: <?php echo $myrow3['email'];?>
<?php
if ($username!="")
{
?>
&nbsp;&nbsp;&nbsp;<span class="menu_title">from Affiliate: <?php echo $username;?></span>
<?php
}
?>
</td>
</tr>
<tr valign="top" class="font_bold">
<td colspan="6">Rental Status : <?php echo $myrow3['status'];?></td>
</tr>	
<tr valign="top">
<td colspan="6"><a href="rentals_stopped.php?flag=delete&id=<?php echo $myrow3['id'];?>">Delete Rental</a></td>
</tr>
<tr valign="top">
<td colspan="6"><hr></td>
</tr>
<?php
} // end connection 3
?> 
</table>  






</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
