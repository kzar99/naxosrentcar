<table width="200" border="0" cellpadding="0" cellspacing="0" bgcolor="#999966">
<tr>
<td height="22" colspan="2"><span class="user">Welcome <?php echo $_SESSION['username']; ?></span> -- <a href="logout.php" title="Disconnect from Admin Pages">LOGOUT</a></td>
</tr>
<tr>
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
</table>
<br>
<table width="200" border="0" cellpadding="0" cellspacing="0" bgcolor="#999966">  
<tr>
<td colspan="2"><span class="menu_title">Financial Reports</span></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="rentals_stopped.php">View stopped rentals</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="rentals_unfinished.php">View unfinished rentals</a></td>
</tr>  
<tr>
<td>&nbsp;</td>
<td><a href="rentals_finished.php?year=<?php echo date("Y");?>">View Complete Rentals</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="rentals_closed.php">View Closed Rentals</a></td>
</tr>  
<tr>
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="rentals_other.php">Other</a></td>
</tr> 
<tr>
<td>&nbsp;</td>
<td><a href="rentals_search.php">Search</a></td>
</tr>  
<tr>
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<!--
<tr>
<td colspan="2"><span class="menu_title">Affiliate Reports</span></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="affiliate_reports.php">View</a></td>
</tr>  
<tr>
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
 -->
</table>
<br>
<br>
<table width="200" border="0" cellpadding="0" cellspacing="0" bgcolor="#999966">

<!--
<tr>
<td colspan="2" valign="top"><span class="menu_title">Affiliates</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="affiliates.php">Manage</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>
 -->

<tr>
<td colspan="2" valign="top"><span class="menu_title">Company Data</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="company_view.php">View-Edit</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>

<tr>
<td colspan="2" valign="top"><span class="menu_title">Extras</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="extras_add.php">Add</a></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="extras_edit.php">View-Edit</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>


<tr>
<td colspan="2" valign="top"><span class="menu_title">Pick up - Drop off</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="special_add.php">Add</a></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="special_edit.php">View-Edit</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>


<tr>
<td colspan="2" valign="top"><span class="menu_title">Seasons</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="periods_view.php">View-Edit</a></td>
</tr>
<tr>
<td valign="top">&nbsp;</td>
<td valign="top"><a href="periods_dates.php">Dates setup</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>



<tr>
<td colspan="2" valign="top"><span class="menu_title">Car Categories</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="categories_types.php">Types</a></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="categories_add.php">Add</a></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="categories_view.php">View-Edit</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>


<tr>
<td colspan="2" valign="top"><span class="menu_title">PriceList</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="pricelist_view.php">View-Edit</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>


<tr>
<td colspan="2" valign="top"><span class="menu_title">Stop Sales</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="stopsales_add.php">Add</a></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="stopsales_edit.php">View-Edit</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>


<tr>
<td colspan="2" valign="top"><span class="menu_title">Offers</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="offers_add.php">Add</a></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="offers_view.php">View-Edit</a></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="offers_banner_images.php">Banner Images</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>

<tr>
<td colspan="2" valign="top"><span class="menu_title">Payment Options</span></td>
</tr>
<tr>
<td valign="top" width="40">&nbsp;</td>
<td valign="top" width="160"><a href="payment_bank.php">Bank Deposit</a></td>
</tr>
<tr>
<td colspan="2" valign="top"><hr color="#993333" size="2"></td>
</tr>
</table>

<br>
<br>
<table width="200" border="0" cellpadding="0" cellspacing="0" bgcolor="#999966">  
<tr>
<td colspan="2"><span class="menu_title">Other Services</span></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="offer_percentages.php">Offer Percetages</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="gallery.php">Image Gallery</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="news.php">News</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="hotels.php">Hotels</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="more.php">More Section</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="guestbook.php">Guestbook</a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a href="members.php">Members & Discounts</a></td>
</tr>
<tr>
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
</table>