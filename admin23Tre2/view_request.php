<?php
include("../includes/connection.php");

if ( isset($_GET['session_id'])) { $session_id = $_GET['session_id']; }
else {
	echo 'no rental selected';
	exit();
}

$query0 = "SELECT id FROM rental  WHERE session_id='$session_id' ";
$result0 = mysql_query($query0)  or die(mysql_error().'<p>'.$query0.'</p>');
while ($row0 = mysql_fetch_array($result0))
{
$id = $row0['id'];
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
$extras_show = "";
$query1 = "SELECT * FROM rental  WHERE id='$id' ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($row1 = mysql_fetch_array($result1))
{

$pickup_area = $row1['pickup_area'];
$dropoff_area = $row1['dropoff_area'];
$extras_id = substr($row1['extras'],0,-1);
$discount_code = $row1['discount_code'];

	$query2 = "SELECT * FROM main_special_location  WHERE special_id = $dropoff_area ";
	$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
	while ($row2 = mysql_fetch_array($result2))
	{
	$dropoff_name = $row2['special_name'];
	}

	if ($extras_id!="")
	{
		$query3 = "SELECT * FROM company_extras  WHERE ex_id IN ($extras_id) ORDER BY extras_name ";
		$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
		while ($row3 = mysql_fetch_array($result3))
		{
		$extras_show .= $row3['extras_name'].", ";
		}
	}

	$query4 = "SELECT * FROM main_special_location  WHERE special_id = $pickup_area ";
	$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
	while ($row4 = mysql_fetch_array($result4))
	{
	$pickup_name = $row4['special_name'];
	}
	
	$original_cost = $row1['total_cost'];
	$total_cost = $original_cost;
	
	if ($discount_code!="")
	{
		$query13="SELECT * FROM members WHERE discount_code='$discount_code' LIMIT 1 ";
		$result13 = mysql_query($query13)  or die(mysql_error().'<p>'.$query13.'</p>');
		if (mysql_num_rows($result13)>0)
		{	
		
			$query14="SELECT * FROM member_discount WHERE id=1 ";
			$result14 = mysql_query($query14)  or die(mysql_error().'<p>'.$query14.'</p>');
			while ($myrow14 = mysql_fetch_array($result14))
			{
			$discount_show = $myrow14['discount'];
			}
			
			$discount = (100-$discount_show)/100;
		}
		else
		{
		$discount = 0;
		}
				
		if ($discount>0)
		{
			$total_cost = number_format($total_cost*$discount,2);		
		}		
				
	}	
	
?>
<table width="460" border="0" cellspacing="2" cellpadding="2">
<tr>
<td colspan="2" class="submit_button">Rental Data <?php echo $row1['order_number'];?></td>
</tr>
<tr valign="top">
<td width="114" class="font_bold">Start Date </td>
<td width="332"><?php echo $row1['start_date'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">End Date</td>
<td><?php echo $row1['end_date'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Pickup Area </td>
<td><?php echo $pickup_name;?></td>
</tr>
<tr valign="top">
<td class="font_bold">Dropoff Area </td>
<td><?php echo $dropoff_name;?></td>
</tr>
<tr valign="top">
<td class="font_bold">Car category </td>
<td><?php echo $row1['category'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Extras</td>
<td><?php echo $extras_show;?></td>
</tr>
<tr valign="top">
<td class="font_bold">Total Cost </td>
<td><?php echo $total_cost;?> <?php if ($original_cost!=$total_cost) {?>(<?php echo $discount_show;?>% discount)<?php } ?></td>
</tr>
<tr valign="top">
<td class="font_bold">Pre-payment</td>
<td><?php echo $row1['pre_pay'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr valign="top">
<td colspan="2" class="submit_button">Status</td>
</tr>
<tr valign="top">
<td class="font_bold">Bank Status</td>
<td><?php echo $row1['bank_status'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Status</td>
<td><?php echo $row1['status'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr valign="top">
<td colspan="2" class="submit_button">Customer Data </td>
</tr>
<tr valign="top">
<td class="font_bold">Full Name </td>
<td><?php echo $row1['full_name'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Driver's Age</td>
<td><?php echo $row1['driver_age'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Phone</td>
<td><?php echo $row1['phone'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Mobile</td>
<td><?php echo $row1['mobile'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">E-mail</td>
<td><?php echo $row1['email'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Hotel Name </td>
<td><?php echo $row1['hotel_name'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Flight Number </td>
<td><?php echo $row1['flight_number'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Ship Name </td>
<td><?php echo $row1['ship_name'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Pickup</td>
<td><?php echo $row1['pickup'];?></td>
</tr>
<tr valign="top">
<td class="font_bold">Comments</td>
<td><?php echo nl2br($row1['sxolia']);?></td>
</tr>
</table>
<?php
}
?>
</body>
</html>
