<?php
session_start();
include("../includes/connection.php");

///////////////
// if user insert dates
///////////////
if (isset($_POST['flag']) && $_POST['flag']=="update")
{

	// start1 date
	if ($_POST['start1']!="")
	{
	$a1 = explode("-",$_POST['start1']);
	$mya1 = mktime (0, 0, 0, $a1[1], $a1[0], $a1[2]);
	}
	else
	{
	$mya1 = 0;
	}
	
	
	// end1 date
	if ($_POST['end1']!="")
	{	
	$b1 = explode("-",$_POST['end1']);
	$myb1 = mktime (0, 0, 0, $b1[1], $b1[0], $b1[2]);
	}
	else
	{
	$myb1 = 0;
	}
	
	// start2 date
	if ($_POST['start2']!="")
	{
	$c1 = explode("-",$_POST['start2']);
	$myc1 = mktime (0, 0, 0, $c1[1], $c1[0], $c1[2]);
	}
	else
	{
	$myc1 = 0;
	}
	
	// end2 date
	if ($_POST['end2']!="")
	{
	$d1 = explode("-",$_POST['end2']);
	$myd1 = mktime (0, 0, 0, $d1[1], $d1[0], $d1[2]);
	}
	else
	{
	$myd1 = 0;
	}
$sql5 = "UPDATE company_periods SET start_date1='$mya1',end_date1='$myb1',start_date2='$myc1',end_date2='$myd1' 
 WHERE period_id='".$_POST['id']."' ";
$result15 = mysql_query($sql5)  or die(mysql_error().'<p>'.$sql5.'</p>');	
	
}


?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Edit the company's seasons/periods</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<ul>
<li><span class="font_bold">Season</span> : the name of the season/period. The names are given automatically and cannot be changed. 
They surve no purpose at all but to classify the price periods of the company. For example, the DEMO Company has 4 price seasons - 
low, medium, high and winter. But you will see low, medium, high and peak. The name of the season is irrelevant, only the dates matter. </li>
<li><span class="font_bold">Period1, Period2</span> : You will see in each period the fields From and Until. These fields mark the 
start and end of the period. In out DEMO example, the Low Season is in 01/04 - 15/05 and in 01/10 - 31/10. So in Period 1 we write 
From=01-04-2005 Until=15-05-2005. In Period2 we write From=01-10-2005 and Until=31-10-2005. For easy reference use the calendar icon 
to input dates. All dates are given as DD-MM-YYYY (YYYY=4 digits year, MM=2 digits month, DD=2 digits day) </li>
</ul>
</td>
</tr>
</table>


<?php
if (isset($_GET['flag']) && $_GET['flag']=="edit")
{
	//////////////////////////////////
	/////////// main form creation
	/////////////////////////////////
	$query2="SELECT * FROM company_periods WHERE period_id='".$_GET['id']."' ";
	$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
	while ($myrow2 = mysql_fetch_array($result2))
	{
	$period_name = $myrow2['period_name'];
	$start_date11 = $myrow2['start_date1'];
	$start_date21 = $myrow2['start_date2'];
	$end_date11 = $myrow2['end_date1'];
	$end_date21 = $myrow2['end_date2'];
	}
	// start date 1
	if ($start_date11<1) { $mya=""; } else { $mya = date("d-m-Y", $start_date11); }
	// end date 1
	if ($end_date11<1)	{ $myb=""; } else { $myb = date("d-m-Y", $end_date11); }
	
	
	// start date 2
	if ($start_date21<1) { $myc=""; } else { $myc = date("d-m-Y", $start_date21); }	
	// end date 2
	if ($end_date21<1)  { $myd=""; } else { $myd = date("d-m-Y", $end_date21); }
?>
<form name="tstest" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
<input name="flag" type="hidden" value="update">
<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>">
<table width="80%"  border="0" align="center" cellpadding="0" cellspacing="0">
<tr class="font_bold">
<td>&nbsp;</td>
<td colspan="2" align="center">Period1</td>
<td colspan="2" align="center">Period2</td>
</tr>
<tr class="font_bold">
<td>Season/Period</td>
<td>From</td>
<td>Until</td>
<td>From</td>
<td>Until</td>
</tr>
<tr>
<td><?php echo $period_name;?></td>
<td>
<input name="start1" type="Text" id="start1" value="<?php echo $mya; ?>" size="12">
<a href="javascript:cal1.popup();">
<img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>			
<td>
<td>
<input name="end1" type="Text" id="end1" value="<?php echo $myb; ?>" size="12">
<a href="javascript:cal2.popup();">
<img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>				
</td>
<td>
<input name="start2" type="Text" id="start2" value="<?php echo $myc; ?>" size="12">
<a href="javascript:cal3.popup();">
<img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>				
</td>
<td>
<input name="end2" type="Text" id="end2" value="<?php echo $myd; ?>" size="12">
<a href="javascript:cal4.popup();">
<img src="images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>				
</td>
</tr>
</table>
<p align="center">
<input name="Submit" type="submit" class="submit_button" value="Update Dates">
</p>
</form>
<script language="JavaScript">
<!-- 
var cal1 = new calendar1(document.forms['tstest'].elements['start1']);
cal1.year_scroll = true;
cal1.time_comp = false;
var cal2 = new calendar1(document.forms['tstest'].elements['end1']);
cal2.year_scroll = true;
cal2.time_comp = false;
var cal3 = new calendar1(document.forms['tstest'].elements['start2']);
cal3.year_scroll = true;
cal3.time_comp = false;
var cal4 = new calendar1(document.forms['tstest'].elements['end2']);
cal4.year_scroll = true;
cal4.time_comp = false;
//-->
</script>		
<?php
} // end if edit
?>



<table width="60%"  border="0" align="center" cellpadding="0" cellspacing="0">
<tr class="font_bold">
<td width="17%">Season</td>
<td colspan="2">Period1</td>
<td colspan="2">Period2</td>
<td width="15%" align="center">Actions</td>
</tr>
<tr>
<td colspan="6"><hr color="#993333" size="2"></td>
</tr>
<?php
$query1="SELECT * FROM company_periods ORDER BY period_id ASC ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($myrow1 = mysql_fetch_array($result1))
{

	if ($myrow1['start_date1']==0)	{ $start_date1=""; }
	else { $start_date1 = date("d-m-Y", $myrow1['start_date1']); }
	
	if ($myrow1['start_date2']==0)	{ $start_date2=""; }
	else { $start_date2 = date("d-m-Y", $myrow1['start_date2']); }
	
	if ($myrow1['end_date1']==0) { $end_date1=""; }
	else { $end_date1 = date("d-m-Y", $myrow1['end_date1']); }
	
	if ($myrow1['end_date2']==0){ $end_date2=""; }
	else { $end_date2 = date("d-m-Y", $myrow1['end_date2']); }
?>
<tr>
<td rowspan="2" valign="middle" class="font_bold"><?php echo $myrow1['period_name'];?></td>
<td width="9%" class="font_bold">From :</td>
<td width="22%"><?php echo $start_date1;?></td>
<td width="9%" class="font_bold">From :</td>
<td width="28%"><?php echo $start_date2;?></td>
<td rowspan="2" align="center" valign="middle">
<a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=edit&id=<?php echo $myrow1['period_id'];?>&name=<?php echo $myrow1['period_name'];?>">
<img src="images/button_properties.png" alt="Edit dates" width="18" height="13" border="0"></a></td>
</tr>
<tr>
<td class="font_bold">Until :</td>
<td><?php echo $end_date1;?></td>
<td class="font_bold">Until :</td>
<td><?php echo $end_date2;?></td>
</tr>
<tr>
<td colspan="6"><hr color="#993333" size="2"></td>
</tr>
<?php
}
?>
</table>



</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
