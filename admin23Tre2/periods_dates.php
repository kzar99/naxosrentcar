<?php
session_start();
include("../includes/connection.php");

if (isset($_GET['flag']) && $_GET['flag']=="update")
{
	$query3 = "TRUNCATE TABLE `company_date_options`";
	$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');

	$query2="SELECT period_name FROM company_periods ORDER BY period_id ASC ";
	$result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
	while ($row2 = mysql_fetch_array($result2))
	{
	$period_name = $row2['period_name'];
	
		$date_from_today_test = 'date_from_today_'.$period_name;
		$min_dur_test = 'min_dur_'.$period_name;
		$min_book_dur_test = 'min_book_dur_'.$period_name;
		
		$date_from_today = $_POST[$date_from_today_test];
		$min_dur = $_POST[$min_dur_test];
		$min_book_dur = $_POST[$min_book_dur_test];
	
	
		$query4 = "INSERT INTO company_date_options (period,date_from_today,min_dur,min_book_dur) VALUES ('$period_name','$date_from_today','$min_dur','$min_book_dur') ";
		$result4 = mysql_query($query4)  or die(mysql_error().'<p>'.$query4.'</p>');
		
		//echo $query4.'<br>';
	}
}


?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr align="center" valign="top">
<td colspan="2"><?php include("_head.php"); ?></td>
</tr>
<tr valign="top">
<td colspan="2"><hr color="#993333" size="2"></td>
</tr>
<tr valign="top">
<td width="200"><?php include("menu_left.php"); ?></td>
<td align="center">
<p class="font_bold">Edit the company's seasons/periods</p>
<table width="90%"  border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
The following date options affect a request.<br>
<br>
<strong>Days from today:</strong> defines the minimum number of days, starting of today, the customer is allowed to make a request.<br>
<span class="user">For example</span>, if today is <strong>May 13th (in low season)</strong>, and you have defined the <strong>"Days from today" number to be 3</strong>, the customer is entitled to a request, starting of <strong>May 16th afterwards</strong>.<br>
Also, when a customer first visits the booking form, this number will indicate the first pre-selected date (the pickup date).<br>
<u>Of course, the customer can select any date he/she wants, but dates earlier than the predefined date will be rejected</u>.<br>
<br>
<strong>Minimum Duration:</strong> defines the first, pre-selected duration of the booking requet.<br>
<span class="user">For example</span> if the <strong>pikcup date is May 16th (in low season)</strong>, and you set the <strong>"Minimum Duration" number to be 4</strong>, the dropoff date will be <strong>pre-defined as May 20th</strong>.<br>
<u>Of course, the customer can select any date he/she wants</u>.<br>
<br>
<br>
<strong>Minimum Booking Duration:</strong> defines the minimum duration of the booking request.<br>
<u>The customer won't be able to book a vehicle for a dulation lower than this number.</u>.<br>
Duration of 0 means the customer is free to book any duration in days.<br>
<br>
</td>
</tr>
</table>


<p>&nbsp;</p>


<form method="post" action="?flag=update" name="form1" id="form1">
<table border="0" cellspacing="5" cellpadding="2">
<tr>
<td class="font_bold" style="border-bottom:1px solid #ffffff;">Period</td>
<td class="font_bold" style="border-bottom:1px solid #ffffff;">&nbsp;</td>
<td class="font_bold" style="border-bottom:1px solid #ffffff;">Days from today</td>
<td class="font_bold" style="border-bottom:1px solid #ffffff;">Minimum Duration</td>
<td class="font_bold" style="border-bottom:1px solid #ffffff;">Minimum Booking Duration</td>
</tr>
<?php
$query1="SELECT * FROM company_periods ORDER BY period_id ASC ";
$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
while ($myrow1 = mysql_fetch_array($result1))
{
	if ($myrow1['start_date1']<1) { $start_date1=""; } else { $start_date1 = date("d/m/Y", $myrow1['start_date1']); }
	if ($myrow1['start_date2']<1) { $start_date2=""; } else { $start_date2 = date("d/m/Y", $myrow1['start_date2']); }
	if ($myrow1['end_date1']<1) { $end_date1=""; } else { $end_date1 = date("d/m/Y", $myrow1['end_date1']); }
	if ($myrow1['end_date2']<1){ $end_date2=""; } else { $end_date2 = date("d/m/Y", $myrow1['end_date2']); }
	
	if ($start_date1!="" && $end_date1!="") {$show_date1 = $start_date1." - ".$end_date1;} else {$show_date1 = "";}
	if ($start_date2!="" && $end_date2!="") {$show_date2 = $start_date2." - ".$end_date2;} else {$show_date2 = "";}

	if ($show_date1!="" || $show_date2!="")
	{
	
		$query5="SELECT * FROM company_date_options WHERE period='".$myrow1['period_name']."' ";
		$result5 = mysql_query($query5)  or die(mysql_error().'<p>'.$query5.'</p>');
		while ($myrow5 = mysql_fetch_array($result5))
		{	
		$date_from_today = $myrow5['date_from_today'];
		$min_dur = $myrow5['min_dur'];
		$min_book_dur = $myrow5['min_book_dur'];
		}
?>
<tr>
<td align="left" valign="middle" class="font_bold" style="border-bottom:1px solid #ffffff;"><?php echo $myrow1['period_name'];?></td>
<td align="left" valign="middle" nowrap style="border-bottom:1px solid #ffffff;">
<?php
if ($show_date1!="") {echo $show_date1."<br>";}
if ($show_date2!="") {echo $show_date2."<br>";}
?></td>
<td align="left" valign="middle" style="border-bottom:1px solid #ffffff;"><input name="date_from_today_<?php echo $myrow1['period_name'];?>" type="text" size="4" value="<?php echo $date_from_today;?>"></td>
<td align="left" valign="middle" style="border-bottom:1px solid #ffffff;"><input name="min_dur_<?php echo $myrow1['period_name'];?>" type="text" size="4" value="<?php echo $min_dur;?>"></td>
<td align="left" valign="middle" style="border-bottom:1px solid #ffffff;"><input name="min_book_dur_<?php echo $myrow1['period_name'];?>" type="text" size="4" value="<?php echo $min_book_dur;?>"></td>
</tr>
<?php
	}
}
?>
</table>
<div align="center"><input name="submit1" type="submit" class="submit_button" value="Define Numbers">
</div>
</form>


</td>
</tr>
<tr align="center" valign="top">
<td colspan="2"><?php include("elements_bottom.php"); ?></td>
</tr>
</table>
</body>
</html>
