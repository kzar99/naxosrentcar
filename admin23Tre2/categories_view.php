<?php
session_start();
include("../includes/connection.php");

$flag = '';
if (isset($_GET) && isset($_GET['flag'])) { $flag = $_GET['flag']; }
else if (isset($_POST) && isset($_POST['flag'])) { $flag = $_POST['flag']; }

$id = '';
if (isset($_GET) && isset($_GET['id'])) { $id = $_GET['id']; }

$car_type = '';
if (isset($_GET['car_type'])) { $car_type = $_GET['car_type']; }

if ($car_type==""){$car_type="car";}

//////////////
/// delete ///
//////////////
if (isset($_GET['flag']) && $_GET['flag']=="delete")
{
	$sql4 = "DELETE FROM company_category WHERE cat_id='".$_GET['id']."' ";
	$result4 = mysql_query($sql4)  or die(mysql_error().'<p>'.$sql4.'</p>');	
	
	$sql5 = "DELETE FROM company_car_list_low WHERE cat_id='".$_GET['id']."' ";
	$result5 = mysql_query($sql5)  or die(mysql_error().'<p>'.$sql5.'</p>');	
	
	$sql6 = "DELETE FROM company_car_list_medium WHERE cat_id='".$_GET['id']."' ";
	$result6 = mysql_query($sql6)  or die(mysql_error().'<p>'.$sql6.'</p>');	
	
	$sql7 = "DELETE FROM company_car_list_high WHERE cat_id='".$_GET['id']."' ";
	$result7 = mysql_query($sql7)  or die(mysql_error().'<p>'.$sql7.'</p>');		
	
	$sql8 = "DELETE FROM company_car_list_peak WHERE cat_id='".$_GET['id']."' ";
	$result8 = mysql_query($sql8)  or die(mysql_error().'<p>'.$sql8.'</p>');		
		
	$sql9 = "DELETE FROM company_car_list_other WHERE cat_id='".$_GET['id']."' ";
	$result9 = mysql_query($sql9)  or die(mysql_error().'<p>'.$sql9.'</p>');	
	
	$sql10 = "DELETE FROM stop_sales WHERE car_id='".$_GET['id']."' ";
	$result10 = mysql_query($sql10)  or die(mysql_error().'<p>'.$sql10.'</p>');		
	
	
	mysql_query("REPAIR TABLE company_category");
	mysql_query("REPAIR TABLE company_car_list_low");
	mysql_query("REPAIR TABLE company_car_list_medium");
	mysql_query("REPAIR TABLE company_car_list_high");
	mysql_query("REPAIR TABLE company_car_list_peak");
	mysql_query("REPAIR TABLE company_car_list_other");
	mysql_query("REPAIR TABLE stop_sales");
}



/////////////////
/// update /////
////////////////
if ($flag=="update")
{
$sql4 = "UPDATE company_category SET 
	category = '".mysql_real_escape_string($_POST['category'])."', 
	description = '".mysql_real_escape_string($_POST['description'])."', 
	cat_cars = '".mysql_real_escape_string($_POST['cars'])."', 
	car_type = '".mysql_real_escape_string($_POST['car_type'])."', 
	adults = '".mysql_real_escape_string($_POST['adults'])."', 
	children = '".mysql_real_escape_string($_POST['children'])."', 
	large_bag = '".mysql_real_escape_string($_POST['large_bag'])."', 
	small_bag = '".mysql_real_escape_string($_POST['small_bag'])."', 
	has_ac = '".mysql_real_escape_string($_POST['has_ac'])."', 
	has_music = '".mysql_real_escape_string($_POST['has_music'])."', 	
	mileage = '".mysql_real_escape_string($_POST['mileage'])."', 	
	min_age = '".mysql_real_escape_string($_POST['min_age'])."', 	
	transmission = '".mysql_real_escape_string($_POST['transmission'])."', 	
	doors = '".mysql_real_escape_string($_POST['doors'])."' 
WHERE cat_id='".mysql_real_escape_string($_POST['id'])."' ";
$result14 = mysql_query($sql4)  or die(mysql_error().'<p>'.$sql4.'</p>');
}
?>
<html>
<head>
<title>Welcome to Administrator Pages</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1253">
<?php include("elements_top.php"); ?>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr align="center" valign="top">
    <td colspan="2"><?php include("_head.php"); ?></td>
  </tr>
  <tr valign="top">
    <td colspan="2"><hr color="#993333" size="2"></td>
  </tr>
  <tr valign="top">
    <td width="200"><?php include("menu_left.php"); ?></td>
    <td align="center"><p class="font_bold">View-Edit Car Categories</p>
        <table width="90%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><ul>
                <li><span class="font_bold">Category</span> : write the category's name, for example A, A1,A2,B, ...</li>
              <li><span class="font_bold">Description</span> : Write a short descriptin if you like </li>
              <li><span class="font_bold">Cars</span> : write the types of cars that are included in category, for example, Fiat seicento1.1, ... </li>
              <li><span class="font_bold">Photo/Image</span> : only jpg 270x180 and up to 100kb. If photo exists, click on it to delete it </li>
            </ul></td>
          </tr>
        </table>

<?php
if(isset($_POST['flag2']) && $_POST['flag2']=="upload_photo")
{
	/*
	Array
	(
		[flag2] => upload_photo
		[cat_id] => 130
		[Upload_Photo] => GO
	)
	Array
	(
		[car_file] => Array
			(
				[name] => test.jpg
				[type] => image/jpeg
				[tmp_name] => /var/tmpfs/phpxrXGON
				[error] => 0
				[size] => 6000
			)

	)
	*/
	$error_message = "";

	$cat_id 		= ( isset($_POST['cat_id']) 		? $_POST['cat_id'] 	: '0'  );

	$a = explode(".", $_FILES['car_file']['name']);
	$new_name = $a[0].'.jpg';

	// https://www.verot.net/php_class_upload.htm
	$foo = new \Verot\Upload\Upload($_FILES['car_file']); 

	$foo->file_new_name_body 	= $a[0];
	$foo->file_new_name_ext 	= 'jpg';
	$foo->file_safe_name = true; // formats the filename (spaces changed to _)
	$foo->file_force_extension = true; // forces an extension if there is't any
	$foo->file_overwrite = true; // sets behaviour if file already exists
	$foo->auto_create_dir = true; // automatically creates destination directory if missing
	$foo->dir_auto_chmod = true; // automatically attempts to chmod the destination directory if not writeable
	$foo->dir_chmod = 0777; // chmod used when creating directory or if directory not writeable
	$foo->jpeg_quality = 100;
	$foo->image_convert = 'jpg'; // if set, image will be converted (possible values : ''|'png'|'jpeg'|'gif'|'bmp'; default: '')
	$foo->allowed = array('image/*'); // array of allowed mime-types (or one string). wildcard accepted, as in image/* (default: check Init)
	$foo->mime_check = true; // sets if the class check the MIME against the allowed list
	$foo->image_resize = true;
	$foo->image_x = 270;
	$foo->image_y = 180;

	$foo->Process("../car_images/");
	if ($foo->processed) 
	{
		$sql4 = "UPDATE company_category SET image='".$new_name."' WHERE cat_id='$cat_id' ";
		$result14 = mysql_query($sql4)  or die(mysql_error().'<p>'.$sql4.'</p>');		
	} 
	else 
	{
		$foo->Clean(); // clean up before showing error
	
		$error_message .= $image_name.': '.$foo->error;
	}


	if ($error_message!="") {echo "<font color=\"#FF0000\"><strong>".$error_message."</strong></font>";}
} // end of flag=upload_photo





if ($flag=="delete_image")
{
$dir5 = "../car_images";
unlink($dir5."/".$_GET['image_name']);

$sql4 = "UPDATE company_category SET image='' WHERE cat_id='".$_GET['cat_id']."' ";
$result14 = mysql_query($sql4)  or die(mysql_error().'<p>'.$sql4.'</p>');
$flag="";
}



//////////////
/// edit /////
//////////////
if ($flag=="edit")
{

	$query3="SELECT * FROM company_category WHERE cat_id='$id' ";
	$result3 = mysql_query($query3)  or die(mysql_error().'<p>'.$query3.'</p>');
	while ($myrow3 = mysql_fetch_array($result3))
	{
?>
        <form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>?car_type=<?php echo $car_type;?>">
          <input name="flag" type="hidden" id="flag" value="update">
          <input name="id" type="hidden" id="flag" value="<?php echo $myrow3['cat_id'];?>">
			<table width="50%"  border="0" align="center" cellpadding="3" cellspacing="0">
			<tr valign="top">
			<td>Type</td>
			<td>
			<select name="car_type">
			<?php
			$query1 = "SELECT * FROM vehicles ORDER BY veh_order ";
			$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
			while ($row1 = mysql_fetch_array($result1))
			{
			?>
			<option value="<?php echo stripslashes($row1['veh_code']);?>" <?php if ($myrow3['car_type']==$row1['veh_code']){echo " selected";}?>><?php echo stripslashes($row1['veh_name']);?></option>
			<?php
			}
			?>
		
			</select>			

			</td>
			</tr>
			
			<tr valign="top">
			<td width="50%">Category</td>
			<td width="50%"><input name="category" type="text" id="category" size="10" maxlength="50" value="<?php echo $myrow3['category'];?>"></td>
			</tr>
			
			<tr valign="top">
			<td>Description</td>
			<td><textarea name="description" rows="3" id="description"><?php echo $myrow3['description'];?></textarea></td>
			</tr>
			
			<tr valign="top">
			<td>Cars</td>
			<td><textarea name="cars" rows="2" id="cars"><?php echo $myrow3['cat_cars'];?></textarea></td>
			</tr>
			
			<tr>
			<td>Number of Adults </td>
			<td><select name="adults">
			<?php
			for ($j=1;$j<=10;$j++)
			{
			?>
			<option value="<?php echo $j;?>" <?php if ($myrow3['adults']==$j) { echo " selected";}?>><?php echo $j;?></option>
			<?php
			}
			?>
			</select>
			</td>
			</tr>
			
			<tr>
			<td>Number of Children</td>
			<td><select name="children">
			<?php
			for ($k=0;$k<=5;$k++)
			{
			?>
			<option value="<?php echo $k;?>" <?php if ($myrow3['children']==$k) { echo " selected";}?>><?php echo $k;?></option>
			<?php
			}
			?>
			</select>
			</td>
			</tr>
			
			<tr>
			<td>Number of large baggages</td>
			<td><select name="large_bag">
			<?php
			for ($l=0;$l<=10;$l++)
			{
			?>
			<option value="<?php echo $l;?>"  <?php if ($myrow3['large_bag']==$l) { echo " selected";}?>><?php echo $l;?></option>
			<?php
			}
			?>
			</select
			></td>
			</tr>
			
			<tr>
			<td>Number of small baggages</td>
			<td><select name="small_bag">
			<?php
			for ($m=0;$m<=10;$m++)
			{
			?>
			<option value="<?php echo $m;?>" <?php if ($myrow3['small_bag']==$m) { echo " selected";}?>><?php echo $m;?></option>
			<?php
			}
			?>
			</select>
			</td>
			</tr>
			
			<tr>
			<td>Has A/C</td>
			<td> Yes
			<input name="has_ac" type="radio" value="Y" <?php if ($myrow3['has_ac']=="Y") { echo " checked";}?>>
			No
			<input name="has_ac" type="radio" value="N" <?php if ($myrow3['has_ac']=="N") { echo " checked";}?>>
			</td>
			</tr>
			
			<tr>
			<td>Has Music</td>
			<td> Yes
			<input name="has_music" type="radio" value="Y" <?php if ($myrow3['has_music']=="Y") { echo " checked";}?>>
			No
			<input name="has_music" type="radio" value="N" <?php if ($myrow3['has_music']=="N") { echo " checked";}?>>
			</td>
			</tr>
			
			<tr>
			<td>Mileage</td>
			<td><input name="mileage" type="text" value="<?php echo stripslashes($myrow3['mileage']);?>"></td>
			</tr>
			
			<tr>
			<td>Minimum Age</td>
			<td><select name="min_age">
			<option value="0" <?php if ($myrow3['min_age']==0) { echo " selected";}?>>Select...</option>
			<?php
			for ($a=18;$a<=60;$a++)
			{
			?>
			<option value="<?php echo $a;?>" <?php if ($myrow3['min_age']==$a) { echo " selected";}?>><?php echo $a;?></option>
			<?php
			}
			?>
			</select>
			</td>
			</tr>
			
			<tr>
			<td>Transmission </td>
			<td><select name="transmission">
			<option value="" <?php if ($myrow3['transmission']=='') { echo " selected";}?>>Select...</option>
			<option value="manual" <?php if ($myrow3['transmission']=='manual') { echo " selected";}?>>Manual</option>
			<option value="automatic" <?php if ($myrow3['transmission']=='automatic') { echo " selected";}?>>Automatic</option>
			</select>
			</td>
			</tr>
			
			<tr>
			<td>Number of doors</td>
			<td><select name="doors">
			<option value="0" <?php if ($myrow3['doors']==0) { echo " selected";}?>>Select...</option>
			<?php
			for ($d=0;$d<=10;$d++)
			{
			?>
			<option value="<?php echo $d;?>" <?php if ($myrow3['doors']==$d) { echo " selected";}?>><?php echo $d;?></option>
			<?php
			}
			?>
			</select>
			</td>
			</tr>

			<tr align="center" valign="top">
			<td colspan="2"><input name="Submit" type="submit" class="submit_button" value="Update Category"></td>
			</tr>			
			</table>
        </form>


<?php
	} // close creation of form
}
?>
<div align="center"> <br>
  Filer results by vehicle type (only those with unique codes):<br> 

	<?php
	$type_array = array();
	$query1 = "SELECT * FROM vehicles WHERE veh_code<>'' ORDER BY veh_order ";
	$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
	while ($row1 = mysql_fetch_array($result1))
	{
		$type_array[] = '"'.stripslashes($row1['veh_code']).'"';
	?>
	<a href="?car_type=<?php echo stripslashes($row1['veh_code']);?>"><?php echo stripslashes($row1['veh_name']);?></a> | 
	<?php
	}
	?>  
	<a href="?car_type=error">[Unspecified]</a>
  <br>
  <br>
</div>
<table width="80%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr class="font_bold">
    <td width="15%">Category</td>
    <td width="10%">Type</td>
    <td align="center">Image/Photo</td>
    <td colspan="2" align="center">Actions</td>
  </tr>
  <tr>
    <td colspan="5"><hr color="#993333" size="2"></td>
  </tr>
<?php
if ($car_type=='error')
{
	$query1="SELECT * FROM company_category WHERE car_type NOT IN (".implode(",", $type_array).") ORDER BY car_type,category ASC ";
}
else
{
	$query1="SELECT * FROM company_category WHERE car_type='$car_type' ORDER BY car_type,category ASC ";
}

$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
$num_results1=mysql_num_rows($result1);
while ($myrow1 = mysql_fetch_array($result1))
{
?>
  <tr valign="top">
    <td><?php echo $myrow1['category'];?></td>
    <td><?php echo $myrow1['car_type'];?></td>
    <td width="35%" align="center"><?php
	if ($myrow1['image']=="")
	{
	?>
        <form action="<?php echo $_SERVER['PHP_SELF'];?>?car_type=<?php echo $car_type;?>" method="post" enctype="multipart/form-data" name="photo_upload_form">
          <input name="flag2" type="hidden" value="upload_photo">
          <input name="cat_id" type="hidden" value="<?php echo $myrow1['cat_id'];?>">
          <input name="car_file" type="file" id="car_file" size="15">
          <input type="submit" name="Upload Photo" value="GO">
        </form>
      <?php
	}
	else
	{
	?>
        <a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=delete_image&cat_id=<?php echo $myrow1['cat_id'];?>&image_name=<?php echo $myrow1['image'];?>&car_type=<?php echo $car_type;?>" onClick="return confirm('You are about to delete this photo. Are you sure you wish to delete this?');"> <img src="../car_images/<?php echo $myrow1['image'];?>" border="0"></a>
        <?php
	}
	?></td>
    <td width="14%" align="center"><a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=edit&id=<?php echo $myrow1['cat_id'];?>&car_type=<?php echo $car_type;?>"> <img src="images/button_properties.png" width="18" height="13" border="0" alt="Edit this Category"></a></td>
    <td width="12%" align="center"><a href="<?php echo $_SERVER['PHP_SELF'];?>?flag=delete&id=<?php echo $myrow1['cat_id'];?>&car_type=<?php echo $car_type;?>" border="0" alt="Delete this Category"> <img src="images/button_drop.png" width="11" height="13" border="0" alt="Delete Category"></a></td>
  </tr>
  <tr valign="top">
    <td colspan="5"><span class="font_bold">Type of Cars :</span> <?php echo stripslashes($myrow1['cat_cars']);?></td>
  </tr>
  <tr valign="top">
    <td colspan="5"><span class="font_bold">Description :</span> <?php echo stripslashes($myrow1['description']);?></td>
  </tr>
  <tr valign="top">
    <td colspan="5">
	<?php if ($myrow1['adults']>0) {?>		<img src="images/passagerare.gif" border="0" align="absmiddle"> x <?php echo $myrow1['adults'];?>, 		<?php } ?>
	<?php if ($myrow1['children']>0) {?>	<img src="images/barn.gif" border="0" align="absmiddle"> x <?php echo $myrow1['children'];?>, 			<?php } ?>
	<?php if ($myrow1['large_bag']>0) {?>	<img src="images/bagage.gif" border="0" align="absmiddle"> x <?php echo $myrow1['large_bag'];?>, 		<?php } ?>
	<?php if ($myrow1['small_bag']>0) {?>	<img src="images/minibagage.gif" border="0" align="absmiddle"> x <?php echo $myrow1['small_bag'];?>, 	<?php } ?>
	<?php if ($myrow1['has_music']=='Y') {?><img src="images/music.gif" border="0" align="absmiddle">, 												<?php } ?>
	<?php if ($myrow1['has_ac']=='Y') {?>	<img src="images/ac.gif" border="0" align="absmiddle">, 												<?php } ?>
	
	<?php if ($myrow1['mileage']!='') {?>	Mileage:<?php echo $myrow1['mileage'];?> , 																<?php } ?>
	<?php if ($myrow1['min_age']>0) {?>		Minimum Age:<?php echo $myrow1['min_age'];?>, 															<?php } ?>
	<?php if ($myrow1['transmission']!='') {?>	Transmission:<?php echo $myrow1['transmission'];?> , 												<?php } ?>
	<?php if ($myrow1['doors']>0) {?>		Door:<?php echo $myrow1['doors'];?> , 																	<?php } ?>



    </td>
  </tr>
  <tr>
    <td colspan="5"><hr color="#993333" size="2"></td>
  </tr>
  <?php
}
?>
</table>
</body>
</html>
