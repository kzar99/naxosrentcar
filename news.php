<?php
include("includes/connection.php"); 
include("includes/func.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>
    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>

    <!--content section-->
	<section class="contentin">
        <div class="container plain">


            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>News</h1>
						<?php
                        $query0 = "SELECT * FROM news ORDER BY date_insert DESC ";
                        $result0 = mysql_query($query0)  or die(mysql_error().'<p>'.$query0.'</p>');
                        $news_found = mysql_num_rows($result0);
                        if ($news_found>0)
                        {
                        $main_dir = "admin23Tre2/news_images";
                        
                            while ($myrow0 = mysql_fetch_array($result0))
                            {
                        ?>
						<?php
                        if ($myrow0['image_name']!="")
                        {
                        ?>
                        <ul class="news-list">
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <h4><?php echo stripslashes($myrow0['news_title']);?></h4>
                            <em>Written on <?php echo date("d-m-Y", $myrow0['date_insert']);?></em>
                            <br /><br />
                            <div class="row">
                                <div class="col-md-3 col-lg-2 col-sm-3 col-xs-12"><?php echo "<img src=\"".$main_dir."/".$myrow0['image_name']."\" border=\"0\" align=\"left\" style=\"margin-right:10px;\">"; ?></div>
                                <div class="col-md-9 col-lg-10 col-sm-9 col-xs-12 text"><p><?php echo stripslashes(nl2br($myrow0['news_text'])); ?></p></div>
                            </div>
                            <hr/>
                        </li>
                        </ul> 
                        <?php } else {?>
                        <p>No news found </p> 
                        <?php }?>            
                        <?php
						}
						}
						?>
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 

<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				