<?php
include("includes/connection.php"); 
include("includes/func.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>
    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>

    <!--content section-->
	<section class="contentin">
        <div class="container plain">


            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Hotels</h1>
                    <form action="hotels.htm" method="post" class="carlist-form">
                    <div class="col-lg-6 col-md-6 col-sm-6 txt"><strong>Filter hotels by location:</strong></div>
					<div class="col-lg-6 col-md-6 col-sm-6">
                    <select name="village_name" size="1" class="car_type2">
                    <option value="">Select Location</option>
                    <?php
                    $query1 = "SELECT village_name FROM hotels GROUP BY village_name ORDER BY village_name ";
                    $result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
                    while ($myrow1 = mysql_fetch_array($result1))
                    {
                    ?>
                    <option value="<?php echo $myrow1['village_name'];?>" <?php if ($_POST['village_name']==$myrow1['village_name']){ echo " selected";}?>><?php echo $myrow1['village_name'];?></option>
                    <?php
                    }
                    ?>
                    </select> 
                    <input name="submit1" type="submit" value="Go!" class="findnow">
                    </div>
                
                    <hr  style="clear:both; visibility:hidden;"/>
                    </form>
                    <br />	
                    
                    
					<?php
                    if (!isset($_POST['village_name'])) 	{ $village_query = "";}
                    else 									{ $village_query = " WHERE village_name='".$_POST['village_name']."' "; }
                    
                    $query0 = "SELECT * FROM hotels ".$village_query." ORDER BY hotels_title ";
                    $result0 = mysql_query($query0)  or die(mysql_error().'<p>'.$query0.'</p>');
                    $news_found = mysql_num_rows($result0);
                    if ($news_found>0)
                    {
                    $main_dir = "admin23Tre2/hotels_images";
                    
                        while ($myrow0 = mysql_fetch_array($result0))
                        {
                    
                    ?>
                    
						<?php
                        if ($myrow0['image_name']!="")
                        {
                        ?>
                        <ul class="news-list">
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <h4><?php echo stripslashes($myrow0['hotels_title']);?></h4>

                            <div class="row">
                                <div class="col-md-3 col-lg-2 col-sm-3 col-xs-12"><?php echo "<img src=\"".$main_dir."/".$myrow0['image_name']."\" border=\"0\" hspace=\"10\" vspace=\"0\" align=\"left\">"; ?></div>
                                <div class="col-md-9 col-lg-10 col-sm-9 col-xs-12 text"><p><?php echo stripslashes(nl2br($myrow0['hotels_text'])); ?></p>
                                
                                <p>
								<?php
                                if ($myrow0['website']!="")
                                {
                                ?>
                                <span style="text-align:right;"><a href="http://<?php echo str_replace("http://", "", $myrow0['website']);?>" target="_blank">read more</a></span>
                                <?php
                                }
                                ?>
								</p>                                
                                </div>
                            </div>
                            <hr/>
                        </li>
                        </ul> 
                        <?php } else {?>
                        <p>No hotels found </p> 
                        <?php }?>            
                        <?php
						}
						}
						?>
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 

<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				