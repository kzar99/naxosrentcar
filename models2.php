<?php
error_reporting(E_ALL);
include("includes/connection.php"); 
include("includes/func2.php"); 

function sortBySubkey(&$array, $subkey, $sortType = SORT_ASC) 
{
	$keys = array();
	
    foreach ($array as $subarray) 
	{
        $keys[] = $subarray[$subkey];
    }
	
    if(!empty ($keys)) {
		array_multisort($keys, $sortType, $array);
	}
}





//$page = 'models2.php';
$page = 'models.htm';

$car_type = "";
$car_show = "";
if (isset($_POST['car_type2'])) { $car_type = stripslashes($_POST['car_type2']); }


/*
	drop down list
*/
$options = '';

$options .= '<option value=""';
if ($car_type=='') { 
	$options .= ' selected';
}
$options .= '>Select vehicle type</option>	';

$q1 = "SELECT veh_code, veh_name FROM vehicles WHERE veh_code<>'' ORDER BY veh_order ";
$r1 = mysql_query($q1)  or die(mysql_error().'<p>'.$q1.'</p>');
while ($row1 = mysql_fetch_assoc($r1))
{
	$options .= '<option value="'.stripslashes($row1['veh_code']).'"';
	if ($car_type==$row1['veh_code']) { 
		$options .= ' selected';
		$car_show = stripslashes($row1['veh_name']);
	}
	$options .= '>'.stripslashes($row1['veh_name']).'</option>	';
}


/*
	car list
*/
$cars = array();
$season_array = array("low", "medium", "high", "peak", "other");


if ($car_type!=''){
	$q200="
	SELECT a.*, b.veh_name 
	FROM company_category 	AS a 
	JOIN vehicles			AS b ON a.car_type=b.veh_code
	WHERE 
		a.car_type='$car_type' 
	ORDER BY a.category 
	";
}
else {
	$q200="
	SELECT a.*, b.veh_name 
	FROM company_category 	AS a 
	JOIN vehicles			AS b ON a.car_type=b.veh_code
	ORDER BY a.category 
	";
}

$r200 = mysql_query($q200)  or die(mysql_error().'<p>'.$q200.'</p>');
$vehicles_found = mysql_num_rows($r200);
if ($vehicles_found>0) {
	
	while ($row200 = mysql_fetch_assoc($r200)) {
		$cat_id = $row200['cat_id'];
		

		$price_array = array();
		$lowest_price = "N/A";
		//$lowest_price = 0;
		
		foreach ($season_array as $season) {
			
			$query1="SELECT * FROM company_car_list_".$season." WHERE cat_id='$cat_id'  ";
			$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
			while ($myrow1 = mysql_fetch_assoc($result1)) {	
				for ($i=1; $i<=8; $i++) {
					if ($myrow1['day'.$i]>0) {
						$price_array[] = (float)$myrow1['day'.$i];
					}
				}
			}		
		}		
		
		if ( count($price_array)>0) {
			sort($price_array);
			$lowest_price = $price_array[0]." &#8364;";
		}
		
		
		if ($row200['image']!="")	{
			
			$cars[ $row200['cat_id'] ] = array(
				'image' 	=> $row200['image'], 
				'category'	=> $row200['category'], 
				'desc'		=> stripslashes($row200['description']), 
				'car_show'	=> $row200['veh_name'],
				'cat_cars'	=> stripslashes($row200['cat_cars']), 
				'price'		=> $lowest_price
			);
		}		
		
		
		
		
	}
	
}
sortBySubkey($cars, 'price'); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css2.php");?>
    </head>
    <body class="innerpage">
	<?php include("includes/_head2.php");?>

    <!--content section-->
	<section class="contentin">
        <div class="container models">


            <div class="row">
            <div class="carlist">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Models</h1>
                    

				<form action="<?php echo $page;?>" method="post" class="carlist-form">
				<input name="car_type2" type="hidden" value="<?php echo $car_type;?>">
				<div class="col-lg-6 col-md-6 col-sm-6 txt"><strong>Select vehicle type:</strong></div>
				<div class="col-lg-6 col-md-6 col-sm-6">
                <select name="car_type2" class="car_type2">
				<?php echo $options;?>
				</select>
				<input name="submit1" type="submit" value="Find" class="findnow">
                </div>
                
                <hr  style="clear:both; visibility:hidden;"/>
				</form>	
				<br />	
                
				
				<?php
				if (count($cars)>0) {
					
					echo '<ul id="list">';
					
					foreach ($cars as $id=> $c) {
                    ?>	
					<li class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
						<div class="row">
							<div class="col-md-4 col-sm-3 col-lg-4 col-xs-12 car-img">
								<img src="car_images/<?php echo $c['image'];?>" alt="Car Rental Category <?php echo $c['category'];?>" name="car image of <?php echo $c['category'];?>" border="0">
							</div>
							<div class="col-md-8 col-sm-9 col-lg-8 col-xs-12 car-desc">
							<p><strong>Info:</strong> <?php echo $c['desc'];?></p>
							<p><strong><?php echo $c['car_show'];?>:</strong> <?php echo $c['cat_cars'];?></p>
							<p><strong>From:</strong> <?php echo $c['price'];?></p>
							</div>
						</div>
					</li>
				<?php

                    }
					
					echo '
					</ul>
					';
					/* for pagination */
					echo '
					<ul class="controls pagination"></ul>
					<input id="current_page" type="hidden">
					<input id="show_per_page" type="hidden">
					';
					
                }
                else {
                ?>
                <div align="center" class="bigtxt7">No vehicles found for category <?php echo $car_show;?></div>
                <?php
                }
                ?>
                
			
				
                </div><!--col-left-->
                
                </div>
                
                <?php include("includes/_right_banners2.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes2.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 

<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>



<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
	
});
</script>


<?php 
/* pagination code + style below */
?>
<style>
	.prev{    border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;}
	.next{    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;}
.controls a{
    padding:3px;
    border:1px solid #ddd;
    padding:5px 10px;
    color:#2a6496;
	text-decoration:none;
	cursor:pointer;
	box-sizing: border-box;
	margin-left: -1px;
}
.active{
    background:#199c1c;
    color:white !important;
}
</style>
<script>
$(document).ready(function() {

    var show_per_page = 10;
    var number_of_items = $('#list').children('li').size();
    var number_of_pages = Math.ceil(number_of_items / show_per_page);

    $('#current_page').val(0);
    $('#show_per_page').val(show_per_page);

    var navigation_html = '<a class="prev" onclick="previous()">&laquo;</a>';
    var current_link = 0;
    while (number_of_pages > current_link) {
        navigation_html += '<a class="page" onclick="go_to_page(' + current_link + ')" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
        current_link++;
    }
    navigation_html += '<a class="next" onclick="next()">&raquo;</a>';

    $('.controls').html(navigation_html);
    $('.controls .page:first').addClass('active');

    $('#list').children().css('display', 'none');
    $('#list').children().slice(0, show_per_page).css('display', 'block');

});



function go_to_page(page_num) {
    var show_per_page = parseInt($('#show_per_page').val(), 0);

    start_from = page_num * show_per_page;

    end_on = start_from + show_per_page;

    $('#list').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');

    $('.page[longdesc=' + page_num + ']').addClass('active').siblings('.active').removeClass('active');

    $('#current_page').val(page_num);
}



function previous() {

    new_page = parseInt($('#current_page').val(), 0) - 1;
    //if there is an item before the current active link run the function
    if ($('.active').prev('.page').length == true) {
        go_to_page(new_page);
    }

}

function next() {
    new_page = parseInt($('#current_page').val(), 0) + 1;
    //if there is an item after the current active link run the function
    if ($('.active').next('.page').length == true) {
        go_to_page(new_page);
    }

}

</script>
</body>
</html>
				