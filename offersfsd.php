<?php
include("includes/connection.php"); 
include("includes/func.php"); 

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Offers</h1>
					<?php
                    $car_type_array = array();
                    
                    $query1="SELECT b.car_type FROM company_offers a, company_category b WHERE a.car_cat=b.cat_id AND a.is_winter='N' GROUP BY car_type ORDER BY car_type ";
                    $result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
                    while ($myrow1 = mysql_fetch_array($result1))
                    {
                    $car_type_array[] = $myrow1['car_type'];
                    }
                    
                    if (count($car_type_array)>0)
                    {
                        foreach ($car_type_array as $car_type)
                        {
                            if ($car_type=="car") {$car_show = "Car";}
                            if ($car_type=="bugrider") {$car_show = "Special Cars";}
                            if ($car_type=="atv") {$car_show = "ATV";}
                            if ($car_type=="motorcycle") {$car_show = "Motorcycle";}
                            if ($car_type=="scooter") {$car_show = "Scooter";}
                            if ($car_type=="bike") {$car_show = "Bike";}	
                    ?>
                    <h4><?php echo $car_show;?></h4>
                    <ul class="news-list">
                    
					<?php
                            $query2 = "SELECT * FROM company_offers a, company_category b WHERE a.car_cat=b.cat_id AND b.car_type='$car_type' AND is_winter='N' ORDER BY a.date_start ";
                            $result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
                            while ($myrow2 = mysql_fetch_array($result2))
                            {
                    ?>
                    <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <br /><br />
                        <div class="row">
                            <div class="col-md-3 col-lg-2 col-sm-3 col-xs-12"><?php echo "<img src=\"".$main_dir."/".$myrow0['image_name']."\" border=\"0\" align=\"left\" style=\"margin-right:10px;\">"; ?></div>
                            <div class="col-md-9 col-lg-10 col-sm-9 col-xs-12 text"><p><?php echo stripslashes(nl2br($myrow0['news_text'])); ?></p></div>
                        </div>
                        <hr/>
                    </li>
                    </ul> 
                    
                    
              	</div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
<script language="JavaScript">
function validateForm()
{	
	if (document.forms[0].elements[0].value == "")
	{
		$('#error').html("Please write your full name!");
		return false;
	}

	if (document.forms[0].elements[1].value == "" || isValidEmailAddress(document.forms[0].elements[1].value) != true)
	{
		$('#error').html("Invalid E-mail Address! Please re-enter");
		return false;
	}

	if (document.forms[0].elements[2].value == "")
	{
		$('#error').html("Please write your phone!");
		return false;
	}

}

$("#phone").keypress(function(e) {
	var a = [];
	var k = e.which;
	
	for (i = 48; i < 58; i++)
		a.push(i);
	
	if (!(a.indexOf(k)>=0))
		e.preventDefault();
	
});


</script>

</body>
</html>
				