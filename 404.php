<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/connection.php");
include($_SERVER['DOCUMENT_ROOT']."/includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>404 - Page not found - Motonaxos Europcar</title>

<?php include($_SERVER['DOCUMENT_ROOT']."/includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include($_SERVER['DOCUMENT_ROOT']."/includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>404 <strong>Page not found</strong></h1>
                    <p>We are really sorry but this page doesn't exist in our website.</p>
                    <p>You can go to <a href="/">homepage</a> or contact us for support:<br />
                    <i class="fa fa-phone"></i> By tel: +30 22850 41404<br />
                  <i class="fa fa-envelope"></i> By email: <a href="mailto:info@motonaxos.com">info@motonaxos.com</a>                    </p>
              	</div><!--col-left-->
                
                
                <?php include($_SERVER['DOCUMENT_ROOT']."/includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include($_SERVER['DOCUMENT_ROOT']."/includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/_footer.php");?>
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>