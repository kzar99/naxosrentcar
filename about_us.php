<?php
include("includes/connection.php");
include("includes/func.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Naxos Europcar Rent A Car & bike, car rentals Naxos</title>
<meta name="description" content="Naxos europcar is a Naxos rent a car & bike company offering car rentals reliable services all over Naxos island " />
<meta name="keywords" content="naxos europcar, Rent A Car & bike, naxos rent a car, auto tour rent a car, naxos car rental, car, rentals, naxos island, bike, naxos bikes, naxos bike, naxos bike rentals, Rent a car in naxo, car & motorbike rental, rent a buggy, rent a atv, rent a car in naxos airport, naxos port car rental, auto rent, car hire, ATV, bug rider motorbike, minibus ,minivan , cabriole, open top car, automatic car, naxos map, online, apartment for rent ,naxos hotels, travel information, Agia anna, Agios prokopios, Plaka beach, naxos photos, about naxos, cheap, economic, naxos town, rental office" />

<?php include("includes/_head_css.php");?>

    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>
 
    <!--content section-->
	<section class="contentin">
        <div class="container plain">
            <div class="row">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>About <strong>us</strong></h1>
					<h4>Motonaxos, the 1st Class Company to rent a car and motorbike in Naxos Island!!</h4>
	                <p>Welcome to Motonaxos, the biggest company for car and motorbike rental on Naxos. Since 1984, Motonaxos provide the best rental vehicles with high standard of services. By offering a wide range of cars for rent, new and safe models of motorbikes and quads, Motonaxos covers all driving needs. The excellent rental deals, the top quality of the hired vehicles and the VIP customer service, makes Motonaxos the best office for car and bike rental in Naxos Island. </p>
                    <p>Since 2003, we represent Europcar, the biggest car rental company in Europe, a guaranteed name in the tourism industry.  Europcar Greece and Motonaxos, gathered their knowledge and experience to provide you the best car rental in Naxos Island. We operate an extensive fleet of cars for rent, a big selection of services, experienced, helpful employees and three rental offices in Naxos. </p>
                    <p>Get to know Motonaxos today, in one of our offices on the island of Naxos or through our website. </p>
                <h4>Meet us!</h4>
                
                <ul class="about-list">
                	<li class="col-md-4 col-lg-4 col-sm-4 col-xs-12"><img src="/images/our_company_a_thumb.jpg" class="thumb"><p><strong>Main Office</strong></p></li>
                    <li class="col-md-4 col-lg-4 col-sm-4 col-xs-12"><img src="/images/our_company_b_thumb.jpg" class="thumb"><p><strong>Agia Anna resort</strong></p></li>
                    <li class="col-md-4 col-lg-4 col-sm-4 col-xs-12"><img src="/images/our_company_c_thumb.jpg" class="thumb"><p><strong>Agios Prokopios resort</strong></p></li>
                </ul>
                  <!--<p>Main Office </p>
                  <p>Agia Anna resort </p>
                  <p>Agios Prokopios resort</p>-->
                </div><!--col-left-->
                
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 
<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				