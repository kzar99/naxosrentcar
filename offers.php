<?php
include("includes/connection.php"); 
include("includes/func.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Europcar</title>
<meta name="description" content="Welcome to Europcar Naxos." >
<meta name="keywords" content="Europcar Naxos" >

<?php include("includes/_head_css.php");?>
    </head>
    <body class="innerpage">
	<?php include("includes/_head.php");?>

    <!--content section-->
	<section class="contentin">
        <div class="container plain">


            <div class="row">
            <div class="carlist">
            	<div class="col-md-8 col-lg-9 col-sm-12 col-xs-12">
                    <h1>Offers</h1>
						<?php
                        $car_type_array = array();
                        
                        $query1="SELECT b.car_type FROM company_offers a, company_category b WHERE a.car_cat=b.cat_id AND a.is_winter='N' GROUP BY car_type ORDER BY car_type ";
                        $result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
                        while ($myrow1 = mysql_fetch_array($result1))
                        {
                        $car_type_array[] = $myrow1['car_type'];
                        }
                        
                        if (count($car_type_array)>0)
                        {
                            foreach ($car_type_array as $car_type)
                            {
								$query1a="SELECT * FROM vehicles WHERE veh_code='$car_type' LIMIT 1  ";
								$result1a = mysql_query($query1a)  or die(mysql_error().'<p>'.$query1a.'</p>');
								$myrow1a = mysql_fetch_array($result1a);
								
								$car_show = $myrow1a['veh_name'];
									
                        ?>
                        <h4><?php echo $car_show;?></h4>
						<?php
                                $query2 = "SELECT * FROM company_offers a, company_category b WHERE a.car_cat=b.cat_id AND b.car_type='$car_type' AND is_winter='N' ORDER BY a.date_start ";
                                $result2 = mysql_query($query2)  or die(mysql_error().'<p>'.$query2.'</p>');
                                while ($myrow2 = mysql_fetch_array($result2))
                                {
                        ?>
                        <ul>
                        <li class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="row">
								<div class="col-md-3 col-sm-4 col-lg-3 col-xs-12 car-img">
								<?php
                                if ($myrow2['image']!="")
                                {
                                ?>
                                <img src="car_images/<?php echo $myrow2['image'];?>" border="0">
                               
                                </div>
								<div class="col-md-6 col-sm-8 col-lg-6 col-xs-12 car-desc">
                                <p><?php echo $myrow2['cat_cars'];?></p>
                                <p><strong>Period:</strong> <?php echo date("d/m/Y",$myrow2['date_start']);?> - <?php echo date("d/m/Y",$myrow2['date_end']);?></p>
                                <p><strong>Duration:</strong> <?php echo $myrow2['offer_dur'];?> days</p>
                                
								<?php
                                }
                                else
                                {
                                ?>
                                <p><?php echo $myrow2['cat_cars'];?></p>
                                <p><strong>Period:</strong> <?php echo date("d/m/Y",$myrow2['date_start']);?> - <?php echo date("d/m/Y",$myrow2['date_end']);?></p>
                                <p><strong>Duration:</strong> <?php echo $myrow2['offer_dur'];?> days</p>
                                <p><strong>Price:</strong> <span class="txt12-1"><?php echo $myrow2['offer_price'];?>&#8364;</span></p>
								<?php
                                }

								
                                if ($myrow2['offer_text']!="")
                                {
                                ?>	
                                <p><?php echo nl2br(stripslashes($myrow2['offer_text']));?></p>
								<?php
                                }
                                ?>
                                <p><a href="offer_info.php?offer=<?php echo $myrow2['offer_id'];?>&type=new" class="offer_btn">more...</a></p>	
                               </div>
                            </div>
                          
                        </li>
                        </ul> 
								<?php
                                }
                                ?>
				<?php
                    }
                } // end if ($car_type_array!="")
				else
				{
				echo '<p>No offers found</p>';
				}
                ?>
                </div><!--col-left-->
                </div>
                
                <?php include("includes/_right_banners.php");?>
                <hr  style="clear:both; visibility:hidden;"/>
            </div><!--row-->
            
            <?php include("includes/_bottom_boxes.php");?>
             <hr  style="clear:both; visibility:hidden; margin:20px 20px;"/>
        </div><!--container-->
    </section>
    
 

<?php include("includes/_footer.php");?>
<?php include("includes/_footer_scripts.php");?>
<script>
$(document).ready(function()
{
	$(".car_type2").dropkick();
});
</script>
</body>
</html>
				