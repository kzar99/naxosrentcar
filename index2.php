<?php
include("includes/connection.php");
include("includes/func2.php");

/*
create carousel
*/
$cars = array();
$season_array = array("low", "medium", "high", "peak", "other");

$q200="
SELECT 
	a.*, 
	b.veh_name 
FROM company_category 	AS a 
JOIN vehicles			AS b ON a.car_type=b.veh_code 
WHERE a.image<>'' 
ORDER BY RAND() 
";
$r200 = mysql_query($q200)  or die(mysql_error().'<p>'.$q200.'</p>');
$vehicles_found = mysql_num_rows($r200);

// counter
// we need 4 items per carousel div
// so we will create multidimensional array with sub-arrays of 4 vehicles each
$v = 0; // veh counter, max 4
$a = 0; // general counter for array build


while ($row200 = mysql_fetch_assoc($r200)) {
			
	if ( !isset($cars[$a]) ){
		$cars[$a] = array();
	}
	
	$v++;
	
	$cat_id = $row200['cat_id'];
	
	$price_array = array();
	$lowest_price = "N/A";
	
	foreach ($season_array as $season) {
		
		$query1="SELECT * FROM company_car_list_".$season." WHERE cat_id='$cat_id'  ";
		$result1 = mysql_query($query1)  or die(mysql_error().'<p>'.$query1.'</p>');
		while ($myrow1 = mysql_fetch_assoc($result1)) {	
			for ($i=1; $i<=8; $i++) {
				if ($myrow1['day'.$i]>0) {
					$price_array[] = (float)$myrow1['day'.$i];
				}
			}
		}		
	}		
	
	if ( count($price_array)>0) {
		sort($price_array);
		$lowest_price = $price_array[0]." &#8364;";
	}
	

	$cars[$a][] = array(
		'image' 	=> $row200['image'], 
		'category'	=> $row200['category'], 
		'cat_cars'	=> stripslashes($row200['cat_cars']), 
		'price'		=> $lowest_price
	);

	
	if ($v==4){
		$a++;	// increase array counter
		$v = 0;	// reset veh counter 
	}		
	
	
	//echo $a.' - '.$v.'<br>';
	
}


$carouseld_divs = '';

foreach ($cars as $c=>$data){
	
	if ($c==0){
		$carouseld_divs .= '<div class="item active">';
	}
	else {
		$carouseld_divs .= '<div class="item">';
	}
	$carouseld_divs .= '<ul class="thumbnails">';
	
	// carousel inner items(up to 4)
	foreach ($data as $d){
		
		$carouseld_divs .= '
		<li class="col-sm-3">
			<div class="casing">
				<div class="thumbnail"><img src="/car_images/'.$d['image'].'" alt="" width="121" height="78"></div>
				<div class="caption"><h4>'.$d['category'].' | <span>'.$d['price'].'</span></h4></div>
			</div>
		</li>		
		';
	}
	
	
	$carouseld_divs .= '</ul>';
	$carouseld_divs .= '</div><!-- /Slide'.($c+1).' --> 	';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Naxos Car Rental, rent a car or a bike on Naxos Island -Naxos Europcar</title>
<meta name="description" content="Motonaxos Rentals from 5€/day. Rent a car in Naxos from the Airport, Naxos town port, Agia Anna, Agios Prokopios or directly at your hotel. Special offers, Free delivery, full insurance for all the rental vehicles." />
<meta name="keywords" content="naxos europcar, Rent A Car & bike, naxos rent a car, auto tour rent a car, naxos car rental, car, rentals, naxos island, bike, naxos bikes, naxos bike, naxos bike rentals, Rent a car in naxos" />
<meta name="google-site-verification" content="_f9rmLQilialVy33S9VpfhqH1RhLuD502y9PrhQfs7E" />
<meta property="og:image" content="https://www.naxosrentcar.com/images/car-rental-1.jpg" />
<meta property="og:type" content="place" />
<meta property="og:url" content="https://naxosrentcar.com" /> 
<meta property="og:title" content="Motonaxos, Naxos Europcar, Naxos Rent a Car & bike, Economy car rental in Naxos" /> 
<meta property="og:description" content="Motonaxos rental from 5&#8364;/day. Rent a car in Agia Anna-Naxos. Rent ATV, Bug Rider. Naxos Europcar Greece. Great service from the biggest car & bike rental company in naxos. Naxos Auto rent - Naxos moto rent, Rent a car at  naxos airport, at  naxos port and all naxos hotels Special offers, Free delivery,full incurance for all the rental vechicles. Also you can see all the  Naxos apartments  & hotels, travel information’s, naxos map and all tips about naxos island" /> 
<meta property="place:location:latitude" content="37.101442" />
<meta property="place:location:longitude" content="25.3786124" />


<?php include("includes/_head_css2.php");?>

</head>

<body class="homepage">
<?php include("includes/_head2.php");?>

<!--Banner section-->
<section class="banner" style="background-image: url(images/car-rental-1.jpg);background-position: center; background-repeat: no-repeat; background-size: cover;">
	<!-- <div id="slidertitle"><h1>Naxos car rental</h1></div>	 -->
	<!-- <div id="slidecaption" class="load-item"></div> -->
	<div class="container">
	<!-- <ul id="slide-list" class="load-item"></ul> -->
		<?php echo create_search_form();?>
	</div>
	<div class="shadow"></div>
	<!-- <div class="slider"></div> -->
</section>

<!--Welcome section-->
<section class="welcome">
	<div class="container">
		<div class="col-xs-12">
			<div class="carousel slide" id="myCarousel" data-wrap="true" data-ride="carousel"  data-interval="2000">
				<div class="carousel-inner">				
					<?php echo $carouseld_divs;?>
				</div><!-- /#myCarousel -->

					<ul class="control-box pager">
						<li class="left"><a data-slide="prev" href="#myCarousel" class="arrowStil"><i class="fa fa-angle-left"></i></a></li>
						<li class="right"><a data-slide="next" href="#myCarousel" class="arrowStil"><i class="fa fa-angle-right"></i></a></li>
					</ul>				
				
				
			</div>
	</div>
</section>

<section class="welcome why-us">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<h1 class="head">Naxos car rental trusted by thousands of travellers</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="faql"><img src="images/why-us.jpg" /></div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px 40px;">
				<h3>Why <strong>choose us</strong></h3>
				<!-- <h4>Book with Motonaxos!!!</h4> -->
				<ul class="checklist">
                   	<li> <strong>More than 30 years of knowledge and experience</strong> at the business community of rental vehicles in Naxos Island. </li> 
					<li> <strong>Your safety is our primary goal</strong>, with continuous renovations of our car models, excellent service and the largest insurance coverages in the market. Offering also extra comforts such as baby seats, helmets free of charge. </li>
					<li> <strong>We are always by your side</strong>, wherever and whenever you many need us. In case of damage or accident, call 0030 694 46 22 555 and a member of our specialized staff will be with you. </li>
					<!--<li> <strong>Service of high standards with</strong> our well-trained staff. Always available to assist you and help you find the most suitable vehicle for you and provide you a detailed map with tips of Naxos. </li>
					 <li> <strong>We take care all of your needs</strong> by offering excellent modern vehicles. You can choose among the biggest range of rental cars and motorbikes in Naxos island.</li> -->
              </ul>    
              <hr  style="clear:both; visibility:hidden; margin: 20px 0px;"/>
              <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              	<a href="/why-us.htm" class="butn">Read more</a>
         	  </div>
			</div>
			
		</div>
	</div>
</section>

<section class="faq">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="">Top Questions</h2>
			</div>
		</div>
		<div class="row" style="background: #fff">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="faql"><img src="images/faq.jpg" /></div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px 40px;">
				<div class="faqr"><!-- <div class="title">Top Questions</div> -->
				<ul>
					<li><a href="#">How will I know that my reservation is confirmed?</a></li>
					<li><a href="#">Is it possible to modify my reservation after it is confirmed?</a></li>
					<li><a href="#">Is it possible to cancel my prepaid reservation after it is confirmed?</a></li>
					<li><a href="#">What is the third party insurance?</a></li>
					<li><a href="#">What is CDW (Collision Damage Waiver) insurance?</a></li>
					<li><a href="#">What is the FDW (Full Damage Waiver) insurance?</a></li>
					<li><a href="#">How old I have to be in order to buy the CDW or FDW?</a></li>

				</ul>
				<hr  style="clear:both; visibility:hidden; margin:5px 0px;"/>
              	<div style="padding-left: 4px;">View all <a href="/faq.htm" class="link">FAQs</a></div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content">
<div class="container">

	<div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box ">
                    <a href="gallery.htm">
                    	<div class="pic" style="background-image: url(/images/apartmentsenosis.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;"><i class="fa fa-camera boxicon"></i>
                        <h4>Naxos <strong>Photogallery</strong></h4></div>
                    </a>
                </div><!--box-->
            </div>			

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box ">
                    <a href="guides.htm">
                    	<div class="pic" style="background-image: url(images/location1.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;">
                        <i class="fa fa-info-circle boxicon"></i>
                        <h4>Naxos <strong>Travel info</strong></h4></div>
                    </a>
                </div>
                <!--box-->
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box ">
                    <a href="naxos.htm">
                    	<div class="pic" style="background-image: url(/images/location4_thumb.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;">
                        <i class="fa fa-compass"></i>
                        <h4>Explore <strong>Naxos</strong></h4></div>
                    </a>
                </div>
                <!--box-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="box">
                    <a href="more.htm">
                    	<div class="pic" style="background-image: url(images/location3.jpg);"></div>
                        <div style="position: absolute; top: 0px; width: 100%; height: 100%; vertical-align: middle; padding: 40px 0px;">
                        <i class="fa fa-asterisk boxicon"></i>
                        <h4>Naxos <strong>plus</strong></h4></div>
                    </a>
                </div>
                <!--box-->
            </div>
    </div><!--row-->


</div>
</section>

<section class="content">
	<div class="container about-home">
		<div class="row" style="text-align: center;"><div class="col-md-12 col-sm-12 col-xs-12"><h1 style="color: #333;">Awards</h1></div></div>
		<div class="row" style="text-align: center;">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div style="margin: 0 auto">
					<div class="award">
						<img src="images/wta-logo-2016.png" alt="wta logo 2016.png">
						<p>World's Leading Car<br> Rental Company Website</p>
					</div>
					<div class="award">
						<img src="images/logo_tripadvisor.png" alt="wta logo 2016.png">
						<p>TripAdvisor Traveller's<br> Choice 2016</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="grey-sec">
	<div class="container about-home">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">   
					
					<p><strong>Top Naxos car rental company to rent a car or motorbike!</strong></p>
	                <p>Welcome to Motonaxos, the biggest company for car and motorbike rental on Naxos. Since 1984, Motonaxos provide the best rental vehicles with high standard of services. By offering a wide range of cars for rent, new and safe models of motorbikes and quads, Motonaxos covers all driving needs. The excellent rental deals, the top quality of the hired vehicles and the VIP customer service, makes Motonaxos the best office for car and bike rental in Naxos Island. </p>
                    <p>Since 2003, we represent Europcar, the biggest car rental company in Europe, a guaranteed name in the tourism industry.  Europcar Greece and Motonaxos, gathered their knowledge and experience to provide you the best car rental in Naxos Island. We operate an extensive fleet of cars for rent, a big selection of services, experienced, helpful employees and three rental offices in Naxos. </p>
                    <!-- <p>Get to know Motonaxos today, in one of our offices on the island of Naxos or through our website. </p> -->
            </div>
			<div class="address col-md-6 col-sm-6 col-xs-12">
				<h4>Motonaxos</h4><p> Naxos island, 84300 Cyclades, Greece <br /><i class="fa fa-envelope"></i> <a href="#">info@motonaxos.com</a><br />
			<div class="address col-md-12 col-sm-6 col-xs-12" style="padding: 0px;">
				<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px">
	            <strong>Office 1:<br /></strong> Court Square, Chora (Town),<br /> Tel.: +30 22850 23420,<br /> Fax: +30 22850 26393</div>
	            <div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px"><strong>Office 2:</strong><br /> Agia Anna beach,<br /> Tel./fax: +30 22850 41404,<br /> Road service: +30 6944 622555</div>
				<div class="col-md-6 col-sm-6 col-xs-12" style="padding: 0px; margin-top: 20px;"><strong>Office 3:</strong><br /> Main street Agios Prokopios,<br /> Tel./fax: +30 22850 41633,<br /> Road service: +30 6944 622555</div>
            </div>
		
		</div>	
	</div>	
</section>

<footer class="footer">
<a class="disclaimer-logo" target="_blank" href="http://www.codibee.com" rel='designer'></a>
</footer>

<?php include("includes/_footer_scripts.php");?>
<!-- <script src="js/supersized.3.2.7.min.js" type="text/javascript"></script>
<script src="js/supersized.shutter.min.js" type="text/javascript"></script> -->
</body>
</html>
		